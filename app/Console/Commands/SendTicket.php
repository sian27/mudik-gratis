<?php

namespace App\Console\Commands;

use App\Mail\TicketPassengerMail;
use App\Mail\TicketVehicleMail;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Trans\TransMudik;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = MasterPassenger::with('vehicle', 'mudik', 'busMudik.bus', 'busBalik.bus', 'truckMudik.truck', 'truckBalik.truck', 'trip.city')
            ->with(['member.busMudik.bus', 'member.busBalik.bus'])->whereNull('parent_id')
            ->where('status', 'accept')->where('send_ticket', 0)->inRandomOrder()->limit(100)->get();

        foreach ($data as $item) {

            try {
                DB::beginTransaction();
                Mail::to($item->email)->send(new TicketPassengerMail(["data" => $item]));
                if ($item->vehicle) {
                    if ($item->mudik->detailTruck) {
                        Mail::to($item->email)->send(new TicketVehicleMail(["data" => $item]));
                        $vehicle = MasterPassengerVehicle::find($item->vehicle->id);
                        $vehicle->send_ticket = 1;
                        $vehicle->save();
                    }
                }

                $item->send_ticket = 1;
                $item->save();

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                Log::info($th);
                Log::channel('mail')->info('ticket = ' . $item->id . ' - ' . $item->nik . ' || ');
                //throw $th;                
            }
        }
    }
}
