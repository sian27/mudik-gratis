<?php

namespace App\Console\Commands;

use App\Mail\BookingPassengerMail;
use App\Mail\BookingVehicleMail;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:booking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send QR Booking';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = MasterPassenger::with('vehicle', 'trip.city')->where('status', 'accept')->whereNull('parent_id')->whereIn('send_booking', [0])->inRandomOrder() ->limit(100)->get();

        foreach ($data as $item) {
            try {
                DB::beginTransaction();
                Mail::to($item->email)->send(new BookingPassengerMail(["data" => $item]));
                if ($item->vehicle) {
                    Mail::to($item->email)->send(new BookingVehicleMail(["data" => $item]));
                    $vehicle = MasterPassengerVehicle::find($item->vehicle->id);
                    $vehicle->send_booking = 1;
                    $vehicle->save();
                }

                $item->send_booking = 1;
                $item->save();

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                // throw $th;     
                Log::info($th);
                Log::channel('mail')->info('booking = ' . $item->id . ' - ' . $item->nik . ' || ');
            }
        }
    }
}
