<?php

namespace App\Console\Commands;

use App\Models\Trans\TransBalik;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ClearDummyBalik extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:dummy_balik';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $balik = TransBalik::withTrashed()->with('detailPassenger', function ($q) {
            $q->where('name', '')->where('email', '');
        })->get();

        // Log::info(json_encode($balik));
        foreach ($balik as $item) {
            $to_time = Carbon::parse($item->created_at);
            $from_time = Carbon::parse(date("Y-m-d H:i:s"));
            $day =  $to_time->diff($from_time)->format('%D');
            $hour =  $to_time->diff($from_time)->format('%H');
            $minute =  $to_time->diff($from_time)->format('%I');
            // return response()->json([
            //     intval($day), intval($hour), intval($minute)
            // ]);
            if ((intval($day) || !intval($day)) && (intval($hour) || !intval($hour))  && intval($minute) > 30) {
                $item->delete();
            }
        }
    }
}
