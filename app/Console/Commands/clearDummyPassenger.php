<?php

namespace App\Console\Commands;

use App\Models\Master\MasterPassenger;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class clearDummyPassenger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:dummy_passenger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = MasterPassenger::where('name','')->get();
        
        Log::info($data);
        
        foreach($data as $item){
            $to_time = Carbon::parse($item->created_at);
            $from_time = Carbon::parse(date("Y-m-d H:i:s"));
            $day =  $to_time->diff($from_time)->format('%D');
            $hour =  $to_time->diff($from_time)->format('%H');
            $minute =  $to_time->diff($from_time)->format('%I');            

            // return response()->json([
            //     intval($day), intval($hour), intval($minute)
            // ]);
            if ((intval($day) || !intval($day)) && (intval($hour) || !intval($hour))  && intval($minute) > 30) {
                // $item->delete();
            }
        }
    }
}
