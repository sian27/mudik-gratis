<?php

namespace App\Console\Commands;

use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterSeatBus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class mailResend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:reblast';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = MasterPassenger::whereHas('trip', function ($q) {
            $q->where('type', 'mudik-balik');
        })->where('send_ticket', '1')->get();

        foreach ($data as $pass) {
            try {
                DB::beginTransaction();
                $pass->send_ticket = 1;

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                //throw $th;
            }
        }
    }
}
