<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterDriver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DriverController extends Controller
{
    function __construct()
    {
        // $name = "health";
        // $this->middleware('permission:' . $name . '-list|' . $name . '-create|' . $name . '-edit|' . $name . '-delete', ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $name . '-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $name . '-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $name . '-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $request->validate(
            [
                'type' => "in:bus,truck,",
            ]
        );

        $data = MasterDriver::with('bus.trip.city')->with('truck.trip.city')->orderBy('id', 'desc');
        if ($request->name) {
            $data = $data->where('name', 'ilike', '%' . $request->name . "%");
        }
        if ($request->type) {
            $data = $data->where('type', $request->type);
        }
        if ($request->city) {
            $data = $data->where(function ($d) use ($request) {
                $d->orWhereHas('bus.trip.city', function ($q) use ($request) {
                    $q->where('name', 'ilike', '%' . $request->city . "%");
                })->orWhereHas('truck.trip.city', function ($q) use ($request) {
                    $q->where('name', 'ilike', '%' . $request->city . "%");
                });
            });
        }
        if($request->all){
            $data = $data->get();
        }
        else{
            $data = $data->paginate(20);
        }

        return getNotifApi('-', '', 'success', ["driver" => $data]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => "required",
                'type' => "required|in:bus,truck",
                // 'vehicle_id' => 'required',
                'image' => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
            ]
        );

        $image = $request->file("image");
        $imageName = 'upload/driver/' . nameFile($image);
        $image->move(public_path('upload/driver'), $imageName);

        try {
            DB::beginTransaction();
            $data = new MasterDriver();
            $data->name = $request->name;
            $data->type = $request->type;
            // if ($request->type == 'bus') {
            //     $data->bus_id = $request->vehicle_id;
            // } else {
            //     $data->truck_id = $request->vehicle_id;
            // }
            $data->image = $imageName;
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            deletedFile($imageName);
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function show($id)
    {
        $data = MasterDriver::with('bus.trip.city')->find($id);

        if ($data) {
            return getNotifApi('-', '-', 'success', ['driver' => $data]);
        }

        return getNotifApi('-', '-', 'not_found');
    }

    public function update(Request $request, $id)
    {

        $request->validate(
            [
                'name' => "required",
                'type' => "required|in:bus,truck",
                // 'vehicle_id' => 'required',
                'image' => ['mimes:jpeg,jpg,png', 'file', 'max:5120'],
            ]
        );

        $data = MasterDriver::with('bus.trip.city')->find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        $image = $request->file("image");

        try {
            DB::beginTransaction();
            $data->name = $request->name;
            $data->type = $request->type;
            // if ($request->type == 'bus') {
            //     $data->bus_id = $request->vehicle_id;
            // } else {
            //     $data->truck_id = $request->vehicle_id;
            // }
            if ($image) {
                $imageName = 'upload/driver/' . nameFile($image);
                $image->move(public_path('upload/driver'), $imageName);
                deletedFile($data->image_path);
                $data->image = $imageName;
            }
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function destroy($id)
    {
        $data = MasterDriver::find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            deletedFile($data->image_path);
            $data->delete();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }
}
