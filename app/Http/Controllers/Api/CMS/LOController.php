<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterLO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LOController extends Controller
{
    function __construct()
    {
        // $name = "health";
        // $this->middleware('permission:' . $name . '-list|' . $name . '-create|' . $name . '-edit|' . $name . '-delete', ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $name . '-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $name . '-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $name . '-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $data = MasterLO::with('bus.trip.city','truck.trip.city')->orderBy('id', 'desc');
        if ($request->name) {
            $data = $data->where('name', 'ilike', '%' . $request->name . "%");
        }
        if ($request->city) {
            $data = $data->where(function ($d) use ($request) {
                $d->whereHas('bus.trip.city', function ($q) use ($request) {
                    $q->where('name', 'ilike', '%' . $request->city . "%");
                });
            });
        }
        if ($request->all) {
            $data = $data->get();
        } else {
            $data = $data->paginate(20);
        }

        return getNotifApi('-', '', 'success', ["lo" => $data]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => "required",
                'image' => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
                'email' => 'required',
                'password' => 'required|min:8',
                're_password' => 'required|same:password',
                'phone' => 'required',
                'type' => 'required|in:bus,truck',
                'vehicle_id' => 'required|integer',
            ]
        );
        $image = $request->file("image");
        $imageName = 'upload/lo/' . nameFile($image);
        $image->move(public_path('upload/lo'), $imageName);

        try {
            DB::beginTransaction();
            $data = new MasterLO();
            $data->name = $request->name;
            $data->email = $request->email;
            $data->password = bcrypt($request->password);
            $data->phone = $request->phone;
            $data->bus_id = $request->bus_id;
            $data->image = $imageName;
            $data->type = $request->type;
            if ($request->type == 'bus') {
                $data->bus_id = $request->vehicle_id;
            } else {
                $data->truck_id = $request->vehicle_id;
            }
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            deletedFile($imageName);
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function show($id)
    {
        $data = MasterLO::with('bus.trip.city')->find($id);

        if ($data) {
            return getNotifApi('-', '-', 'success', ['lo' => $data]);
        }

        return getNotifApi('-', '-', 'not_found');
    }

    public function update(Request $request, $id)
    {

        $request->validate(
            [
                'name' => "required",
                'image' => ['mimes:jpeg,jpg,png', 'file', 'max:5120'],
                'email' => 'required',
                'password' => 'min:8',
                're_password' => 'nullable|same:password',
                'phone' => 'required',
                'type' => 'required|in:bus,truck',
                'vehicle_id' => 'required|integer',
            ]
        );

        $data = MasterLO::with('bus.trip.city')->find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        $image = $request->file("image");

        try {
            DB::beginTransaction();
            $data->name = $request->name;
            $data->email = $request->email;
            $data->phone = $request->phone;
            $data->type = $request->type;
            if ($request->type == 'bus') {
                $data->bus_id = $request->vehicle_id;
                $data->truck_id = null;
            } else {
                $data->bus_id = null;
                $data->truck_id = $request->vehicle_id;
            }
            if ($request->password) {
                $data->password = bcrypt($request->password);
            }
            if ($image) {
                $imageName = 'upload/lo/' . nameFile($image);
                $image->move(public_path('upload/lo'), $imageName);
                deletedFile($data->image_path);
                $data->image = $imageName;
            }
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function destroy($id)
    {
        $data = MasterLO::find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            deletedFile($data->image_path);
            $data->delete();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }
}
