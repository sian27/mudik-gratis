<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BannerController extends Controller
{
    public function index()
    {
        $data = MasterContent::where('slug', 'banner')->get();

        return getNotifApi('', '', 'success', ['banner' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|in:html,file',
            'html' => 'required_if:slug,html',
            // 'file' => 'required_if:slug,file|mimes:jpeg,jpg,png,mp4,avi,mkv|file|max:136314880'
            'file' => ['required_if:slug,file', 'mimes:jpeg,jpg,png,mp4,avi,mkv', 'file', 'max:136314880'],
        ]);

        $file = $request->file("file");

        try {
            $data = new MasterContent();
            $data->slug = 'banner';
            $data->type = $request->type;
            $data->html = $request->html;
            if ($file) {
                $fileName = 'upload/banner/' . nameFile($file);
                $file->move(public_path('upload/banner'), $fileName);
                $data->file = $fileName;
            }
            $data->save();
            return forApi('success');
        } catch (\Throwable $th) {
            //throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function show($id)
    {
        $data = MasterContent::find($id);
        if (!$data) {
            return getNotifApi('', '', 'errors');
        }
        return forApi('success', '', ['banner' => $data]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'type' => 'required|in:html,file',
            'html' => 'required_if:slug,html',
            // 'file' => 'mimes:jpeg,jpg,png,mp4,avi,mkv|file|max:136314880'
            'file' => ['nullable', 'mimes:jpeg,jpg,png,mp4,avi,mkv', 'file', 'max:136314880'],
        ]);

        $file = $request->file("file");

        $data = MasterContent::find($id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            $data->type = $request->type;
            $data->html = $request->html;
            if ($file) {
                $fileName = 'upload/banner/' . nameFile($file);
                $file->move(public_path('upload/banner'), $fileName);
                deletedFile($data->file_path);
                $data->file = $fileName;
            }
            $data->save();
            DB::commit();
            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function destroy($id)
    {
        $data = MasterContent::find($id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            deletedFile($data->file_path);
            $data->delete();
            DB::commit();
            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
