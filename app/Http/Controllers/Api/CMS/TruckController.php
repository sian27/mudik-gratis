<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterTrip;
use App\Models\Master\MasterTruck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TruckController extends Controller
{
    function __construct()
    {
        // $name = "health";
        // $this->middleware('permission:' . $name . '-list|' . $name . '-create|' . $name . '-edit|' . $name . '-delete', ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $name . '-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $name . '-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $name . '-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $data = MasterTruck::with('trip.city','driver')->orderBy('id', 'desc');
        if ($request->city) {
            $data = $data->where('name', 'ilike', '%' . $request->city . "%");
        }
        if ($request->all) {
            $data = $data->get();
        } else {
            $data = $data->paginate(20);
        }

        return getNotifApi('-', '', 'success', ["truck" => $data]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                // 'name' => "required",
                'driver_id' => "required",
                'trip_id' => "required",
                'code' => "required",
                'no_police' => "required",
                'quota' => "required",
                'date_at' => 'required',
                'time_at' => 'required',
                'place_at' => 'required',
            ]
        );

        $trip = MasterTrip::find($request->trip_id);

        if (!$trip) {
            return setCustomError('Data Tujuan tidak ditemukan', 404);
        }

        try {
            DB::beginTransaction();
            $data = new MasterTruck();
            $data->name = $trip->city->name . ' - ' . (strlen($request->code) > 1 ? $request->code : '0' . $request->code);
            $data->driver_id = $request->driver_id;
            $data->trip_id = $request->trip_id;
            $data->code = $request->code;
            $data->no_police = $request->no_police;
            $data->quota = $request->quota;
            $data->date_at = $request->date_at;
            $data->time_at = $request->time_at;
            $data->place_at = $request->place_at;
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function show($id)
    {
        $data = MasterTruck::with('trip.city')->find($id);

        if ($data) {
            return getNotifApi('-', '-', 'success', ['truck' => $data]);
        }

        return getNotifApi('-', '-', 'not_found');
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            // 'name' => "required",
            'driver_id' => "required",
            'trip_id' => "required",
            'code' => "required",
            'no_police' => "required",
            'quota' => "required",
            'date_at' => 'required',
            'time_at' => 'required',
            'place_at' => 'required',
        ]);

        $data = MasterTruck::find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        $trip = MasterTrip::find($request->trip_id);

        if (!$trip) {
            return setCustomError('Data Tujuan tidak ditemukan', 404);
        }

        try {
            DB::beginTransaction();
            $data->name = $trip->city->name . ' - ' . (strlen($request->code) > 1 ? $request->code : '0' . $request->code);
            $data->driver_id = $request->driver_id;
            $data->trip_id = $request->trip_id;
            $data->code = $request->code;
            $data->no_police = $request->no_police;
            $data->quota = $request->quota;
            $data->date_at = $request->date_at;
            $data->time_at = $request->time_at;
            $data->place_at = $request->place_at;
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function destroy($id)
    {
        $data = MasterTruck::find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }
}
