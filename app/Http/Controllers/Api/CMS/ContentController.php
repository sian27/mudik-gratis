<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterContent;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'slug' => 'required|in:lokasi-mudik,lokasi-keberangkatan,syarat-ketentuan'
        ]);

        $data = MasterContent::where('slug', $request->slug)->first();

        return forApi('success', '', ['content' => $data]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'html' => 'required'
        ]);

        $data = MasterContent::find($id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        try {
            $data->html = $request->html;
            $data->save();
            return forApi('success');
        } catch (\Throwable $th) {
            //throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
