<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterCity;
use App\Models\Master\MasterRute;
use App\Models\Master\MasterTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TripController extends Controller
{
    function __construct()
    {
        // $name = "health";
        // $this->middleware('permission:' . $name . '-list|' . $name . '-create|' . $name . '-edit|' . $name . '-delete', ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $name . '-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $name . '-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $name . '-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $data = MasterTrip::with('city')->orderBy('id', 'desc');
        if ($request->name) {
            $data = $data->whereHas('city', function ($q) use ($request) {
                $q->where('name', 'ilike', '%' . $request->name . "%");
            });
        }
        if ($request->all) {
            $data = $data->get();
        } else {
            $data = $data->paginate(20);
        }

        return getNotifApi('-', '', 'success', ["trip" => $data]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'city_id' => "required",
                'type' => 'required|in:mudik-balik-motor,mudik-balik,mudik-saja,mudik-saja-motor'
            ]
        );

        $city = MasterCity::find($request->city_id);

        if (!$city) {
            return setCustomError('Kota tidak ditemukan', 404);
        }

        try {
            DB::beginTransaction();
            $data = new MasterTrip();
            $data->city_id = $request->city_id;
            $data->type = $request->type;
            $data->save();

            if (is_array($request->rute)) {
                if (count($request->rute) > 15) {
                    return setCustomError("Maaf maksimal rute adalah 15");
                }
                foreach (range(0, count($request->rute) - 1) as $i) {
                    MasterRute::insert([
                        "name" => $request->rute[$i], 'order' => $i + 1, "trip_id" => $data->id
                    ]);
                }
            }
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function show($id)
    {
        $data = MasterTrip::with('city')->with('rute')->find($id);

        if ($data) {
            return getNotifApi('-', '-', 'success', ['trip' => $data]);
        }

        return getNotifApi('-', '-', 'not_found');
    }

    public function update(Request $request, $id)
    {

        $request->validate(
            [
                'city_id' => "required",
                'type' => 'required|in:mudik-balik-motor,mudik-balik,mudik-saja,mudik-saja-motor'
            ]
        );

        $data = MasterTrip::with('city')->find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        $city = MasterCity::find($request->city_id);

        if (!$city) {
            return setCustomError('Kota tidak ditemukan', 404);
        }

        try {
            DB::beginTransaction();
            $data->city_id = $request->city_id;
            $data->type = $request->type;
            $data->save();

            if (is_array($request->rute)) {
                if (count($request->rute) > 15) {
                    return setCustomError("Maaf maksimal rute adalah 15");
                }
                MasterRute::where('trip_id', $data->id)->delete();
                foreach (range(0, count($request->rute) - 1) as $i) {
                    MasterRute::insert([
                        "name" => $request->rute[$i], 'order' => $i + 1, "trip_id" => $data->id
                    ]);
                }
            }
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function destroy($id)
    {
        $data = MasterTrip::find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }
}
