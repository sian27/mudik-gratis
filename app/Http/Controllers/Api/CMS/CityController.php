<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    function __construct()
    {
        // $name = "health";
        // $this->middleware('permission:' . $name . '-list|' . $name . '-create|' . $name . '-edit|' . $name . '-delete', ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $name . '-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $name . '-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $name . '-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $data = MasterCity::orderBy('id', 'desc');
        if ($request->name) {
            $data = $data->where('name', 'ilike', '%' . $request->name . "%");
        }
        if($request->all){
            $data = $data->get();
        }
        else{
            $data = $data->paginate(20);
        }

        return getNotifApi('-', '', 'success', ["city" => $data]);
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => "required",
                'terminal_name' => "required",
                'image' => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
            ]
        );

        $image = $request->file("image");
        $imageName = 'upload/city/' . nameFile($image);
        $image->move(public_path('upload/city'), $imageName);

        try {
            DB::beginTransaction();
            $data = new MasterCity();
            $data->name = $request->name;
            $data->terminal_name = $request->terminal_name;
            $data->image = $imageName;
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            deletedFile($imageName);
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function show($id)
    {
        $data = MasterCity::find($id);

        if ($data) {
            return getNotifApi('-', '-', 'success', ['city' => $data]);
        }

        return getNotifApi('-', '-', 'not_found');
    }

    public function update(Request $request, $id)
    {

        $request->validate(
            [
                'name' => "required",
                'terminal_name' => "required",
                'image' => ['nullable', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
            ]
        );

        $data = MasterCity::find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        $image = $request->file("image");

        try {
            DB::beginTransaction();
            $data->name = $request->name;
            $data->terminal_name = $request->terminal_name;
            if ($image) {
                $imageName = 'upload/city/' . nameFile($image);
                $image->move(public_path('upload/city'), $imageName);
                deletedFile($data->image_path);
                $data->image = $imageName;
            }
            $data->save();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }

    public function destroy($id)
    {
        $data = MasterCity::find($id);
        if (!$data) {
            return getNotifApi('-', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            deletedFile($data->image_path);
            $data->delete();
            DB::commit();
            return getNotifApi('-', '', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            return getNotifApi('-', '', 'errors');
        }
    }
}
