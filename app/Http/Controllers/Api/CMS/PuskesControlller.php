<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPuskes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PuskesControlller extends Controller
{
    public function index(Request $request)
    {
        $data = MasterPuskes::orderBy('id', 'desc');

        if ($request->search) {
            $data->orWhere('name', 'ilike', '%' . $request->search . '%')->orWhere('kecamatan', 'ilike', '%' . $request->search . '%')->orWhere('kabupaten', 'ilike', '%' . $request->search . '%');
        }

        $data = $data->paginate(50);

        return forApi('success', '', ['puskes' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $data = new MasterPuskes();
            $data->kabupaten = $request->kabupaten;
            $data->kecamatan = $request->kecamatan;
            $data->name = $request->name;
            $data->address = $request->address;
            $data->phone = $request->phone;
            $data->save();
            DB::commit();
            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            //throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function show($id)
    {
        $data = MasterPuskes::find($id);

        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        return forApi('success', '', ['puskes' => $data]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);

        $data = MasterPuskes::find($id);

        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            $data->kabupaten = $request->kabupaten;
            $data->kecamatan = $request->kecamatan;
            $data->name = $request->name;
            $data->address = $request->address;
            $data->phone = $request->phone;
            $data->save();
            DB::commit();
            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            //throw $th;
            return forApi('errors');
        }
    }

    public function delete($id)
    {
        $data = MasterPuskes::find($id);

        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            $data->delete();
            DB::commit();
            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            //throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
