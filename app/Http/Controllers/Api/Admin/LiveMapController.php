<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterRute;
use App\Models\Master\MasterTruck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LiveMapController extends Controller
{
    public function index(Request $request)
    {
        $result = [];
        $truck = MasterBus::selectRaw("*, 'bus' as category")->with('lo.last_report');
        $bus = MasterTruck::selectRaw("*, 'truck' as category")->with('lo.last_report')->union($truck)->orderBy('updated_at')->get();

        $result = $bus;
        // foreach ($bus as $item) {
        //     $rute = MasterRute::select('id')->where('trip_id', $item->trip_id);
        //     if ($item->status == 'start') {
        //         $rute = $rute->orderBy('order', 'desc');
        //         $rute = $rute->whereHas('checkpoint', function ($q) {
        //             $q->where('status', 'start');
        //         });
        //     } else {
        //         $rute = $rute->orderBy('order', 'asc');
        //         $rute = $rute->whereHas('checkpoint', function ($q) {
        //             $q->where('status', 'end');
        //         });
        //     }

        //     $rute = $rute->with('checkpoint')->first();

        //     if ($rute) {
        //         array_push($result, [
        //             "bus" => $item,
        //             "rute" => $rute,
        //         ]);
        //     }
        // }

        return getNotifApi('', '', 'success', ['result' => $result]);
    }
}
