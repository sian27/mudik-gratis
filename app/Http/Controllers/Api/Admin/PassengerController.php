<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Mail\BookingPassengerMail;
use App\Mail\BookingVehicleMail;
use App\Mail\RejectBooking;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterTrip;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PassengerController extends Controller
{
    public function count(Request $request)
    {
        $type = $request->type;
        if ($type == 'man') {
            $data = TransMudik::whereHas('detailPassenger', function ($q) {
                $q->where('gender', 'l');
            })->get();
        } else if ($type == 'woman') {
            $data = TransMudik::whereHas('detailPassenger', function ($q) {
                $q->where('gender', 'p');
            })->get();
        } else if ($type == 'vehicle') {
            $data = TransMudik::whereHas('detailPassenger', function ($q) {
                $q->whereHas('vehicle');
            })->get();
        } else {
            return setCustomError("invalid type value");
        }
        return getNotifApi('-', '-', 'success', ['count' => count($data)]);
    }

    public function manifest(Request $request)
    {
        // $data = TransMudik::with('detailPassenger.member', 'detailPassenger.puskes', 'detailPassenger.vehicle', 'detailBus', 'detailTruck');
        // if($request->id){
        //     $data = $data->where('id', $request->id);
        // }
        // $data = $data->get();

        $data = MasterPassenger::with('member', 'vehicle', 'puskes', 'mudik.detailBus', 'mudik.detailTruck', 'trip.city', 'busMudik.bus', 'busBalik.bus', 'truckMudik.truck', 'truckBalik.truck')->where('name', '!=', '')->whereNull('parent_id')->where('email', '!=', '');
        if ($request->only_pending == 'yes') {
            $data = $data->where('status', 'pending');
        }
        if ($request->key_search == 'city') {
            $data = $data->whereHas('trip.city', function ($q) use ($request) {
                $q->where('name', 'ilike', '%' . $request->search . '%');
            });
        }
        if ($request->key_search == 'name') {
            $data = $data->where('name', 'ilike', '%' . $request->search . '%');
        }
        if ($request->key_search == 'email') {
            $data = $data->where('email', 'ilike', '%' . $request->search . '%');
        }
        if ($request->flag == 'khusus') {
            $data = $data->where('flag', 'khusus');
        } else {
            $data = $data->where('flag', '!=', 'khusus');
        }

        // if($request->sort_city){
        //     $data = $data->orderBy('');
        // }

        $data = $data->orderBy('id', 'asc')->paginate(100);
        return getNotifApi('-', '-', 'success', ['transaction' => $data]);
    }

    public function manifest_goal()
    {
        $data = MasterPassenger::where('name', '!=', '')->where('email', '!=', '')->get();

        return forApi('success', '', ['total' => count($data)]);
    }

    public function accept(Request $request)
    {
        $request->validate([
            "passenger_id" => 'required|integer',
            'passenger_date' => 'required',
            'passenger_place' => 'required',
            'passenger_time_start' => 'required',
            'passenger_time_end' => 'required',
        ]);

        $data = MasterPassenger::with('vehicle', 'member', 'mudik', 'balik')->find($request->passenger_id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        $vehicle = MasterPassengerVehicle::where('passenger_id', $request->passenger_id)->first();

        if ($vehicle) {
            $request->validate([
                'vehicle_date' => 'required',
                'vehicle_place' => 'required',
                'vehicle_time_start' => 'required',
                'vehicle_time_end' => 'required',
            ]);
            $vehicle = true;
        }

        $trip = MasterTrip::find($data->trip_id);

        try {
            DB::beginTransaction();
            $data->status = 'accept';
            $data->verify_date = $request->passenger_date;
            $data->verify_place = $request->passenger_place;
            $data->verify_time_start = $request->passenger_time_start;
            $data->verify_time_end = $request->passenger_time_end;
            $data->send_booking = 0;
            $data->save();

            if (!$data->mudik) {
                $bus = availableBus($trip, $request->slot);
                if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                    $truck = availableTruck($trip, $request->slot);
                }

                if (!$bus['id']) {
                    return setCustomError("Qouta Bus Mudik/Balik Tujuan yang dipilih saat ini hanya tersisa " . $bus['estimate'][0], 200, ['estimate' => $bus['estimate'][0]]);
                }

                $code = '';
                $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                $code .= generateCode($data->trip_id, 2);
                $code .= generateCode($data->id, 4);

                $mudik = new TransMudik;
                $mudik->code = $code;
                $mudik->passenger_id = $data->id;
                $mudik->trip_id = $data->trip_id;
                $mudik->bus_id = $bus['id'];
                $mudik->bus_card = generateCode($bus['card'], 1);
                $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                $mudik->save();

                if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                    $balik = new TransBalik;
                    $balik->code = $code;
                    $balik->passenger_id = $data->id;
                    $balik->trip_id = $data->trip_id;
                    $balik->bus_id = $bus['id'];
                    $balik->bus_card = generateCode($bus['card'], 1);
                    $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                    $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                    $balik->save();
                }
            }

            $i = 1;

            foreach ($data->member as $item) {
                $item->status = 'accept';
                $item->verify_date = $request->passenger_date;
                $item->verify_place = $request->passenger_place;
                $item->verify_time_start = $request->passenger_time_start;
                $item->verify_time_end = $request->passenger_time_end;
                // $item->send_booking = 0;
                $item->save();

                if (!$data->mudik) {

                    $ii = $bus['card'] + $i;

                    $code = '';
                    $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                    $code .= generateCode($item->trip_id, 2);
                    $code .= generateCode($item->id, 4);

                    $mudik = new TransMudik;
                    $mudik->code = $code;
                    $mudik->passenger_id = $data->id;
                    $mudik->trip_id = $data->trip_id;
                    $mudik->bus_id = $bus['id'];
                    $mudik->bus_card = generateCode($ii, 1);
                    $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                    $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                    $mudik->save();

                    if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                        $balik = new TransBalik();
                        $balik->code = $code;
                        $balik->passenger_id = $data->id;
                        $balik->trip_id = $data->trip_id;
                        $balik->bus_id = $bus['id'];
                        $balik->bus_card = generateCode($ii, 1);
                        $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                        $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                        $balik->save();
                    }
                }
            }

            if ($vehicle) {
                $vehicle = MasterPassengerVehicle::where('passenger_id', $data->id)->first();
                if ($trip->type !=  'mudik-balik-motor' && $trip->type != 'mudik-saja-motor') {
                    $vehicle->delete();
                } else {
                    $vehicle->verify_date = $request->vehicle_date;
                    $vehicle->verify_place = $request->vehicle_place;
                    $vehicle->verify_time_start = $request->vehicle_time_start;
                    $vehicle->verify_time_end = $request->vehicle_time_end;
                    $vehicle->send_booking = 0;
                    $vehicle->save();
                }
            }

            // $data = MasterPassenger::with('mudik')->find($data->id);

            // return view('mail.booking-passenger', ['data' => $data]);

            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function resend($id)
    {

        $data = MasterPassenger::with('vehicle', 'member', 'mudik', 'balik')->find($id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        $vehicle = MasterPassengerVehicle::where('passenger_id', $id)->first();

        try {
            DB::beginTransaction();
            $data->status = 'accept';
            $data->send_booking = 0;
            $data->save();

            if ($vehicle) {
                $data->status = 'accept';
                $vehicle->send_booking = 0;
                $vehicle->save();
            }

            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function reject(Request $request)
    {
        $request->validate([
            'passenger_id' => 'required|integer',
            'reason' => 'required'
        ]);

        $data = MasterPassenger::where('nik', $request->nik)->get();

        try {
            $data->status = 'reject';
            $data->save();

            // Mail::to($data->email)->send(new RejectBooking(["data" => $data, 'request' => $request]));

            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            //throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
