<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterCity;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterSeatBus;
use App\Models\Master\MasterSeatTruck;
use App\Models\Master\MasterTrip;
use App\Models\Master\MasterTruck;
use App\Models\Other\OtherStatistic;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Ui\Presets\React;

class TripController extends Controller
{
    public function index()
    {
        $data = MasterTrip::with('city')->orderBy('id', 'asc')->get();
        return getNotifApi('-', '-', 'success', ['trip' => $data]);
    }
    public function count()
    {
        ini_set('max_execution_time', 300);
        $city = MasterCity::with('trip')->orderBy('id', 'asc')->get();
        $data = [];
        foreach ($city as $item) {
            $man = 0;
            $woman = 0;
            $vehicle = 0;
            $bus = 0;
            $truck = 0;

            foreach ($item->trip as $sub) {
                $mudik = TransMudik::where('trip_id', $sub->id)
                    ->withCount('bus')
                    ->withCount('truck')
                    ->withCount('passenger_man')
                    ->withCount('passenger_woman')
                    ->withCount('vehicle')
                    ->first();

                if ($mudik) {
                    $man += $mudik->passenger_man_count;
                    $woman += $mudik->passenger_woman_count;
                    $vehicle += $mudik->vehicle_count;
                    $bus += $mudik->bus_count;
                    $truck += $mudik->truck_count;
                }
            }

            array_push($data, [
                "name" => $item->name,
                "man" => $man,
                "woman" => $woman,
                "vehicle" => $vehicle,
                "bus" => $bus,
                "truck" => $truck,
            ]);
        }
        // $data = TransMudik::selectRaw('trip_id')->with('trip.city')
        //     ->withCount('bus')->withCount('truck')->withCount('passenger_man')
        //     ->withCount('passenger_woman')->withCount('vehicle')->orderBy('trip_id')
        //     ->groupBy('trip_id')->get();

        return getNotifApi('-', '-', 'success', ['transaction' => $data]);
    }

    public function summary()
    {
        $bus = MasterBus::selectRaw("*, 'bus' as category ")->with('lo.last_report')->with('driver')->distinct()->paginate(20);

        $truck = MasterTruck::selectRaw("*, 'truck' as category ")->with('lo.last_report')->with('driver')->distinct()->paginate(20);

        $data = [];

        foreach (range(0, 20) as $i) {
            if (!isset($bus[$i]) && !isset($truck[$i])) {
                break;
            } else {
                if (isset($bus[$i])) {
                    if (date('Y-m-d') < date('Y-m-d', strtotime('2022-05-06'))) {
                        $passenger = MasterSeatBus::where('bus_id', $bus[$i]->id)
                            ->whereHas('passenger_mudik', function ($q) {
                                $q->whereNotNull('name')->whereNotNull('email');
                            })->get();
                        $man = MasterSeatBus::where('bus_id', $bus[$i]->id)
                            ->whereHas('passenger_mudik', function ($q) {
                                $q->where('gender', 'l');
                            })->get();
                        $woman = MasterSeatBus::where('bus_id', $bus[$i]->id)
                            ->whereHas('passenger_mudik', function ($q) {
                                $q->where('gender', 'p');
                            })->get();
                    } else {
                        $passenger = MasterSeatBus::where('bus_id', $bus[$i]->id)
                            ->whereHas('passenger_balik', function ($q) {
                                $q->whereNotNull('name')->whereNotNull('email');
                            })->get();
                        $man = MasterSeatBus::where('bus_id', $bus[$i]->id)
                            ->whereHas('passenger_balik', function ($q) {
                                $q->where('gender', 'l');
                            })->get();
                        $woman = MasterSeatBus::where('bus_id', $bus[$i]->id)
                            ->whereHas('passenger_balik', function ($q) {
                                $q->where('gender', 'p');
                            })->get();
                    }
                    array_push($data, ["vehicle" => $bus[$i], "result" => ["category" => 'bus', "passenger" => count($passenger), 'man' => count($man), 'woman' => count($woman), 'vehicle' => 0]]);
                }

                if (isset($truck[$i])) {
                    // if (date('Y-m-d') < date('Y-m-d', strtotime('2022-05-06'))) {
                    //     $vehicle = MasterSeatTruck::where('truck_id', $truck[$i]->id)->whereHas('passenger_mudik', function ($q) {
                    //         $q->whereNotNull('name')->whereNotNull('email')->where('motor', 1);
                    //     })->get();
                    // } else {
                    //     $vehicle = MasterSeatTruck::where('truck_id', $truck[$i]->id)->whereHas('passenger_balik', function ($q) {
                    //         $q->whereNotNull('name')->whereNotNull('email')->where('motor', 1);
                    //     })->get();
                    // }
                    array_push($data, ["vehicle" => $truck[$i], "result" => ["category" => 'truck', "passenger" => 0, 'man' => 0, 'woman' => 0, 'vehicle' => $truck[$i]->used]]);
                }
            }
        }

        return getNotifApi('-', '-', 'success', ['summary' => $data]);
    }

    public function statistic(Request $request)
    {
        $request->validate([
            'type' => 'required|in:passenger,vehicle,total',
            'category' => 'required|in:mudik,balik',
            'verif' => 'required|in:yes,no',
        ]);

        ini_set('max_execution_time', 300);

        // $khusus = 0;
        // $data = MasterPassenger::select('*')->where('flag', '!=', 'khusus');

        // if ($request->category == 'mudik') {
        //     $data = $data->whereHas('trip', function ($q) {
        //         $q->whereIn('type', ['mudik-saja', 'mudik-balik']);
        //     });
        // } else if ($request->category == 'balik') {
        //     $data = $data->whereHas('trip', function ($q) {
        //         $q->whereIn('type', ['mudik-balik']);
        //     });
        // }
        // if ($request->type == 'passenger') {
        //     if ($request->verif == 'yes') {
        //         $khusus = 0;
        //     }
        //     $data = $data
        //         ->whereNotNull('name');
        //     if ($request->verif == 'yes') {
        //         $data = $data->where('status', 'accept');
        //     }
        // } else if ($request->type == 'vehicle') {
        //     $khusus = 0;
        //     $data = $data
        //         ->whereHas('vehicle', function ($q) use ($request) {
        //             $q->whereNotNull('no_police')->whereNotNull('trip_id');
        //             if ($request->verif == 'yes') {
        //                 $q->whereNotNull('send_booking');
        //             }
        //         });
        // }
        // $data = $data->count('id');

        // $data = 

        // $data = DB::raw("SELECT count(*) as jml FROM \"public\".\"t_mudik\" as mudik INNER JOIN \"public\".\"m_passenger\" as pass ON pass.id = mudik.passenger_id WHERE pass.flag != 'khusus' AND mudik.deleted_at IS NULL");

        $data = OtherStatistic::where('category', $request->type)->where('type', $request->category)->where('verif', $request->verif)->first();
        $khusus = 0;
        // if ($request->type == 'passenger') {
        //     $khusus = $request->category == 'mudik' ? 2600 : 2540;
        // }

        return forApi('success', null, ['result' => $data->amount + $khusus]);

        // return forApi('success', null, ['result' => $data['jml'] + $khusus]);
    }
}
