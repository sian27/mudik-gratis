<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterRute;
use Illuminate\Http\Request;

class CheckpointController extends Controller
{
    public function index(Request $request)
    {

        $bus = MasterBus::get();

        $result = [];

        foreach ($bus as $item) {
            $data = MasterRute::where('trip_id', $item->trip_id);
            if ($item->status == 'start') {
                $data = $data->orderBy('order', 'asc');
                $data = $data->with('checkpoint', function ($q) {
                    $q->where('status', 'start');
                });
            } else {
                $data = $data->orderBy('order', 'desc');
                $data = $data->with('checkpoint', function ($q) {
                    $q->where('status', 'end');
                });
            }

            $data = $data->get();

            array_push($result, [
                "bus" => $item,
                "rute" => $data,
            ]);
        }        

        return getNotifApi('', '', 'success', ['result' => $result]);
    }
}
