<?php

namespace App\Http\Controllers\Api\Dinkes;

use App\Http\Controllers\Controller;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;

class ManifestController extends Controller
{
    public function index(Request $request)
    {
        $data = TransMudik::orderBy('id', 'desc')->whereHas('detailPassenger', function ($q) {
            $q->whereNotNull('puskes_id');
        })->whereHas('detailPassenger.puskes', function ($q) {
            $q->where('kabupaten', 'ilike', '%' . auth('dinkes')->user()->territory . '%');
        })->with('detailPassenger.puskes')->get();
        return getNotifApi('-', '-', 'success', ['manifest' => $data]);
    }
}
