<?php

namespace App\Http\Controllers\Api\Dinkes;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterAdmin;
use App\Models\Master\MasterDinkes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    private $slug = 'auth.dinkes';

    /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth("dinkes")->attempt($validator->validated())) {
            return getNotifApi($this->slug, 'login', 'errors');
        }

        if (!MasterDinkes::where('email', $request->email)->where('active', 1)->first()) {
            return response()->json([
                "success" => false,
                "data" => null,
                "notification" => [
                    "title" => "Opps",
                    "message" => "Your account has been deactived"
                ],
            ]);
        }

        return getNotifApi($this->slug, 'login', 'success', $this->respondWithToken($token)->original);
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth("dinkes")->logout();

        return getNotifApi($this->slug, 'logout', 'success');
    }

    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        $data = auth('dinkes')->user();
        if ($data) {
            return getNotifApi($this->slug, 'profile', 'success', ["user" => $data]);
        }
        return getNotifApi($this->slug, 'login', 'errors');
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            // 'expires_in' => auth('dinkes')->factory()->getTTL() * 60
            // 'expires_in' => Carbon::now()->addDays(7)->timestamp
        ]);
    }
}
