<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterCity;
use Illuminate\Http\Request;

class CityController extends Controller
{
    protected $slug = 'frontend-city';

    public function count_city()
    {
        $data = MasterCity::get();
        return getNotifApi($this->slug, 'count-bus', 'success', ['count' => count($data)]);
    }

}
