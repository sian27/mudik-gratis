<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterPassenger;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransBus;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BusController extends Controller
{
    protected $slug = 'frontend-bus';

    public function count()
    {
        // $start = MasterBus::select('*')->whereHas('trip', function ($q) {
        //     $q->whereIn('type', ['mudik-saja-motor', 'mudik-saja']);
        // })->orderBy('id', 'desc')->get();   
        // $end = MasterBus::select('*')
        //     ->whereHas('trip', function ($q) {
        //         $q->whereIn('type', ['mudik-balik-motor', 'mudik-balik']);
        //     })
        //     ->get();
        $start = 292;
        $end = 200;
        return getNotifApi(
            $this->slug,
            '-',
            'success',
            ["count" =>
            ['start' => number_format($start, 0), "end" => number_format($end, 0)]]
        );
    }

    public function count_quota()
    {
        // $start = MasterBus::select(DB::raw('sum(quota) as quota'))->whereHas('trip', function ($q) {
        //     $q->whereIn('type', ['mudik-saja-motor', 'mudik-saja']);
        // })->first();
        // $end = MasterBus::select(DB::raw('sum(quota) as quota'))->whereHas('trip', function ($q) {
        //     $q->whereIn('type', ['mudik-balik-motor', 'mudik-balik']);
        // })->first();
        $start = 11680;
        $end = 8000;
        return getNotifApi(
            $this->slug,
            '-',
            'success',
            ["quota" =>
            ['start' => number_format($start, 0), "end" => number_format($end, 0)]]
        );
    }

    public function count_quota_available()
    {
        $taked_mudik = MasterPassenger::select('*')->where('flag', '!=', 'khusus')
            ->whereNotNull('name')
            ->whereHas('trip', function ($q) {
                $q->whereIn('type', ['mudik-saja', 'mudik-balik']);
            })->count('id');
        $quota_start = 11680;
        if ($taked_mudik) {
            $quota_start -= $taked_mudik + 2600;
        }
        // $quota_start -=  2600;
        // $quota_start -=  0;
        // dump($taked_mudik + 2540);
        $taked_balik = MasterPassenger::select('*')->where('flag', '!=', 'khusus')
            ->whereNotNull('name')
            ->whereHas('trip', function ($q) {
                $q->whereIn('type', ['mudik-balik']);
            })->count('id');

        $quota_end = 8000;
        if ($taked_balik) {
            $quota_end -= $taked_balik + 2540;
        }
        // $quota_end -= 2540;
        // $quota_end = 857;
        // dump($taked_balik + 2540);
        
        return getNotifApi(
            $this->slug,
            '-',
            'success',
            ["available" =>
            ['start' => number_format(0, 0), "end" => number_format( 0, 0)]]
        );
    }
}
