<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterTrip;
use Illuminate\Http\Request;

class TripController extends Controller
{
    public function check_type(Request $request)
    {
        $data = MasterTrip::find($request->id);
        if ($data) {
            return getNotifApi('-', '-', 'success', ['trip' => $data]);
        }
        return getNotifApi('-', '-', 'errors', ['trip' => $data]);
    }
}
