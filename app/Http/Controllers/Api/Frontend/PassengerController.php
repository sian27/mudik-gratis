<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PassengerController extends Controller
{
    public function pdf($key)
    {
        if (!$key) {
            return setCustomError('Key invalid');
        }
        $decrypt = decrypt($key);
        $key = explode(",", $decrypt);

        $id = $key[0];
        $type = $key[1];
        $category = $key[2];

        $send['id'] = $id;
        $send['type'] = $type;
        $send['category'] = $category;

        if ($category == 'passenger') {
            $data = MasterPassenger::with('mudik.trip.city', 'mudik.detailBus', 'mudik.detailTruck')->find($id);
            if (!$data) {
                return forApi('not_found');
            }
            $send['data'] = $data;
            if ($type == 'booking') {
                return view('mail.booking-passenger', $send);
            } else if ($type == 'ticket') {

                $no_seat = '';
                $all_id = [];
                $all = MasterPassenger::select('id')->where('id', $id)->orWhere('parent_id', $id)->whereNotNull('name')->whereNotNull('email')->get();

                foreach ($all as $items) {
                    array_push($all_id, $items->id);
                }

                $trans = TransMudik::select('bus_card')->whereIn('passenger_id', $all_id)->get();

                foreach ($trans as $i => $items) {
                    if ($i == 0) {
                        $no_seat .= $items->bus_card;
                    } else {
                        $no_seat .= ', ' . $items->bus_card;
                    }
                }

                $send['seat'] = $no_seat;

                return view('mail.ticket-passenger', $send);
            }
        } else if ($category == 'vehicle') {
            $data = MasterPassenger::with('vehicle', 'mudik.trip.city', 'mudik.detailBus', 'mudik.detailTruck')->find($id);
            if (!$data) {
                return forApi('not_found');
            }
            $send['data'] = $data;
            if ($type == 'booking') {
                return view('mail.booking-vehicle', $send);
            } else if ($type == 'ticket') {

                $no_seat = '';
                $all_id = [];
                $all = MasterPassenger::select('id')->where('id', $id)->orWhere('parent_id', $id)->whereNotNull('name')->whereNotNull('email')->get();

                foreach ($all as $items) {
                    array_push($all_id, $items->id);
                }

                $trans = TransMudik::select('bus_card')->whereIn('passenger_id', $all_id)->get();

                foreach ($trans as $i => $items) {
                    if ($i == 0) {
                        $no_seat .= $items->bus_card;
                    } else {
                        $no_seat .= ', ' . $items->bus_card;
                    }
                }

                $send['seat'] = $no_seat;

                return view('mail.ticket-vehicle', $send);
            }
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "city" => 'required'
        ]);

        $data = MasterPassenger::with('member')->find($id);
        if (!$data) {
            getNotifApi('', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            $data->placebirth = $request->city;
            if ($data->member) {
                foreach ($data->member as $items) {
                    $member = MasterPassenger::find($items->id);
                    $member->placebirth = $request->city;
                    $member->save();
                }
            }
            $data->save();
            DB::commit();
            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            //throw $th;
            return forApi('errors');
        }
    }


    public function pdfUnencrypt(Request $request)
    {
        $request->validate([
            'passenger_id'  => 'required',
            'type'  => 'required|in:booking,ticket',
            'category'  => 'required|in:passenger,vehicle',
        ]);

        $id = $request->passenger_id;
        $type = $request->type;
        $category = $request->category;

        $send['id'] = $id;
        $send['type'] = $type;
        $send['category'] = $category;

        $data = MasterPassenger::with('vehicle', 'mudik', 'busMudik.bus', 'busBalik.bus', 'truckMudik.truck', 'truckBalik.truck', 'trip.city')
            ->with(['member.busMudik.bus', 'member.busBalik.bus'])->find($id);
        if ($category == 'passenger') {
            if (!$data) {
                return forApi('not_found');
            }
            $send['data'] = $data;
            if ($type == 'booking') {
                return view('mail.booking-passenger', $send);
            } else if ($type == 'ticket') {
                return view('mail.ticket-passenger', $send);
            }
        } else if ($category == 'vehicle') {
            if (!$data) {
                return forApi('not_found');
            }
            $send['data'] = $data;
            if ($type == 'booking') {
                return view('mail.booking-vehicle', $send);
            } else if ($type == 'ticket') {
                return view('mail.ticket-vehicle', $send);
            }
        }
    }
}
