<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterTruck;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TruckController extends Controller
{
    public function count()
    {
        $start = MasterTruck::select('*')->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-saja', 'mudik-balik']);
        })->get();
        $end = MasterTruck::select('*')->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-balik']);
        })->get();
        return getNotifApi(
            "-",
            '-',
            'success',
            ["count" =>
            ['start' => number_format(count($start), 0), "end" => number_format(count($end), 0)]]
        );
    }

    public function count_quota()
    {
        $start = MasterTruck::select(DB::raw('sum(quota) as quota'))->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-saja', 'mudik-balik']);
        })->first();
        $end = MasterTruck::select(DB::raw('sum(quota) as quota'))->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-balik']);
        })->first();
        return getNotifApi(
            "-",
            '-',
            'success',
            ["quota" =>
            ['start' => number_format($start ? $start->quota : 0, 0), "end" => number_format($end ? $end->quota : 0, 0)]]
        );
    }

    public function count_quota_available()
    {
        $start = MasterTruck::select(DB::raw('sum(quota) as quota'))
        ->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-saja', 'mudik-balik']);
        })->first();
        $taked_mudik = MasterPassenger::select('*')->where('flag', '!=', 'khusus')
        ->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-saja', 'mudik-balik']);
        })
            ->whereNotNull('name')->whereNotNull('email')
            ->whereHas('vehicle')
            ->count('id');
        $quota_start = $start->quota;
        if ($taked_mudik) {
            $quota_start -= $taked_mudik;
        }
        $end = MasterTruck::select(DB::raw('sum(quota) as quota'))
        ->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-balik']);
        })->first();
        $taked_balik = MasterPassenger::select('*')->where('flag', '!=', 'khusus')
        ->whereHas('trip', function ($q) {
            $q->whereIn('type', ['mudik-balik']);
        })->whereNotNull('name')->whereNotNull('email')
            ->whereHas('vehicle')
            ->count('id');
        $quota_end = $end->quota;
        if ($taked_balik) {
            $quota_end -= $taked_balik;
        }
        return getNotifApi(
            "-",
            '-',
            'success',
            ["available" =>
            ['start' => number_format( 0, 0), "end" => number_format( 0, 0)]]
        );
    }
}
