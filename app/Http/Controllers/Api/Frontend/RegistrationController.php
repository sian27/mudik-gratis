<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterTrip;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegistrationController extends Controller
{
    private $slug = 'frontend-registration';

    public function booking(Request $request)
    {
        $request->validate([
            "trip_id" => "required",
            'province' => 'required',
            'nik' => 'required',
            "slot" => "required|integer"
        ]);

        if (!$request->slot) {
            $request->slot = 4;
        }

        if ($request->slot > 4) {
            $request->slot = 4;
        }

        $trip = MasterTrip::find($request->trip_id);
        if (!$trip) {
            return forApi('errors', 'Kota Tujuan tidak ditemukan');
        }

        $bus = availableBus($trip, $request->slot);
        if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
            $truck = availableTruck($trip, $request->slot);
            if (!$truck['id']) {
                return forApi('errors', "Yahh kamu kurang cepat, Kuota Truck Mudik/Balik Anda saat ini hanya tersisa " . $truck['estimate'][0] . ". Silahkan pilih yang tanpa motor jika ada", ['estimate' => 0, 'type' => 'truck']);
            }
        }

        if (!$bus['id']) {
            return forApi('errors', "Yahh kamu kurang cepat, Kuota Bus Mudik/Balik Anda saat ini hanya tersisa " . $bus['estimate'][0], ['estimate' => $bus['estimate'][0], "type" => 'bus']);
        }

        if (!$request->base_id) {
            try {
                DB::beginTransaction();

                $parent = MasterPassenger::where('nik', $request->nik)->whereNull('name')->first();
                if (!$parent) {

                    $parent = new MasterPassenger;
                    $parent->trip_id = $request->trip_id;
                    $parent->nik = $request->nik;
                    $parent->save();
                    $parent = MasterPassenger::with('mudik', 'balik')->find($parent->id);

                    $code = '';
                    $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                    $code .= generateCode($trip->id, 2);
                    $code .= generateCode($parent->id, 4);

                    $province = strtolower($request->placebirth);
                    // if (is_jakarta($province)) {

                    $mudik = new TransMudik;
                    $mudik->code = $code;
                    $mudik->passenger_id = $parent->id;
                    $mudik->trip_id = $trip->id;
                    $mudik->bus_id = $bus['id'];
                    $mudik->bus_card = generateCode($bus['card'], 1);
                    $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                    $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                    $mudik->save();

                    if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                        $balik = new TransBalik;
                        $balik->code = $code;
                        $balik->passenger_id = $parent->id;
                        $balik->trip_id = $trip->id;
                        $balik->bus_id = $bus['id'];
                        $balik->bus_card = generateCode($bus['card'], 1);
                        $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                        $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                        $balik->save();
                    }
                    // }

                    $i = 1;
                    foreach (range(1, $request->slot - 1) as $i) {
                        $sub = new MasterPassenger();
                        $sub->trip_id = $request->trip_id;
                        $sub->parent_id = $parent->id;
                        $sub->save();

                        $code = '';
                        $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                        $code .= generateCode($trip->id, 2);
                        $code .= generateCode($sub->id, 4);

                        // if (is_jakarta($province)) {
                        $mudik = new TransMudik;
                        $mudik->code = $code;
                        $mudik->passenger_id = $sub->id;
                        $mudik->trip_id = $trip->id;
                        $mudik->bus_id = $bus['id'];
                        $mudik->bus_card = generateCode($bus['card'] + $i, 1);
                        $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                        $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                        $mudik->save();

                        if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                            $balik = new TransBalik;
                            $balik->code = $code;
                            $balik->passenger_id = $sub->id;
                            $balik->trip_id = $trip->id;
                            $balik->bus_id = $bus['id'];
                            $balik->bus_card = generateCode($bus['card'] + $i, 1);
                            $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                            $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                            $balik->save();
                        }
                        // }
                    }

                    if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                        $vehicle  = new MasterPassengerVehicle;
                        $vehicle->passenger_id = $parent->id;
                        $vehicle->trip_id = $request->trip_id;
                        $vehicle->save();
                    }
                }
                DB::commit();
                return getNotifApi('', '', 'success', ['id' => $parent->id]);
            } catch (\Throwable $th) {
                DB::rollBack();
                throw $th;
                return forApi('errors');
            }
        }
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'email' => 'required|email',
                'nik' => 'required|max:16|min:16',
                'no_kk' => 'required|max:16|min:16',
                'gender' => 'required|in:p,l',
                'phone' => 'required|max:16|min:9',
                'datebirth' => 'required',
                'placebirth' => 'required',
                'address' => 'required',
                'trip_id' => 'required',
                // 'file_ktp' => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
                // 'file_kk' => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
                // 'vaksin' => 'required|in:green,yellow,red',
            ],
        );

        if ($request->quota > 4) {
            $request->quota = 4;
        }

        // $file_booster = null;

        // if (!$request->file_booster) {
        //     $request->validate([
        //         "type_vaksin" => 'required|in:booster,pcr,antigen',
        //         "puskes_id" => 'required|integer'
        //     ]);
        // } else {
        //     $request->validate([
        //         "file_booster" => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120']
        //     ]);
        // }
        $file_booster = $request->file('file_booster');

        $parent = MasterPassenger::with('member')->with('vehicle')->where('id', $request->base_id)->first();

        if (!$parent) {
            $trip = MasterTrip::find($request->trip_id);
            if (!$trip) {
                return forApi('errors', 'Kota Tujuan tidak ditemukan');
            }

            $bus = availableBus($trip, $request->quota);
            if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                $truck = availableTruck($trip, $request->quota);
                if (!$truck['id']) {
                    return forApi('errors', "Yahh kamu kurang cepat, Kuota Truck Mudik/Balik Anda saat ini hanya tersisa " . $truck['estimate'][0] . ". Silahkan pilih yang tanpa motor jika ada", ['estimate' => 0, 'type' => 'truck']);
                }
            }

            if (!$bus['id']) {
                return forApi('errors', "Yahh kamu kurang cepat, Kuota Bus Mudik/Balik Anda saat ini hanya tersisa " . $bus['estimate'][0], ['estimate' => $bus['estimate'][0], "type" => 'bus']);
            }

            try {
                DB::beginTransaction();

                $parent = new MasterPassenger;
                $parent->trip_id = $request->trip_id;
                $parent->nik = $request->nik;
                $parent->save();
                $parent = MasterPassenger::with('mudik', 'balik')->find($parent->id);

                $code = '';
                $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                $code .= generateCode($trip->id, 2);
                $code .= generateCode($parent->id, 4);


                $province = strtolower($request->province);
                // if ($province == 'jakarta') {

                $mudik = new TransMudik;
                $mudik->code = $code;
                $mudik->passenger_id = $parent->id;
                $mudik->trip_id = $trip->id;
                $mudik->bus_id = $bus['id'];
                $mudik->bus_card = generateCode($bus['card'], 1);
                $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                $mudik->save();

                if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                    $balik = new TransBalik;
                    $balik->code = $code;
                    $balik->passenger_id = $parent->id;
                    $balik->trip_id = $trip->id;
                    $balik->bus_id = $bus['id'];
                    $balik->bus_card = generateCode($bus['card'], 1);
                    $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                    $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                    $balik->save();
                }
                // }

                // $i = 1;                
                foreach (range(1, $request->quota - 1) as $i) {
                    $sub = new MasterPassenger();
                    $sub->trip_id = $request->trip_id;
                    $sub->parent_id = $parent->id;
                    $sub->save();

                    $code = '';
                    $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                    $code .= generateCode($trip->id, 2);
                    $code .= generateCode($sub->id, 4);

                    // if ($province == 'jakarta') {
                    $mudik = new TransMudik;
                    $mudik->code = $code;
                    $mudik->passenger_id = $sub->id;
                    $mudik->trip_id = $trip->id;
                    $mudik->bus_id = $bus['id'];
                    $mudik->bus_card = generateCode($bus['card'] + $i, 1);
                    $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                    $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                    $mudik->save();

                    if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                        $balik = new TransBalik;
                        $balik->code = $code;
                        $balik->passenger_id = $sub->id;
                        $balik->trip_id = $trip->id;
                        $balik->bus_id = $bus['id'];
                        $balik->bus_card = generateCode($bus['card'] + $i, 1);
                        $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  $truck['id'] : null;
                        $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ?  generateCode($truck['card'], 1) : null;
                        $balik->save();
                    }
                    // }
                }

                if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                    $vehicle  = new MasterPassengerVehicle;
                    $vehicle->passenger_id = $parent->id;
                    $vehicle->trip_id = $request->trip_id;
                    $vehicle->save();
                }

                $request->base_id = $parent->id;
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                throw $th;
                return forApi('errors');
            }
        }

        $parent = MasterPassenger::with('member')->with('vehicle')->where('id', $request->base_id)->first();

        $trip = MasterTrip::find($request->trip_id);

        // get file image
        $file_ktp = $request->file('file_ktp');
        $file_kk = $request->file('file_kk');
        $ktp = 'upload/passenger/' . nameFile($file_ktp);
        $kk = 'upload/passenger/' . nameFile($file_kk);

        try {
            DB::beginTransaction();

            // create data parent            
            $parent->name = $request->name;
            $parent->email = $request->email;
            $parent->nik = $request->nik;
            $parent->no_kk = $request->no_kk;
            $parent->trip_id = $request->trip_id;
            $parent->gender = $request->gender;
            $parent->phone = $request->phone;
            $parent->placebirth = $request->placebirth;
            $parent->address = $request->address;
            $parent->datebirth = $request->datebirth;
            $parent->vaksin = $request->vaksin;

            // uploading file            
            $file_ktp->move(public_path('upload/passenger'), $ktp);
            $file_kk->move(public_path('upload/passenger'), $kk);


            $parent->file_ktp = $ktp;
            $parent->file_kk = $kk;

            if ($file_booster) {
                $booster = 'upload/passenger/' . nameFile($file_booster);
                $file_booster->move(public_path('upload/passenger'), $booster);
                $parent->file_booster = $booster;
            } else {
                $parent->type_vaksin = $request->type_vaksin;
                $parent->puskes_id = $request->puskes_id;
            }
            $parent->flag = 'web';

            $parent->save();

            // input data anggota keluarga
            $i = 1;
            foreach ($parent->member as $item) {
                $status = true;
                $list = ['name', 'placebirth', 'datebirth', 'nik', 'phone', 'email', 'gender'];
                foreach ($list as $item) {
                    $check = $item . '_family_' . $i;
                    if ($item === null) {
                    } else {
                        if (!$request[$check]) {
                            $status = false;
                        }
                    }
                }
                if ($status) {
                    // $file_booster = null;
                    // if (!$request['file_booster_family_' . $i]) {
                    //     $request->validate([
                    //         "type_vaksin_family_" . $i => 'required|in:booster,pcr,antigen',
                    //         "puskes_id_family_" . $i => 'required|integer'
                    //     ]);
                    // } else {
                    //     $request->validate([
                    //         "file_booster_family_" . $i => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120']
                    //     ]);
                    // }
                    $file_booster_member = $request->file('file_booster_family_' . $i);
                    $item->trip_id = $request->trip_id;
                    $item->name = $request['name_family_' . $i];
                    $item->placebirth = $request['placebirth_family_' . $i];
                    $item->datebirth = $request['datebirth_family_' . $i];
                    $item->nik = $request['nik_family_' . $i];
                    $item->no_kk = $parent->no_kk;
                    $item->phone = $request['phone_family_' . $i];
                    $item->email = $request['email_family_' . $i];
                    $item->gender = $request['gender_family_' . $i];
                    $item->vaksin = $request['vaksin_family_' . $i];

                    if ($file_booster_member) {
                        $booster = 'upload/passenger/' . nameFile($file_booster_member);
                        $file_booster_member->move(public_path('upload/passenger'), $booster);
                        $item->file_booster = $booster;
                    } else {
                        $item->type_vaksin = $request['type_vaksin_family_' . $i];
                        $item->puskes_id = $request['puskes_id_family_' . $i];
                    }
                    $item->flag = 'web';

                    $i++;

                    $item->save();
                }
            }

            if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                $request->validate([
                    'no_police' => 'required',
                    'merk_vehicle' => 'required',
                    'type_vehicle' => 'required',
                    'color_vehicle' => 'required',
                    'no_stnk' => 'required',
                ]);
                // input date kendaraan
                $vehicle = MasterPassengerVehicle::where('passenger_id', $parent->id)->first();
                // $vehicle->passenger_id = $parent->id;
                $vehicle->trip_id = $request->trip_id ?? $request->city_id;
                $vehicle->no_police = $request->no_police;
                $vehicle->merk_vehicle = $request->merk_vehicle;
                $vehicle->type_vehicle = $request->type_vehicle;
                $vehicle->color_vehicle = $request->color_vehicle;
                $vehicle->no_stnk = $request->no_stnk;
                $vehicle->owner_name = $parent->name;
                $vehicle->owner_nik = $parent->nik;
                $vehicle->owner_no_kk = $parent->no_kk;
                $vehicle->owner_email = $parent->email;
                $vehicle->owner_phone = $parent->phone;
                $vehicle->flag = 'web';
                $vehicle->save();
            }
            MasterPassenger::where('parent_id', $parent->id)->whereNull('name')->whereNull('email')->delete();
            DB::commit();
            return getNotifApi('', '-', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '-', 'errors');
        }
    }

    public function check_quota(Request $request)
    {
        $request->validate(
            [
                'trip_id' => 'required',
            ]
        );

        if (!isset($request->slot)) {
            $request->slot = 4;
        }

        if ($request->slot > 4) {
            $request->slot = 4;
        }

        $trip = MasterTrip::find($request->trip_id);
        if (!$trip) {
            return forApi('errors', 'Kota Tujuan tidak ditemukan');
        }

        $bus = availableBus($trip, $request->slot);
        if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
            $truck = availableTruck($trip);
            if (!$truck['id']) {
                return forApi('errors', "Yahh kamu kurang cepat, Kuota Truck Mudik/Balik Anda saat ini hanya tersisa " . $truck['estimate'][0] . ". Silahkan pilih yang tanpa motor jika ada", ['estimate' => 0, 'type' => 'truck']);
            }
        }

        if (!$bus['id']) {
            return forApi('errors', "Yahh kamu kurang cepat, Kuota Bus Mudik/Balik Anda saat ini hanya tersisa " . $bus['estimate'][0], ['estimate' => $bus['estimate'][0], "type" => 'bus']);
        }

        return forApi('success', null, ['bus_id' => $bus['id'], 'estimate' => $bus['estimate'][0]]);
    }

    public function check_nik(Request $request)
    {
        $request->validate(
            [
                'nik' => 'required',
            ]
        );

        $check = MasterPassenger::where("nik", $request->nik)->whereNotNull('name')->whereNotNull('email');
        if ($request->base_id) {
            // $check = $check->where(function ($q) use ($request) {
            //     $q->orWhere('name', '');
            // });
        }
        $check = $check->first();
        if (!$check) {
            return response()->json([
                // "status" => true,
                "valid" => true,
                "message" => "NIK tersedia"
            ]);
        }
        return response()->json([
            // "status" => false,
            "valid" => false,
            "message" => "NIK sudah terdaftar"
        ]);
    }
}
