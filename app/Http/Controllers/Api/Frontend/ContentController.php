<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterContent;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    private $slug = 'frontend-content';

    public function banner()
    {
        $data = MasterContent::where('type', 'banner')->orderBy('id', 'desc')->get();
        return getNotifApi($this->slug, '-', 'success', ['content' => $data]);
    }

    public function syarat_pendaftaran()
    {
        $data = MasterContent::where('type', 'syarat-pendaftaran')->orderBy('id', 'desc')->first();
        return getNotifApi($this->slug, '-', 'success', ['content' => $data]);
    }

    public function lokasi_mudik()
    {
        $data = MasterContent::where('type', 'lokasi-mudik')->orderBy('id', 'desc')->first();
        return getNotifApi($this->slug, '-', 'success', ['content' => $data]);
    }

    public function lokasi_keberangkatan()
    {
        $data = MasterContent::where('type', 'lokasi-keberangkatan')->orderBy('id', 'desc')->first();
        return getNotifApi($this->slug, '-', 'success', ['content' => $data]);
    }

    public function syarat_ketentuan()
    {
        $data = MasterContent::where('type', 'syarat-ketentuan')->orderBy('id', 'desc')->first();
        return getNotifApi($this->slug, '-', 'success', ['content' => $data]);
    }
}
