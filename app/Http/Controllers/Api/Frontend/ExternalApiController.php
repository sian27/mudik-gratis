<?php

namespace App\Http\Controllers\Api\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Trans\TransCallApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;

class ExternalApiController extends Controller
{
    private $tokenDigiData = 'YjI4MjljM2YtZmU3NC00N2IzLWFmOGQtNmI1YTI1ZmNiMmFi';

    function quota()
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Token' => $this->tokenDigiData
        ];

        $client = new GuzzleClient([
            'headers' => $headers
        ]);
        $response = $client->get('https://api.digidata.ai/trondigi_prod/remaining-balance');

        return json_decode($response->getBody()->getContents());
    }

    public function check_vaksin(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = new TransCallApi;
            $data->type = "check-vaksin";
            $data->date = date('Y-m-d');
            $json = ["nik" => $request->nik];
            $data->description = json_encode($json);
            $data->save();

            $client = new \GuzzleHttp\Client();
            $response = $client->post('http://45.77.34.207/bridge/vacc?key=TronT3st3r&nik=' . $request->nik . '&fullName=R&name=Tron');
            DB::commit();
            return $response;
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('-', '-', 'errors');
        }
    }

    public function orc_extra(Request $request)
    {
        $qouta = $this->quota()->data[0]->remainingAccess;
        if ($qouta) {
            try {
                DB::beginTransaction();
                $data = new TransCallApi;
                $data->type = "orc-extra";
                $data->date = date('Y-m-d');
                $data->save();
                $headers = [
                    'Content-Type' => 'application/json',
                    'Token' => $this->tokenDigiData
                ];

                $client = new GuzzleClient([
                    'headers' => $headers
                ]);
                $response = $client->post('https://api.digidata.ai/trondigi_prod/ocr_extra', [
                    'json' => [
                        "trx" => 12,
                        "ktp_image" => $request->ktp_image
                    ]
                ]);
                DB::commit();
                Log::info('berhasil');
                // Log::info($response);
                return response()->json($response->getBody()->getContents());
            } catch (\Throwable $th) {
                DB::rollBack();
                Log::info($th);
                // throw $th;
                return getNotifApi('-', '-', 'errors');
            }
        }
        else{
            forApi('success', 'Bypass success');
        }
    }

    public function verify_family_card(Request $request)
    {
        $qouta = $this->quota()->data[1]->remainingAccess;

        if (!$qouta) {
            return response()->json([
                "data" => [
                    "family_id" => true
                ],
            ]);
        }

        if (strlen($request->nik) != 16 || strlen($request->kk) != 16) {
            return response()->json([
                "data" => [
                    "family_id" => true
                ],
            ]);
        }

        try {
            DB::beginTransaction();
            $data = new TransCallApi;
            $data->type = "verify_family_card";
            $data->date = date('Y-m-d');
            $data->save();
            $headers = [
                'Content-Type' => 'application/json',
                'Token' => $this->tokenDigiData
            ];

            $client = new GuzzleClient([
                'headers' => $headers
            ]);
            $response = $client->post('https://api.digidata.ai/trondigi_prod/verify_family_card', [
                'json' => [
                    "trx_id" => '01',
                    "nik" => $request->nik,
                    "family_id" => $request->kk
                ]
            ]);
            DB::commit();
            Log::info('berhasil verify_family_card');
            // Log::info($response);
            return response()->json(json_decode($response->getBody()->getContents()));
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            // throw $th;
            return getNotifApi('-', '-', 'errors');
        }
    }

    public function verify_identity(Request $request)
    {
        $bypass = false;

        if ($bypass) {
            return response()->json([
                "data" => true
            ]);
        }
        try {
            DB::beginTransaction();
            $data = new TransCallApi;
            $data->type = "digidata-orc-extra";
            $data->date = date('Y-m-d');
            $data->save();
            $headers = [
                'Content-Type' => 'application/json',
                'Token' => $this->tokenDigiData
            ];

            $client = new GuzzleClient([
                'headers' => $headers
            ]);
            $response = $client->post('https://api.digidata.ai/trondigi_prod/verify_identity', [
                'json' => [
                    "trx_id" => "",
                    "nik" => $request->nik,
                    "name" => $request->name,
                    "birthdate" => date('d-m-Y', strtotime($request->birthdate)),
                    "birthplace" => $request->birthplace,
                    "address" => $request->address
                ]
            ]);
            DB::commit();
            Log::info('berhasil verify_identity');
            // Log::info($response->getBody()->getContents());
            return response()->json(json_decode($response->getBody()->getContents()));
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            // throw $th;
            return getNotifApi('-', '-', 'errors');
        }
    }
}
