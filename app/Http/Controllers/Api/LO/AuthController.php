<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterAdmin;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterLO;
use App\Models\Master\MasterTruck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    private $slug = 'auth.lo';

    /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth("lo")->attempt($validator->validated())) {
            return getNotifApi($this->slug, 'login', 'errors');
        }

        if (!MasterLO::where('email', $request->email)->where('active', 1)->first()) {
            return response()->json([
                "success" => false,
                "data" => null,
                "notification" => [
                    "title" => "Opps",
                    "message" => "Your account has been deactived"
                ],
            ]);
        }

        $data = MasterLO::where('email', $request->email)->where('active', 1)->first();
        if ($data->type == 'bus') {
            $vehicle = MasterBus::with('trip.city')->find($data->bus_id);
        } else {
            $vehicle = MasterTruck::with('trip.city')->find($data->truck_id);
        }

        if (!$vehicle) {
            return setCustomError('Maaf Anda belum tekoneksi dengan bus ataupun truck, silahkan hubungi admin');    
        }

        return getNotifApi($this->slug, 'login', 'success', $this->respondWithToken($token)->original);
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth("lo")->logout();

        return getNotifApi($this->slug, 'logout', 'success');
    }

    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        $data = MasterLO::find(auth('lo')->user()->id);
        if ($data->type == 'bus') {
            $vehicle = MasterBus::with('trip.city')->find($data->bus_id);
        } else {
            $vehicle = MasterTruck::with('trip.city')->find($data->truck_id);
        }
        if ($data) {
            return getNotifApi($this->slug, 'profile', 'success', ["user" => $data, "vehicle_code" => $vehicle?->code, 'city' => $vehicle?->trip?->city?->name]);
        }
        return getNotifApi($this->slug, 'login', 'errors');
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            // 'expires_in' => auth('lo')->factory()->getTTL() * 60
            // 'expires_in' => Carbon::now()->addDays(7)->timestamp
        ]);
    }
}
