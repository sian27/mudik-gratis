<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterLO;
use App\Models\Master\MasterRute;
use App\Models\Master\MasterTruck;
use App\Models\Trans\TransReport;
use App\Models\Trans\TransRute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckpointController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'type' => 'nullable|in:start,end'
        ]);

        $auth = MasterLO::with('bus', 'truck')->find(auth('lo')->user()->id);

        if ($auth->type == 'bus') {
            $vehicle = $auth->bus;
        } else {
            $vehicle = $auth->truck;
        }

        if (!$vehicle) {
            return setCustomError('Anda belum terikat dengan bus ataupun truck, Silahkan hubungi admin');
        }

        $status = $vehicle->status;
        if (!empty($request->type)) {
            $status = $request->type;
        }

        $data = MasterRute::where('trip_id', $vehicle->trip_id);
        if ($status == 'start') {
            $data = $data->orderBy('order', 'asc');
            $data = $data->with('checkpoint', function ($q) use ($auth, $vehicle) {
                $q->where('status', 'start');
                if ($auth->type == 'bus') {
                    $q->where('bus_id', $vehicle->id);
                } else {
                    $q->where('truck_id', $vehicle->id);
                }
            });
        } else {
            $data = $data->orderBy('order', 'desc');
            $data = $data->with('checkpoint', function ($q) use ($auth, $vehicle) {
                $q->where('status', 'end');
                if ($auth->type == 'bus') {
                    $q->where('bus_id', $vehicle->id);
                } else {
                    $q->where('truck_id', $vehicle->id);
                }
            });
        }

        $data = $data->get();

        return getNotifApi('', '', 'success', ['type' => $status, 'rute' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "rute_id" => 'required|integer',
            'type' => 'required|in:start,end',
            'longitude' => 'required',
            'latitude' => 'required',
        ]);

        $auth = MasterLO::with('bus.trip', 'truck.trip')->find(auth('lo')->user()->id);

        if ($auth->type == 'bus') {
            $vehicle = $auth->bus;
        } else {
            $vehicle = $auth->truck;
        }

        if ($request->type == 'end' && $vehicle->status == 'start') {
            return setCustomError('Harap selesaikan rute mudik terlebih dahulu');
        }

        $rute = MasterRute::find($request->rute_id);
        if (!$rute) {
            return getNotifApi('', '', 'not_found');
        }

        $last_rute = MasterRute::where('trip_id', $vehicle->trip->id)->with('checkpoint', function ($q) use ($auth, $vehicle, $request) {
            $q->where($auth->type == 'bus' ? 'bus_id' : 'truck_id', $vehicle->id)->where('status', $request->type);
        })->orderBy('order', 'asc')->get();
        $last_rute_id = null;
        $last_current_rute_id = null;

        $j = count($last_rute) - 1;
        foreach ($last_rute as $i => $item) {
            if ($j == $i) {
                $last_rute_id = true;
                $last_current_rute_id = $item->id;
                break;
            } else {
                if ($item->checkpoint == null) {
                    $last_current_rute_id = $item->id;
                    break;
                }
            }
        }

        try {
            DB::beginTransaction();
            $data = new TransRute();
            $data->rute_id = $request->rute_id;
            if ($auth->type == 'bus') {
                $data->bus_id = $vehicle->id;
            } else {
                $data->truck_id = $vehicle->id;
            }
            $data->status = $request->type;
            $data->longitude = $request->longitude;
            $data->latitude = $request->latitude;

            if ($auth->type == 'bus') {
                $vehicle = MasterBus::find($vehicle->id);
            } else {
                $vehicle = MasterTruck::find($vehicle->id);
            }

            $vehicle->longitude = $request->longitude;
            $vehicle->latitude = $request->latitude;

            if ($last_current_rute_id && $last_current_rute_id != $request->rute_id) {
                return setCustomError('Maaf, harap pastikan checkpoint sebelumnya sudah di check dan mulailah dari km terendah');
            }
            if ($last_rute_id) {
                $report = new TransReport();
                $report->vehicle_code = $vehicle->name;
                $report->date = date('Y-m-d');
                $report->time = date('H:i');
                $report->status = 'arrive';
                $report->type = auth('lo')->user()->type;
                $report->lo_id = auth('lo')->user()->id;
                $report->is_done = 'yes';
                $report->description = 'Telah tiba';
                $report->longitude = $request->longitude;
                $report->latitude = $request->latitude;
                $report->address = $rute->name ?? '-';
                $report->save();
                $vehicle->status = 'end';
            } else {
                $report = new TransReport();
                $report->vehicle_code = $vehicle->name;
                $report->date = date('Y-m-d');
                $report->time = date('H:i');
                $report->status = 'ongoing';
                $report->type = auth('lo')->user()->type;
                $report->lo_id = auth('lo')->user()->id;
                $report->is_done = 'yes';
                $report->description = 'Dalam Perjalanan';
                $report->longitude = $request->longitude;
                $report->latitude = $request->latitude;
                $report->address = $rute->name ?? '-';
                $report->save();
            }
            $data->save();
            $vehicle->save();

            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
