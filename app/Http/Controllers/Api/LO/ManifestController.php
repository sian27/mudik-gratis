<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterLO;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManifestController extends Controller
{
    public function index(Request $request)
    {
        $auth = MasterLO::with('bus', 'truck')->find(auth('lo')->user()->id);

        $data = MasterPassenger::where('status', 'accept');
        if ($auth->type == 'bus') {
            if (str_contains($auth->email, 'lo_verifikasi')) {
                $data = $data->whereHas('busMudik', function ($q) use ($auth) {
                    $q->whereHas('bus', function ($j) use ($auth) {
                        $j->where('trip_id', $auth->bus->trip_id);
                    });
                });
            } else {
                if (date('Y-m-d') < date('Y-m-d', strtotime('2022-05-06'))) {
                    $data = $data->whereHas('busMudik', function ($q) use ($auth) {
                        $q->where('bus_id', $auth->bus->id);
                    });
                } else {
                    $data = $data->whereHas('busBalik', function ($q) use ($auth) {
                        $q->where('bus_id', $auth->bus->id);
                    });
                }
            }
        } else {
            if (str_contains($auth->email, 'lo_verifikasi')) {
                $data = $data->whereHas('truckMudik', function ($q) use ($auth) {
                    $q->whereHas('truck', function ($j) use ($auth) {
                        $j->where('trip_id', $auth->truck->trip_id);
                    });
                });
            } else {
                if (date('Y-m-d') < date('Y-m-d', strtotime('2022-05-06'))) {
                    $data = $data->whereHas('truckMudik', function ($q) use ($auth) {
                        $q->where('truck_id', $auth->truck->id);
                    });
                } else {
                    $data = $data->whereHas('truckBalik', function ($q) use ($auth) {
                        $q->where('truck_id', $auth->truck->id);
                    });
                }
            }
        }
        if ($request->sorter == 'name') {
            $data = $data->orderBy('name', $request->sorter_type);
        } else if ($request->sorter == 'date') {
            $data = $data->orderBy('id', $request->sorter_type);
        }
        $data = $data->where('scan_booking', '1')->whereNull('parent_id');

        if ($request->search) {
            $data = $data->where(function ($query) use ($request) {
                $query
                    ->orWhere('name', 'ilike', '%' . $request->search . '%')
                    ->orWhereHas('mudik', function ($q) use ($request) {
                        $q->where('code', 'ilike', '%' . $request->search . '%');
                    })
                    ->orWhereHas('trip.city', function ($q) use ($request) {
                        $q->where('name', 'ilike', '%' . $request->search . '%');
                    });
            })
                ->with('mudik.trip.city');
        } else {
            $data = $data->with('mudik.trip.city');
        }

        $data = $data->get();

        return getNotifApi('', '', 'success', ['manifest' => $data]);
    }

    public function show($id)
    {
        $data = MasterPassenger::select("*")->with('mudik', 'busMudik.bus', 'busBalik.bus', 'truckMudik.truck', 'truckBalik.truck', 'trip.city')
            ->with(['member.busMudik.bus', 'member.busBalik.bus', 'vehicle'])->where('scan_booking', '1')->find($id);

        if ($data) {
            return getNotifApi('', '', 'success', ['manifest' => $data]);
        }
        return getNotifApi('', '', 'not_found');
    }
}
