<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterLO;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'min:3',
            'email' => 'email:rfc,dns',
            'phone' => 'min:9|max:15',
            'password' => 'nullable|min:8',
            're_password' => 'same:password',
            'image' => ['mimes:jpeg,jpg,png', 'file', 'max:5120'],
        ]);

        $data = MasterLO::find(auth('lo')->user()->id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        $image = $request->file("image");
        $image_old = $data->image_path;
        try {
            $data->name = $request->name;
            $data->email = $request->email;
            if ($data->password) {
                $data->password = bcrypt($request->password);
            }
            $data->phone = $request->phone;
            if ($image) {
                $imageName = 'upload/lo/' . nameFile($image);
                $image->move(public_path('upload/lo'), $imageName);
                $data->image = $imageName;
            }
            $data->save();

            deletedFile($image_old);

            return getNotifApi('','','success');
        } catch (\Throwable $th) {
            // throw $th
            return getNotifApi('','','errors');
        }
    }
}
