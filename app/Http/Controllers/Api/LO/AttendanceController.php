<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterTruck;
use App\Models\Trans\TransAttendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{

    public function index()
    {
        $auth = auth("lo")->user();
        $data = TransAttendance::where("lo_id", $auth->id)
            ->orderBy('created_at', "desc")
            ->get();

        return getNotifApi('', '', 'success', ['attendance' => $data]);
    }

    public function show($id)
    {
        $data = TransAttendance::find($id);
        if ($data) {
            return getNotifApi('', '', 'success', ['attendance' => $data]);
        }
        return getNotifApi('', '', 'not_found');
    }

    public function check()
    {
        $auth = auth("lo")->user();
        $check_in = TransAttendance::where("lo_id", $auth->id)->where("check_in", "like", date("Y-m-d") . "%")->first();
        $check_out = TransAttendance::where("lo_id", $auth->id)->where("check_in", "like", date("Y-m-d") . "%")->where("check_out", "!=", null)->first();

        return getNotifApi('-', '-', 'success', [
            "check_in" => $check_in ? true : false,
            "check_out" => $check_out ? true : false,
            "detail" => $check_in
        ]);
    }
    public function check_in(Request $request)
    {
        $request->validate([
            'longitude' => 'required',
            'latitude' => 'required',
            'address' => 'required',
            'date' => 'required',
            "time" => 'required',
            'image' => ['mimes:jpeg,jpg,png', 'file', 'max:5120'],
        ]);

        $auth = auth("lo")->user();

        $taked = TransAttendance::where("lo_id", $auth->id)->where("check_in", "like", date("Y-m-d", strtotime($request->date)) . "%")->first();

        if ($taked) {
            return setCustomError('Maaf, hari ini kamu sudah melakukan Check In');
        }

        $image = $request->file("image");

        try {
            DB::beginTransaction();
            $data = new TransAttendance;
            $data->lo_id = $auth->id;
            $data->start_longitude = $request->longitude;
            $data->start_latitude = $request->latitude;
            $data->start_address = $request->address;
            if ($image) {
                $imageName = 'upload/attendance/' . nameFile($image);
                $image->move(public_path('upload/attendance'), $imageName);
                $data->start_image = $imageName;
            }

            $data->check_in = date("Y-m-d H:i", strtotime($request->date . " " . $request->time));
            $data->save();

            $auth = auth('lo')->user();
            if ($auth->type == 'bus') {
                MasterBus::where('id', $auth->bus_id)->update([
                    'longitude' => $request->longitude,
                    'latitude' => $request->latitude,
                ]);
            } else {
                MasterTruck::where('id', $auth->truck_id)->update([
                    'longitude' => $request->longitude,
                    'latitude' => $request->latitude,
                ]);
            }

            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function check_out(Request $request)
    {
        $request->validate([
            'longitude' => 'required',
            'latitude' => 'required',
            'address' => 'required',
            'date' => 'required',
            "time" => 'required',
            'image' => ['mimes:jpeg,jpg,png', 'file', 'max:5120'],
        ]);

        $auth = auth("lo")->user();

        $check_in = TransAttendance::where("lo_id", $auth->id)->where("check_in", "like", date("Y-m-d", strtotime($request->date)) . "%")->first();

        if (!$check_in) {
            return setCustomError('Maaf, silahkan lakukan Check In terlebih dahulu');
        }

        $taked = TransAttendance::where("lo_id", $auth->id)->where("check_in", "like", date("Y-m-d", strtotime($request->date)) . "%")->where("check_out", "!=", null)->first();
        if ($taked) {
            return setCustomError('Maaf, hari ini kamu sudah melakukan Check Out');
        }

        $image = $request->file("image");

        try {
            DB::beginTransaction();
            $data = TransAttendance::where("lo_id", $auth->id)->where("check_in", "like", date("Y-m-d", strtotime($request->date)) . "%")->first();
            $data->end_longitude = $request->longitude;
            $data->end_latitude = $request->latitude;
            $data->end_address = $request->address;
            $data->check_out = date("Y-m-d H:i", strtotime($request->date . " " . $request->time));
            if ($image) {
                $imageName = 'upload/attendance/' . nameFile($image);
                $image->move(public_path('upload/attendance'), $imageName);
                $data->end_image = $imageName;
            }
            $data->save();
            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
