<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterSupport;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function index()
    {
        $data = MasterSupport::get();

        return getNotifApi('', '', 'success', ['support' => $data]);
    }
}
