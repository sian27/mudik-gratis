<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Trans\TransReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $data = TransReport::select('t_report.*')->with('lo')->join('m_lo as lo', 't_report.lo_id', '=', 'lo.id');
        if ($request->search) {
            $data = $data->where(function ($query) use ($request) {
                $query->orWhere('vehicle_code', 'ilike', '%' . $request->search . '%')->orWhereHas('lo', function ($q) use ($request) {
                    $q->where('name', 'ilike', '%' . $request->search . '%');
                });
            });
        }

        if ($request->sorter == 'name') {
            $data = $data->orderBy('lo.name', $request->sorter_type);
        } else if ($request->sorter == 'date') {
            $data = $data->orderBy('id', $request->sorter_type);
        } else {
            $data = $data->orderBy('id', 'desc');
        }

        $data = $data->where('status', '!=', 'ongoing')->get();

        return getNotifApi('', '', 'success', ['report' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'vehicle_code' => 'required',
            'date' => 'required',
            'time' => 'required',
            'status' => 'required|in:rest,trouble',
            'description' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'address' => 'required',
            'is_done' => 'required|in:yes,not_yet'
        ]);

        try {
            $data = new TransReport();
            $data->vehicle_code = $request->vehicle_code;
            $data->date = date('Y-m-d', strtotime($request->date));
            $data->time = date('H:i', strtotime($request->time));
            $data->status = $request->status;
            $data->type = auth('lo')->user()->type;
            $data->lo_id = auth('lo')->user()->id;
            $data->is_done = $request->is_done;
            $data->description = $request->description;
            $data->longitude = $request->longitude;
            $data->latitude = $request->latitude;
            $data->address = $request->address;
            $data->save();

            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function show($id)
    {

        $data = TransReport::with('lo')->find($id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }
        return getNotifApi('', '', 'success', ['report' => $data]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            // 'vehicle_code' => 'required',
            // 'date' => 'required',
            // 'time' => 'required',
            // 'status' => 'required|in:rest,trouble',
            // 'description' => 'required',
            // 'longitude' => 'required',
            // 'latitude' => 'required',
            // 'address' => 'required',
            'is_done' => 'required|in:yes,not_yet'
        ]);

        $data = TransReport::find($id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        try {
            DB::beginTransaction();
            // $data->vehicle_code = $request->vehicle_code;
            // $data->date = date('Y-m-d', strtotime($request->date));
            // $data->time = date('H:i', strtotime($request->time));
            // $data->status = $request->status;
            $data->is_done = $request->is_done;
            // $data->type = auth('lo')->user()->type;
            // $data->lo_id = auth('lo')->user()->id;
            // $data->description = $request->description;
            // $data->longitude = $request->longitude;
            // $data->latitude = $request->latitude;
            // $data->address = $request->address;
            $data->save();

            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
