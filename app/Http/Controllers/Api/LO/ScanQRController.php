<?php

namespace App\Http\Controllers\Api\LO;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScanQRController extends Controller
{
    public function get(Request $request)
    {
        $request->validate([
            "key" => 'required',
        ]);

        $decrypt = decrypt($request->key);
        $key = explode(",", $decrypt);

        $id = $key[0];
        $type = $key[1];
        $category = $key[2];

        try {
            DB::beginTransaction();
            $data = MasterPassenger::with('vehicle', 'mudik', 'busMudik.bus', 'busBalik.bus', 'truckMudik.truck', 'truckBalik.truck', 'trip.city')
                ->with(['member.busMudik.bus', 'member.busBalik.bus'])->find($id);
            if (!$data) {
                return getNotifApi('', '', 'not_found');
            }
            if ($category == 'passenger') {
                if ($type == 'booking') {
                    if (!$data->scan_booking) {
                        $data->scan_booking = 1;
                        $data->send_ticket = 0;
                    }
                } else if ($type == 'ticket') {
                    if (!$data->scan_ticket) {
                        $data->scan_ticket = 1;
                        $data->update_at = date('Y-m-d H:i:s');
                    } else {
                        $data->scan_tiket_balik = 1;
                    }
                }
                $data->save();
            } else {
                $vehicle = MasterPassengerVehicle::with('passenger.mudik.detailBus', 'passenger.mudik.detailTruck', 'trip.city')->where('passenger_id', $id)->first();
                if ($type == 'booking') {
                    if (!$vehicle->scan_booking) {
                        $vehicle->scan_booking = 1;
                        $vehicle->send_ticket = 0;
                    }
                } else {
                    if (!$vehicle->scan_ticket) {
                        $vehicle->scan_ticket = 1;
                    } else {
                        $data->scan_tiket_balik = 1;
                    }
                }
                $vehicle->save();
            }

            DB::commit();

            return getNotifApi('', '', 'success', ['type' => $type, 'category' => $category, 'result' => $data]);
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function ticket(Request $request)
    {
        $request->validate([
            "key" => 'required',
            'type' => 'required|in:passenger,vehicle'
        ]);

        $id = decrypt($request->key);

        try {
            DB::beginTransaction();
            DB::commit();
            if ($request->type == 'passenger') {

                $data = MasterPassenger::with('vehicle', 'member')->find($id);
                if (!$data) {
                    return getNotifApi('', '', 'not_found');
                }
                $data->scan_ticket = 1;
                $data->save();
            } else {
                $data = MasterPassengerVehicle::where('passenger_id', $id)->first();
                if (!$data) {
                    return getNotifApi('', '', 'not_found');
                }
                $data->scan_ticket = 1;
                $data->save();
            }
            return getNotifApi('', '', 'success', ['result' => $data]);
        } catch (\Throwable $th) {
            DB::rollBack();
            //throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
