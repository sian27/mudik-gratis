<?php

namespace App\Http\Controllers\Api\Khusus;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterSeatBus;
use App\Models\Master\MasterSeatTruck;
use App\Models\Master\MasterTrip;
use App\Models\Master\MasterTruck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReshuffleController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'passenger_id' => 'required',
            'split' => 'nullable',
        ]);

        try {
            DB::beginTransaction();


            $pass = MasterPassenger::select('*')->with('member')->find($request->passenger_id);

            $pass_id = [];
            array_push($pass_id, $pass->id);

            foreach ($pass->member as $item) {
                array_push($pass_id, $item->id);
            }

            $trip_id = $pass->trip_id;


            // pelembang
            if ($pass->trip_id == 34) {
                $trip_id = 36;
            }
            // lampung
            else if ($pass->trip_id == 38) {
                $trip_id = 39;
            }
            // cilacap
            else if ($pass->trip_id == 23) {
                $trip_id = 24;
            }
            // purwokerto 
            else if ($pass->trip_id == 19) {
                $trip_id = 20;
            }
            // wonosobo
            else if ($pass->trip_id == 13) {
                $trip_id = 16;
            }

            $allBus = MasterBus::where('trip_id', $trip_id)->orderBy('id', 'asc')->get();
            $trip = MasterTrip::find($trip_id);

            if ($pass->motor) {
                $seat_truck = MasterSeatTruck::whereNull('passenger_mudik')->whereHas('truck', function ($q) use ($trip_id) {
                    $q->where('trip_id', $trip_id);
                })->orderBy('id', 'asc')->first();
                if (!$seat_truck) {
                    return setCustomError('Kuota Truck Habis');
                }
                $this->store_truck($trip->type, $trip_id, $pass_id[0]);
            }

            $estimate = [];
            $success = [];
            foreach ($allBus as $bus) {
                $quota = $bus->quota - $bus->khusus;
                if ($request->flag == 'khusus') {
                    $quota = $bus->khusus;
                }
                $check = MasterSeatBus::where('bus_id', $bus->id)->whereNotNull('passenger_mudik')->orderBy('id', 'asc');
                $seat = MasterSeatBus::where('bus_id', $bus->id)->limit($bus->quota)->orderBy('id', 'asc')->get();
                $current = [];
                foreach ($seat as $sub) {
                    if (!$sub->passenger_mudik) {
                        array_push($current, $sub->id);
                    }
                }
                $check = $check->whereHas('passenger_mudik', function ($q) use ($request) {
                    if ($request->flag == 'khusus') {
                        $q->where('flag', 'khusus');
                    } else {
                        $q->where(function ($j) {
                            $j->where('flag', '!=', 'khusus')->orWhereNull('flag');
                        });
                    }
                });
                // $current = $current->whereHas('passenger_mudik', function ($q) use ($request) {
                //     if ($request->flag == 'khusus') {
                //         $q->where('flag', 'khusus');
                //     } else {
                //         $q->where(function ($j) {
                //             $j->where('flag', '!=', 'khusus')->orWhereNull('flag');
                //         });
                //     }
                // });
                $check = $check->get();
                if ($quota > count($check)) {
                    if ($request->split) {
                        for ($i = 0; $i < count($pass_id); $i += $request->split) {
                            $split = array_slice($pass_id, $i, $request->split);
                            if (count($current) >= count($split)) {
                                foreach ($split as $item) {
                                    $this->store_bus($trip->type, $bus->id, $item);
                                }
                                array_push($success, true);
                            } else {
                                array_push($estimate, ["name_bus" => $bus->name, 'available' => count($current)]);
                                array_push($success, false);
                            }
                        }
                        if (in_array(true, $success)) {
                            break;
                        }
                    } else {
                        // count  current = 2, pass_id = 4
                        if (count($current) >= count($pass_id)) {
                            foreach ($pass_id as $item) {
                                $this->store_bus($trip->type, $bus->id, $item);
                            }
                            array_push($success, true);
                            break;
                        } else {
                            array_push($estimate, ["name_bus" => $bus->name, 'available' => count($current)]);
                            array_push($success, false);
                        }
                    }
                }
            }

            // return setCustomError('erorr', 400, $success);

            if (!in_array(true, $success)) {
                return setCustomError('Kuota Bus Habis', 400, ['estimate' => $estimate]);
            }
            DB::commit();

            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors', $th);
        }
    }

    public function store_bus($type, $bus_id, $pass_id)
    {
        if ($type == "mudik-saja") {
            $seat_mudik = MasterSeatBus::where('bus_id', $bus_id)->whereNull('passenger_mudik')->orderBy('id', 'asc')->first();
            $seat_mudik->passenger_mudik = $pass_id;
            $seat_mudik->save();
        } else {
            $seat_mudik = MasterSeatBus::where('bus_id', $bus_id)->whereNull('passenger_mudik')->orderBy('id', 'asc')->first();
            $seat_balik = MasterSeatBus::where('bus_id', $bus_id)->whereNull('passenger_balik')->orderBy('id', 'asc')->first();
            $seat_mudik->passenger_mudik = $pass_id;
            $seat_mudik->save();
            $seat_balik->passenger_balik = $pass_id;
            $seat_balik->save();
        }
    }

    public function store_truck($type, $trip_id, $pass_id)
    {
        if ($type == "mudik-saja") {
            $seat_mudik = MasterSeatTruck::whereNull('passenger_mudik')->whereHas('truck', function ($q) use ($trip_id) {
                $q->where('trip_id', $trip_id);
            })->orderBy('id', 'asc')->first();
            $seat_mudik->passenger_mudik = $pass_id;
            $seat_mudik->save();
        } else {
            $seat_mudik = MasterSeatTruck::whereNull('passenger_mudik')->whereHas('truck', function ($q) use ($trip_id) {
                $q->where('trip_id', $trip_id);
            })->orderBy('id', 'asc')->first();
            $seat_balik = MasterSeatTruck::whereNull('passenger_balik')->whereHas('truck', function ($q) use ($trip_id) {
                $q->where('trip_id', $trip_id);
            })->orderBy('id', 'asc')->first();
            $seat_mudik->passenger_mudik = $pass_id;
            $seat_mudik->save();
            $seat_balik->passenger_balik = $pass_id;
            $seat_balik->save();
        }
    }
}
