<?php

namespace App\Http\Controllers\Api\Khusus;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterTrip;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MigrationController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'passenger_id' => 'required',
            'city_id' => 'required',
        ]);

        $pass = MasterPassenger::with('member')->with('vehicle')->find($request->passenger_id);

        if (!$pass) {
            return setCustomError('Data Passenger Tidak Ada');
        }

        $newtrip = MasterTrip::where('city_id', $request->city_id)->where('type', 'mudik-balik')->first();
        if (!$newtrip) {
            return setCustomError('Data Trip Tidak Ada');
        }

        try {
            DB::beginTransaction();

            $pass->migrasi = 1;
            $pass->trip_id = $newtrip->id;
            $pass->save();

            $newmudik = TransMudik::where('passenger_id', $pass->id)->first();
            $newmudik->trip_id = $newtrip->id;
            $newmudik->migrasi = 1;
            $newmudik->save();

            $balik = TransBalik::where('passenger_id', $pass->id)->first();
            if (!$balik) {
                $code = '';
                $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                $code .= generateCode($newtrip->id, 2);
                $code .= generateCode($pass->id, 4);

                $newbalik = new TransBalik;
                $newbalik->trip_id = $newtrip->id;
                $newbalik->code = $code;
                $newbalik->passenger_id = $pass->id;
                $newbalik->bus_id = 1;
                $newbalik->bus_card = '00';
                $newbalik->migrasi = 1;
                $newbalik->save();
            }

            foreach ($pass->member as $item) {
                $member = MasterPassenger::find($item->id);
                $member->migrasi = 1;
                $member->trip_id = $newtrip->id;
                $member->save();

                $newmudik = TransMudik::where('passenger_id', $member->id)->first();
                if (!$newmudik) {
                    $code = '';
                    $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                    $code .= generateCode($newtrip->id, 2);
                    $code .= generateCode($member->id, 4);

                    $newmudik = new TransMudik;
                    $newmudik->trip_id = $newtrip->id;
                    $newmudik->code = $code;
                    $newmudik->bus_id = 1;
                    $newmudik->passenger_id = $member->id;
                    $newmudik->bus_card = '00';
                    $newmudik->migrasi = 1;
                    $newmudik->save();
                }
                $newmudik->trip_id = $newtrip->id;
                $newmudik->migrasi = 1;
                $newmudik->save();

                $balik = TransBalik::where('passenger_id', $member->id)->first();
                if (!$balik) {
                    $code = '';
                    $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                    $code .= generateCode($newtrip->id, 2);
                    $code .= generateCode($member->id, 4);

                    $newbalik = new TransBalik;
                    $newbalik->trip_id = $newtrip->id;
                    $newbalik->code = $code;
                    $newbalik->bus_id = 1;
                    $newbalik->passenger_id = $member->id;
                    $newbalik->bus_card = '00';
                    $newbalik->migrasi = 1;
                    $newbalik->save();
                }
            }

            if ($pass->motor) {
                $vehicle = MasterPassengerVehicle::where('passenger_id', $pass->id)->first();
                $vehicle->migrasi = 1;
                $vehicle->trip_id = $newtrip->id;
                $vehicle->save();
            }

            DB::commit();
            return forApi('success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors', $th);
        }
    }
}
