<?php

namespace App\Http\Controllers\Api\Khusus;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PassengerController extends Controller
{
    public function accept(Request $request)
    {
        $request->validate([
            "passenger_id" => 'required|integer',
        ]);

        $data = MasterPassenger::with('member', 'mudik', 'balik')->find($request->passenger_id);
        if (!$data) {
            return getNotifApi('', '', 'not_found');
        }

        $vehicle = MasterPassengerVehicle::where('passenger_id', $request->passenger_id)->first();

        try {
            DB::beginTransaction();
            $data->status = 'accept';
            $data->send_booking = 0;
            $data->save();

            foreach ($data->member as $item) {
                $item->status = 'accept';
                // $item->send_booking = 0;
                $item->save();
            }

            if ($vehicle) {
                $vehicle->send_booking = 0;
                $vehicle->save();
            }

            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
