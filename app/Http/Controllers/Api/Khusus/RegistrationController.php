<?php

namespace App\Http\Controllers\Api\Khusus;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterTrip;
use App\Models\Master\MasterTruck;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegistrationController extends Controller
{
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'email' => 'required|email',
                // 'nik' => 'required|max:16|min:16',
                // 'no_kk' => 'required|max:16|min:16',
                // 'gender' => 'required|in:p,l',
                // 'phone' => 'required|max:16|min:9',
                // 'datebirth' => 'required',
                // 'placebirth' => 'required',
                // 'address' => 'required',
                // 'trip_id' => 'required',
                // 'motor' => 'required',
                // 'file_ktp' => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
                // 'file_kk' => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120'],
                // 'vaksin' => 'required|in:green,yellow,red',
            ],
        );

        // if ($request->quota > 4) {
        $request->quota = 4;
        // }

        // $file_booster = null;

        // if (!$request->file_booster) {
        //     $request->validate([
        //         "type_vaksin" => 'required|in:booster,pcr,antigen',
        //         "puskes_id" => 'required|integer'
        //     ]);
        // } else {
        //     $request->validate([
        //         "file_booster" => ['required', 'mimes:jpeg,jpg,png', 'file', 'max:5120']
        //     ]);
        // }
        $file_booster = $request->file('file_booster');

        $parent = MasterPassenger::with('member')->with('vehicle')->where('id', $request->base_id)->first();

        if (!$parent) {
            $trip = MasterTrip::find($request->trip_id);
            if (!$trip) {
                return forApi('errors', 'Kota Tujuan tidak ditemukan');
            }

            $bus = MasterBus::where('trip_id', $request->trip_id)->first();
            if ($request->motor) {
                $truck = MasterTruck::where('trip_id', $request->trip_id)->first();
                if ($truck) {
                    return forApi('errors', "Truck nya gak ada :p");
                }
            }

            if (!$bus) {
                return forApi('errors', "Bus nya gak ada :p");
            }

            try {
                DB::beginTransaction();

                // get file image
                $file_ktp = $request->file('file_ktp');
                $file_kk = $request->file('file_kk');

                $parent = new MasterPassenger;
                $parent->trip_id = $request->trip_id;
                $parent->nik = $request->nik;
                $parent->name = $request->name;
                $parent->email = $request->email;
                $parent->nik = $request->nik;
                $parent->no_kk = $request->no_kk;
                $parent->trip_id = $request->trip_id;
                $parent->gender = $request->gender;
                $parent->phone = $request->phone;
                $parent->placebirth = $request->placebirth;
                $parent->address = $request->address;
                $parent->datebirth = $request->datebirth;
                $parent->vaksin = $request->vaksin;
                $parent->motor = $request->motor;

                // uploading file         
                if ($file_ktp) {
                    $ktp = 'upload/passenger/' . nameFile($file_ktp);
                    $file_ktp->move(public_path('upload/passenger'), $ktp);
                    $parent->file_ktp = $ktp;
                }
                if ($file_kk) {
                    $kk = 'upload/passenger/' . nameFile($file_kk);
                    $file_kk->move(public_path('upload/passenger'), $kk);
                    $parent->file_kk = $kk;
                }

                if ($file_booster) {
                    $booster = 'upload/passenger/' . nameFile($file_booster);
                    $file_booster->move(public_path('upload/passenger'), $booster);
                    $parent->file_booster = $booster;
                } else {
                    $parent->type_vaksin = $request->type_vaksin;
                    $parent->puskes_id = $request->puskes_id;
                }
                $parent->flag = 'khusus';

                $parent->save();
                // $parent = MasterPassenger::with('mudik', 'balik')->find($parent->id);

                $code = '';
                $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                $code .= generateCode($trip->id, 2);
                $code .= generateCode($parent->id, 4);



                $mudik = new TransMudik;
                $mudik->code = $code;
                $mudik->passenger_id = $parent->id;
                $mudik->trip_id = $trip->id;
                $mudik->bus_id = $bus->id;
                $mudik->bus_card = generateCode(0, 1);
                $mudik->truck_id = $request->motor ?  $truck->id : null;
                $mudik->truck_card = $request->motor ?  generateCode(0, 1) : null;
                $mudik->save();

                if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                    $balik = new TransBalik;
                    $balik->code = $code;
                    $balik->passenger_id = $parent->id;
                    $balik->trip_id = $trip->id;
                    $balik->bus_id = $bus->id;
                    $balik->bus_card = generateCode(0, 1);
                    $balik->truck_id = $request->motor ?  $truck->id : null;
                    $balik->truck_card = $request->motor ?  generateCode(0, 1) : null;
                    $balik->save();
                }

                foreach (range(1, $request->quota - 1) as $i) {
                    $status = true;
                    // $list = ['name', 'placebirth', 'datebirth', 'nik', 'phone', 'email', 'gender'];
                    // foreach ($list as $item) {
                    //     $check = $item . '_family_' . $i;
                    //     if ($item === null) {
                    //     } else {
                    //         if (!$request[$check]) {
                    //             $status = false;
                    //         }
                    //     }
                    // }
                    if ($status) {

                        $sub = new MasterPassenger;
                        $sub->trip_id = $request->trip_id;
                        $sub->parent_id = $parent->id;
                        $file_booster_member = $request->file('file_booster_family_' . $i);
                        $sub->trip_id = $request->trip_id;
                        $sub->name = $request['name_family_' . $i];
                        $sub->placebirth = $request['placebirth_family_' . $i];
                        $sub->datebirth = $request['datebirth_family_' . $i];
                        $sub->nik = $request['nik_family_' . $i];
                        $sub->no_kk = $parent->no_kk;
                        $sub->phone = $request['phone_family_' . $i];
                        $sub->email = $request['email_family_' . $i];
                        $sub->gender = $request['gender_family_' . $i];
                        $sub->vaksin = $request['vaksin_family_' . $i];

                        if ($file_booster_member) {
                            $booster = 'upload/passenger/' . nameFile($file_booster_member);
                            $file_booster_member->move(public_path('upload/passenger'), $booster);
                            $sub->file_booster = $booster;
                        } else {
                            $sub->type_vaksin = $request['type_vaksin_family_' . $i];
                            $sub->puskes_id = $request['puskes_id_family_' . $i];
                        }
                        $sub->flag = 'khusus';

                        $i++;

                        $sub->save();


                        $code = '';
                        $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                        $code .= generateCode($trip->id, 2);
                        $code .= generateCode($sub->id, 4);

                        // if ($province == 'jakarta') {
                        $mudik = new TransMudik;
                        $mudik->code = $code;
                        $mudik->passenger_id = $sub->id;
                        $mudik->trip_id = $trip->id;
                        $mudik->bus_id = $bus->id;
                        $mudik->bus_card = generateCode(0 + $i, 1);
                        $mudik->truck_id = $request->motor ?  $truck->id : null;
                        $mudik->truck_card = $request->motor ?  generateCode(0, 1) : null;
                        $mudik->save();

                        if ($trip->type ==  'mudik-balik' || $trip->type == 'mudik-balik-motor') {
                            $balik = new TransBalik;
                            $balik->code = $code;
                            $balik->passenger_id = $sub->id;
                            $balik->trip_id = $trip->id;
                            $balik->bus_id = $bus->id;
                            $balik->bus_card = generateCode(0 + $i, 1);
                            $balik->truck_id = $request->motor ?  $truck->id : null;
                            $balik->truck_card = $request->motor ?  generateCode(0, 1) : null;
                            $balik->save();
                        }
                    }
                }

                if ($request->motor) {
                    // $request->validate([
                    //     'no_police' => 'required',
                    //     'merk_vehicle' => 'required',
                    //     'type_vehicle' => 'required',
                    //     'color_vehicle' => 'required',
                    //     'no_stnk' => 'required',
                    // ]);
                    // input date kendaraan
                    $vehicle  = new MasterPassengerVehicle;
                    $vehicle->passenger_id = $parent->id;
                    $vehicle->trip_id = $request->trip_id ?? $request->city_id;
                    $vehicle->no_police = $request->no_police;
                    $vehicle->merk_vehicle = $request->merk_vehicle;
                    $vehicle->type_vehicle = $request->type_vehicle;
                    $vehicle->color_vehicle = $request->color_vehicle;
                    $vehicle->no_stnk = $request->no_stnk;
                    $vehicle->owner_name = $parent->name;
                    $vehicle->owner_nik = $parent->nik;
                    $vehicle->owner_no_kk = $parent->no_kk;
                    $vehicle->owner_email = $parent->email;
                    $vehicle->owner_phone = $parent->phone;
                    $vehicle->flag = 'khusus';
                    $vehicle->save();
                }
                $child = MasterPassenger::where('parent_id', $parent->id)->whereNull('name')->whereNull('email')->get();
                foreach($child as $sub){
                    TransMudik::where('passenger_id', $sub->id)->delete();
                    TransBalik::where('passenger_id', $sub->id)->delete();
                    $sub->delete();                    
                }

                DB::commit();
                return forApi('success');
            } catch (\Throwable $th) {
                DB::rollBack();
                // throw $th;
                return forApi('errors');
            }
        }
    }
}
