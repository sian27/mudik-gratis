<?php

namespace App\Http\Controllers\Api\Other;

use App\Http\Controllers\Controller;
use App\Models\Trans\TransBalik;
use Illuminate\Http\Request;

class GhostController extends Controller
{
    public function check(Request $request)
    {
        $data = TransBalik::select('*')->where('trip_id', $request->trip_id)->whereHas('detailPassenger', function ($q) {
            $q->whereNotNull('email');
        })->with('detailPassenger')->get();

        return forApi('success', '', ['check' => $data]);
    }
}
