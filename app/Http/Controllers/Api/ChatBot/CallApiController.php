<?php

namespace App\Http\Controllers\Api\ChatBot;

use App\Http\Controllers\Controller;
use App\Models\Trans\TransCallApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CallApiController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|in:check-vaksin,orc-extra,orc-extra,verify-identify'
        ]);
        
        try {
            DB::beginTransaction();
            $data = new TransCallApi;
            $data->type = $request->type;
            $data->date = date('Y-m-d');
            DB::commit();
            return getNotifApi('-', '-', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            //throw $th;
            return getNotifApi('-', '-', 'errors');
        }
    }
}
