<?php

namespace App\Http\Controllers\Api\ChatBot;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterTrip;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegistrationController extends Controller
{
    public function booking(Request $request)
    {
        $request->validate([
            "trip_id" => "required",
            "nik" => "required|min:16|max:16",
            "slot" => "required|integer",
            'province' => 'required'
        ]);

        if ($request->slot > 4) {
            $request->slot = 4;
        }

        $trip = MasterTrip::find($request->trip_id);
        if (!$trip) {
            return setCustomError('Kota Tujuan tidak ditemukan');
        }

        $bus = availableBus($trip, $request->slot);
        if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
            $truck = availableTruck($trip, $request->slot);
            if (!$truck['id']) {
                return setCustomError("Yahh kamu kurang cepat, Kuota Truck Mudik/Balik Anda saat ini hanya tersisa " . $bus['estimate'][0] . ". Silahkan pilih yang tanpa motor jika ada", 400, ['estimate' => 0, 'type' => 'truck']);
            }
        }

        if (!$bus['id']) {
            return setCustomError("Yahh kamu kurang cepat, Kuota Bus Mudik/Balik Anda saat ini hanya tersisa " . $bus['estimate'][0], 400, ['estimate' => $bus['estimate'][0], "type" => 'bus']);
        }

        try {
            DB::beginTransaction();


            $parent = new MasterPassenger();
            $parent->trip_id = $request->trip_id;
            $parent->nik = $request->nik;
            $parent->save();

            $parent = MasterPassenger::with('member', 'mudik', 'balik')->find($parent->id);

            $code = '';
            $code = strtoupper(chr(rand(ord('a'), ord('z'))));
            $code .= generateCode($trip->id, 2);
            $code .= generateCode($parent->id, 4);

            $province = strtolower($request->province);

            // if ($province == 'jakarta') {
            $mudik = new TransMudik;
            $mudik->code = $code;
            $mudik->passenger_id = $parent->id;
            $mudik->trip_id = $trip->id;
            $mudik->bus_id = $bus['id'];
            $mudik->bus_card = generateCode($bus['card'], 1);
            $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
            $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
            $mudik->save();

            if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-balik') {
                $balik = new TransBalik;
                $balik->code = $code;
                $balik->passenger_id = $parent->id;
                $balik->trip_id = $trip->id;
                $balik->bus_id = $bus['id'];
                $balik->bus_card = generateCode($bus['card'], 1);
                $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                $balik->save();
            }
            // }

            $i = 1;
            foreach (range(1, $request->slot - 1) as $i) {
                $sub = new MasterPassenger();
                $sub->trip_id = $request->trip_id;
                $sub->parent_id = $parent->id;
                $sub->save();

                $code = '';
                $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                $code .= generateCode($trip->id, 2);
                $code .= generateCode($sub->id, 4);

                // if ($province == 'jakarta') {

                $mudik = new TransMudik;
                $mudik->code = $code;
                $mudik->passenger_id = $sub->id;
                $mudik->trip_id = $trip->id;
                $mudik->bus_id = $bus['id'];
                $mudik->bus_card = generateCode($bus['card'] + $i, 1);
                $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                $mudik->save();

                if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-balik') {
                    $balik = new TransBalik;
                    $balik->code = $code;
                    $balik->passenger_id = $sub->id;
                    $balik->trip_id = $trip->id;
                    $balik->bus_id = $bus['id'];
                    $balik->bus_card = generateCode($bus['card'] + $i, 1);
                    $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                    $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                    $balik->save();
                }
                // }

                $i++;
            }

            if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                $vehicle  = new MasterPassengerVehicle;
                $vehicle->passenger_id = $parent->id;
                $vehicle->trip_id = $request->trip_id;
                $vehicle->save();
            }
            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function cancel(Request $request)
    {
        $request->validate([
            "nik" => "required|min:16|max:16",
        ]);

        $parent = MasterPassenger::whereNull('parent_id')->where('nik', $request->nik)->first();

        if (!$parent) {
            return setCustomError('Data Passenger tidak ditemukan');
        }

        try {
            DB::beginTransaction();
            $parent->delete();
            DB::commit();
            return getNotifApi('', '', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('', '', 'errors');
        }
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'email' => 'required|email',
                'nik' => 'required|max:16|min:16',
                'no_kk' => 'required|max:16|min:16',
                'gender' => 'required',
                'phone' => 'required|max:16|min:9',
                'datebirth' => 'required',
                'placebirth' => 'required',
                'address' => 'required',
                'file_ktp' => 'required',
                'file_kk' => 'required',
                // 'vaksin' => 'required|in:green,yellow,red',
            ],
        );

        // if (!$request->file_booster) {
        //     $request->validate([
        //         "type_vaksin" => 'required',
        //         "puskes_id" => 'required|integer'
        //     ]);
        // } else {
        //     $request->validate([
        //         "file_booster" => 'required'
        //     ]);
        // }

        if ($request->slot > 4) {
            $request->slot = 4;
        }


        $parent = MasterPassenger::with('member')->with('vehicle')->where('nik', $request->nik)->whereNull('parent_id')->first();

        if (!$parent) {
            $trip = MasterTrip::find($request->trip_id);
            if (!$trip) {
                return setCustomError('Kota Tujuan tidak ditemukan');
            }

            $bus = availableBus($trip, $request->slot);
            if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                $truck = availableTruck($trip, $request->slot);
                if (!$truck['id']) {
                    return setCustomError("Yahh kamu kurang cepat, Kuota Truck Mudik/Balik Anda saat ini hanya tersisa " . $bus['estimate'][0] . ". Silahkan pilih yang tanpa motor jika ada", 400, ['estimate' => 0, 'type' => 'truck']);
                }
            }

            if (!$bus['id']) {
                return forApi('errors', "Yahh kamu kurang cepat, Kuota Bus Mudik/Balik Anda saat ini hanya tersisa " . $bus['estimate'][0], ['estimate' => $bus['estimate'][0], "type" => 'bus']);
            }

            try {
                DB::beginTransaction();

                $parent = new MasterPassenger();
                $parent->trip_id = $request->trip_id;
                $parent->nik = $request->nik;
                $parent->save();

                $code = '';
                $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                $code .= generateCode($trip->id, 2);
                $code .= generateCode($parent->id, 4);

                $province = strtolower($request->province);

                // if (is_jakarta($province)) {
                $mudik = new TransMudik;
                $mudik->code = $code;
                $mudik->passenger_id = $parent->id;
                $mudik->trip_id = $trip->id;
                $mudik->bus_id = $bus['id'];
                $mudik->bus_card = generateCode($bus['card'], 1);
                $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                $mudik->save();

                if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-balik') {
                    $balik = new TransBalik;
                    $balik->code = $code;
                    $balik->passenger_id = $parent->id;
                    $balik->trip_id = $trip->id;
                    $balik->bus_id = $bus['id'];
                    $balik->bus_card = generateCode($bus['card'], 1);
                    $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                    $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                    $balik->save();
                }
                // }

                $i = 1;
                foreach (range(1, $request->slot - 1) as $i) {
                    $sub = new MasterPassenger();
                    $sub->trip_id = $request->trip_id;
                    $sub->parent_id = $parent->id;
                    $sub->save();

                    $code = '';
                    $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                    $code .= generateCode($trip->id, 2);
                    $code .= generateCode($sub->id, 4);

                    // if (is_jakarta($province)) {

                    $mudik = new TransMudik;
                    $mudik->code = $code;
                    $mudik->passenger_id = $sub->id;
                    $mudik->trip_id = $trip->id;
                    $mudik->bus_id = $bus['id'];
                    $mudik->bus_card = generateCode($bus['card'] + $i, 1);
                    $mudik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                    $mudik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                    $mudik->save();

                    if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-balik') {
                        $balik = new TransBalik;
                        $balik->code = $code;
                        $balik->passenger_id = $sub->id;
                        $balik->trip_id = $trip->id;
                        $balik->bus_id = $bus['id'];
                        $balik->bus_card = generateCode($bus['card'] + $i, 1);
                        $balik->truck_id = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? $truck['id'] : null;
                        $balik->truck_card = $trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor' ? generateCode($truck['card'], 1) : null;
                        $balik->save();
                    }
                    // }

                    $i++;
                }

                if ($trip->type ==  'mudik-balik-motor' || $trip->type == 'mudik-saja-motor') {
                    $vehicle  = new MasterPassengerVehicle;
                    $vehicle->passenger_id = $parent->id;
                    $vehicle->trip_id = $request->trip_id;
                    $vehicle->save();
                }
                DB::commit();
                return getNotifApi('', '', 'success');
            } catch (\Throwable $th) {
                DB::rollBack();
                throw $th;
                return getNotifApi('', '', 'errors');
            }
        }

        $parent = MasterPassenger::with('member')->with('vehicle')->where('nik', $request->nik)->whereNull('parent_id')->first();

        try {
            DB::beginTransaction();

            // create data parent            
            $parent->name = $request->name;
            $parent->email = $request->email;
            $parent->nik = $request->nik;
            $parent->parent_id = null;
            $parent->no_kk = $request->no_kk;
            $parent->gender = strtolower($request->gender) == 'laki-laki' ? 'l' : 'p';
            $parent->phone = $request->phone;
            $parent->placebirth = $request->placebirth;
            $parent->address = $request->address;
            $parent->datebirth = date('Y-m-d', strtotime($request->datebirth));
            $parent->vaksin = $request->vaksin;
            $parent->file_ktp = $request->file_ktp;
            $parent->file_kk = $request->file_kk;
            if ($request->file_booster) {
                $parent->file_booster = $request->file_booster;
            } else {
                $parent->type_vaksin = strtolower($request->type_vaksin);
                $parent->puskes_id = $request->puskes_id;
            }
            $parent->flag = 'chatbot';
            $parent->save();

            // input data anggota keluarga
            $i = 1;
            foreach ($parent->member as $item) {
                $status = true;
                $list = ["name", "placebirth", "datebirth", 'nik', 'phone', 'email', 'gender'];
                foreach ($list as $item) {
                    $check = $item . '_family_' . $i;
                    if (!$request->$check) {
                        $status = false;
                    }
                }
                if ($status) {
                    // if (!$request['file_booster_family_' . $i]) {
                    //     $request->validate([
                    //         "type_vaksin_family_" . $i => 'required|in:booster,pcr,antigen',
                    //         "puskes_id_family_" . $i => 'required|integer'
                    //     ]);
                    // } else {
                    //     $request->validate([
                    //         "file_booster_family_" . $i => 'required'
                    //     ]);
                    // }
                    // $member = MasterPassenger::where('parent_id', $parent->id)->first();
                    $item->name = $request['name_family_' . $i];
                    $item->placebirth = $request['placebirth_family_' . $i];
                    $item->datebirth = date('Y-m-d', strtotime($request['datebirth_family_' . $i]));
                    $item->nik = $request['nik_family_' . $i];
                    $item->phone = $request['phone_family_' . $i];
                    $item->email = $request['email_family_' . $i];
                    $item->gender = strtolower($request['gender_family_' . $i]) == 'laki-laki' ? 'l' : 'p';
                    $item->vaksin = $request['vaksin_family_' . $i];
                    // $item->address = $request['address_family_' . $i];
                    if ($request->file_booster) {
                        $item->file_booster = $request['file_booster_family_' . $i];
                    } else {
                        $item->type_vaksin = strtolower($request['type_vaksin_family_' . $i]);
                        $item->puskes_id = $request['puskes_id_family_' . $i];
                    }
                    $item->flag = 'chatbot';
                    $item->save();
                }
            }

            $status = true;
            if ($status && $parent->vehicle) {
                $request->validate([
                    'no_police' => 'required',
                    'merk_vehicle' => 'required',
                    'type_vehicle' => 'required',
                    'color_vehicle' => 'required',
                    'no_stnk' => 'required',
                ]);
                // input date kendaraan
                $vehicle = new MasterPassengerVehicle;
                $vehicle->passenger_id = $parent->id;
                $vehicle->no_police = $request->no_police;
                $vehicle->merk_vehicle = $request->merk_vehicle;
                $vehicle->type_vehicle = $request->type_vehicle;
                $vehicle->color_vehicle = $request->color_vehicle;
                $vehicle->no_stnk = $request->no_stnk;
                $vehicle->trip_id = $parent->trip_id;
                $vehicle->owner_name = $parent->name;
                $vehicle->owner_nik = $parent->nik;
                $vehicle->owner_no_kk = $parent->no_kk;
                $vehicle->owner_email = $parent->email;
                $vehicle->owner_phone = $parent->phone;
                $vehicle->flag = 'chatbot';
                $vehicle->save();
            }

            MasterPassenger::where('parent_id', $parent->id)->whereNull('name')->whereNull('email')->delete();

            DB::commit();

            return getNotifApi('', '-', 'success');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return getNotifApi('', '-', 'errors');
        }
    }
}
