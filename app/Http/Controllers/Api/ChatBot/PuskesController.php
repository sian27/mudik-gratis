<?php

namespace App\Http\Controllers\Api\ChatBot;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPuskes;
use Illuminate\Http\Request;

class PuskesController extends Controller
{
    public function index()
    {
        $data = MasterPuskes::orderBy('id', 'asc')->get();
        return getNotifApi('', '', 'success', ['puskes' => $data]);
    }
}
