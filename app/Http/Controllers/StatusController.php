<?php

namespace App\Http\Controllers;

use App\Models\Master\MasterBus;
use App\Models\Master\MasterTruck;
use App\Models\Other\OtherStatus;
use App\Models\Trans\TransReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatusController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'category' => 'required'
        ]);

        $data = TransReport::orderBy('id', 'desc');

        // if ($request->category == 'bus') {
        $data = $data->distinct('lo_id');
        // } else if ($request->category == 'truck') {
        // $data = $data->distinct('truck_id');
        // }

        if (date('Y-m-d') < date('Y-m-d', strtotime('2022-05-07')) && $request->type != 'arrive') {
            $data = $data->where('status', $request->type);
            if ($request->category == 'bus') {
                $data = $data->where('type', 'bus');
            } else if ($request->category == 'truck') {
                $data = $data->where('type', 'truck');
            }
        } else {
            if ($request->category == 'bus') {
                $data = $data->where('type', 'bus');
            } else if ($request->category == 'truck') {
                $data = $data->where('type', 'truck');
            }
            $data = $data->where('status', $request->type);
        }


        if (date('Y-m-d H:i') < date('Y-m-d 06:00', strtotime('2022-05-07')) && $request->type == 'arrive') {
            if ($request->category == 'bus') {
                $data = MasterBus::where('status', 'end');
            } else if ($request->category == 'truck') {
                $data = MasterTruck::where('status', 'end');
            }
        }


        $data = $data->count('id');

        return forApi('success', null, ['status' => $data]);

        // $data = OtherStatus::where('category', $request->category)->where('type', $request->type)->first();

        // return forApi('success', null, ['status' => $data->used]);
    }
}
