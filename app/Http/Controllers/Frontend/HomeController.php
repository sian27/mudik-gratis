<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterCity;
use App\Models\Master\MasterContent;
use App\Models\Master\MasterPuskes;
use App\Models\Master\MasterTrip;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $send['title'] = "Home";
        $send['breadcrumb'] = [];
        $send['trip'] = MasterTrip::with('city')->get();
        $send['banner'] = MasterContent::where('slug', 'banner')->orderBy('id', 'desc')->get();
        $send['syarat_pendaftaran'] = MasterContent::where('slug', 'syarat-pendaftaran')->orderBy('id', 'desc')->first();
        $send['lokasi_mudik'] = MasterContent::where('slug', 'lokasi-mudik')->orderBy('id', 'desc')->first();
        $send['lokasi_keberangkatan'] = MasterContent::where('slug', 'lokasi-keberangkatan')->orderBy('id', 'desc')->first();
        $send['syarat_ketentuan'] = MasterContent::where('slug', 'syarat-ketentuan')->orderBy('id', 'desc')->first();
        $send['puskes'] = MasterPuskes::orderBy('id', 'asc')->get();
        return view('frontend.home', $send);
    }
}
