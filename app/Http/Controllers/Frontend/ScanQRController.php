<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterPuskes;
use App\Models\Master\MasterTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ScanQRController extends Controller
{
    public function index()
    {
        return view('scan.index');
    }

    public function detail(Request $request)
    {
        // dd($request->search);
        $decrypt = decrypt($request->key);
        $key = explode(",", $decrypt);

        $id = $key[0];
        $type = $key[1];
        $category = $key[2];

        $param = [
            "type" => $type,
            "category" => $category,
        ];

        try {
            DB::beginTransaction();
            DB::commit();
            if ($category == 'passenger') {

                $data = MasterPassenger::with('vehicle', 'member', 'mudik.trip.city', 'mudik.detailBus', 'mudik.detailTruck')->find($id);
                if (!$data) {
                    return getNotifApi('', '', 'not_found');
                }
                if ($type == 'booking') {
                    if (!$data->scan_booking) {
                        $data->scan_booking = 1;
                        $data->send_ticket = 0;
                    }
                } else {
                    if (!$data->scan_ticket) {
                        $data->scan_ticket = 1;
                    }
                }
                $data->save();
            } else {
                $data = MasterPassengerVehicle::with('passenger.mudik.detailBus', 'passenger.mudik.detailTruck', 'trip.city')->where('passenger_id', $id)->first();
                if (!$data) {
                    return getNotifApi('', '', 'not_found');
                }
                if ($type == 'booking') {
                    if (!$data->scan_booking) {
                        $data->scan_booking = 1;
                        $data->send_ticket = 0;
                    }
                } else {
                    if (!$data->scan_ticket) {
                        $data->scan_ticket = 1;
                    }
                }
                $data->save();
            }

            $trip = MasterTrip::with('city')->get();
            $puskes = MasterPuskes::orderBy('id', 'asc')->get();

            Session::flash('success', 'Scan QR berhasil');
            return view('scan.detail', ['data' => $data, 'param' => $param, 'trip' => $trip, 'puskes' => $puskes]);
        } catch (\Throwable $th) {
            DB::rollBack();
            // throw $th;
            return getNotifApi('', '', 'errors');
        }
    }
}
