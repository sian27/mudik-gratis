<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LOJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->hasHeader('Authorization') || !auth('lo')->user()) {
            return getNotifApi("", '', 'invalid');
        }        
        return $next($request);
    }
}
