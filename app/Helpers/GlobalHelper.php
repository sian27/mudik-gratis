<?php

//Membuat generate nama file dg format tahun bulan tanggal jam menit

use App\Models\Master\MasterAdmin;
use App\Models\Master\MasterBus;
use App\Models\Master\MasterTruck;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;


function user($guard = null)
{
    $id = auth($guard)->user()->id;
    $level = [];
    if ($guard == 'admin') {
        return MasterAdmin::with('role')->find($id);
    }
}

function nameFile($file)
{
    if (isset($file)) {
        $extention = $file->getClientOriginalExtension();
        $nameFile = date('YmdHis') . '-' . strtoupper(Str::random(5)) . '.' . $extention;
        return $nameFile;
    }
}

// deleteFile if exists
function deletedFile($path)
{
    if (!str_contains($path, "default") || !str_contains($path, "http")) {
        if (file_exists(public_path($path)) && $path) {
            unlink(public_path($path));
        }
    }
}


function availableBus($trip, $slot)
{
    if ($slot > 4) {
        $slot = 4;
    }
    $data = MasterBus::where('trip_id', $trip->id)->orderBy('id', 'asc')
    ->where('is_full', 0)
    ->get();
    $result['id'] = 0;
    $result['card'] = 0;
    $result['estimate'] = [0];
    $estimate = [0];
    foreach ($data as $item) {
        $quota = $item->quota - $item->khusus;
        $available = $quota;
        if ($trip->type ==  'mudik-saja' || $trip->type == 'mudik-saja-motor') {
            $taked_mudik = TransMudik::where('bus_id', $item->id)->get();
            if ($taked_mudik) {
                $available = $quota - count($taked_mudik);
            }
        } else {
            $taked_balik = TransBalik::where('bus_id', $item->id)->get();
            if ($taked_balik) {
                $available = $quota - count($taked_balik);
            }
        }
        if ($available >= $slot) {
            $result['id'] = $item->id;
            $result['card'] = $quota - $available + 1;
            array_push($estimate, $available);
            break;
        } else if ($available < $slot - 1) {
            array_push($estimate, $available);
        } else if (!$available) {
            $item->is_full = 1;
            $item->save();
        }
    }
    rsort($estimate);
    if (count($estimate)) {
        $result['estimate'] = $estimate;
        rsort($estimate);
    }
    return $result;
}

function availableTruck($trip, $slot = 1)
{
    if ($slot > 1) {
        $slot = 1;
    }
    $data = MasterTruck::where('trip_id', $trip->id)->where('is_full', 0)->orderBy('id','asc')
    ->get();
    $result['id'] = 0;
    $result['card'] = 0;
    $result['estimate'] = [0];
    foreach ($data as $item) {
        $quota = $item->quota - $item->khusus;
        $available = $quota;
        if ($trip->type ==  'mudik-saja' || $trip->type == 'mudik-saja-motor') {
            $taked_mudik = TransMudik::where('truck_id', $item->id)->whereHas('detailPassenger', function ($q) {
                $q->whereHas('vehicle');
            })->get();
            if ($taked_mudik) {
                $available = $quota - count($taked_mudik);
            }
        } else {
            $taked_balik = TransBalik::where('truck_id', $item->id)->whereHas('detailPassenger', function ($q) {
                $q->whereHas('vehicle');
            })->get();
            if ($taked_balik) {
                $available = $quota - count($taked_balik);
            }
        }

        if ($available >= $slot) {
            $result['id'] = $item->id;
            $result['card'] = $quota - $available + 1;
            break;
        } else if (!$available) {
            $item->is_full = 1;
            $item->save();
        }
    }
    return $result;
}

function generateCode($data, $length)
{
    return str_pad($data, $length, '0', STR_PAD_LEFT);
}


function makeQrcode($code, $type, $category)
{
    $writer = new PngWriter();

    /// Create QR code
    $qrCode = QrCode::create(encrypt($code . ',' . $type . ',' . $category))
        ->setEncoding(new Encoding('UTF-8'))
        ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
        ->setSize(300)
        ->setMargin(10)
        ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
        ->setForegroundColor(new Color(0, 0, 0))
        ->setBackgroundColor(new Color(255, 255, 255));

    $result = $writer->write($qrCode);
    $dataUri = $result->getDataUri();

    return $dataUri;
}


function is_jakarta($data)
{
    $data =  strtolower($data);
    $list = [
        "jakarta",
        'jakarta-timur',
        'jakarta-utara',
        'jakarta-pusat',
        'jakarta-barat',
        'jakarta-selatan',
        'jakarta timur',
        'jakarta utara',
        'jakarta pusat',
        'jakarta barat',
        'jakarta selatan',
    ];
    if (in_array($data, $list)) {
        return true;
    }
    return false;
}
