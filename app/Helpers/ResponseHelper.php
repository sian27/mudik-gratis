<?php

use App\Models\Master\MasterResponseApi;

function getCodeResponse($type)
{
    $result = 275;
    if ($type == "success") {
        $result = 200;
    } else if ($type == "not_found") {
        $result = 404;
    } else if ($type == "errors") {
        $result = 400;
    } else if ($type == "invalid") {
        $result = 401;
    } else if ($type == "forbidden") {
        $result = 403;
    }

    return $result;
}

function getNotifApi($slug, $function, $type, $data = null)
{
    $res = MasterResponseApi::where('slug', $slug)->where('function',  $function)->where('type', $type)->first();
    $success = $type == "success" ? true : false;
    $code = getCodeResponse($type);
    if ($res) {
        return response()->json([
            "success" => $success,
            "data" => $data,
            "notification" => [
                "title" => $res->title,
                "message" => $res->message
            ],
        ], $code);
    } else {
        if ($type == "success") {
            return response()->json([
                "success" => true,
                "data" => $data,
                "notification" => [
                    "title" => "Selamat",
                    "message" => "Permintaan Anda berhasil"
                ],
            ], $code);
        } else if ($type == "not_found") {
            return response()->json([
                "success" => false,
                "data" => $data,
                "notification" => [
                    "title" => "Opps",
                    "message" => "Permintaan Anda tidak dapat ditemukan"
                ],
            ], $code);
        } else if ($type == "errors") {
            return response()->json([
                "success" => false,
                "data" => $data,
                "notification" => [
                    "title" => "Opps",
                    "message" => "Terjadi Kesalahan"
                ],
            ], $code);
        } else if ($type == "invalid") {
            return response()->json([
                "success" => false,
                "data" => $data,
                "notification" => [
                    "title" => "Opps",
                    "message" => "Token Anda tidak valid"
                ],
            ], $code);
        } else if ($type == "forbidden") {
            return response()->json([
                "success" => false,
                "data" => $data,
                "notification" => [
                    "title" => "Opps",
                    "message" => "Anda tidak mempunyai akses untuk memproses ini"
                ],
            ], $code);
        }
    }
}

function setCustomError($message, $status = 400, $data = null)
{
    return response()->json([
        "success" => false,
        "data" => $data,
        "notification" => [
            "title" => "Opps",
            "message" => $message
        ],
    ], $status);
}

function forApi($type, $message = null, $data = null)
{
    if ($type == "success") {
        return response()->json([
            "success" => true,
            "data" => $data,
            "notification" => [
                "title" => "Selamat",
                "message" => $message ?? "Permintaan Anda berhasil"
            ],
        ], 200);
    } else if ($type == "not_found") {
        return response()->json([
            "success" => false,
            "data" => $data,
            "notification" => [
                "title" => "Opps",
                "message" => $message ?? "Permintaan Anda tidak dapat ditemukan"
            ],
        ], 200);
    } else if ($type == "errors") {
        return response()->json([
            "success" => false,
            "data" => $data,
            "notification" => [
                "title" => "Opps",
                "message" => $message ?? "Terjadi Kesalahan"
            ],
        ], 200);
    } else if ($type == "invalid") {
        return response()->json([
            "success" => false,
            "data" => $data,
            "notification" => [
                "title" => "Opps",
                "message" => $message ?? "Token Anda tidak valid"
            ],
        ], 200);
    } else if ($type == "forbidden") {
        return response()->json([
            "success" => false,
            "data" => $data,
            "notification" => [
                "title" => "Opps",
                "message" => $message ?? "Anda tidak mempunyai akses untuk memproses ini"
            ],
        ], 200);
    }
}
