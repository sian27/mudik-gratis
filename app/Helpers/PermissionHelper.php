<?php 

function check_permission($permission, $guard)
{
    $result = false;
    if ($permission) {
        foreach ($permission as $item) {
            if (auth($guard)->user()->can($item . '-list')) {
                $result = true;
            }
            if (auth($guard)->user()->can($item . '-create')) {
                $result = true;
            }
            if (auth($guard)->user()->can($item . '-edit')) {
                $result = true;
            }
            if (auth($guard)->user()->can($item . '-delete')) {
                $result = true;
            }
        }
    } else {
        return true;
    }

    return $result;
}

function label_permission($name)
{
    $name = str_replace('.', ' ', $name);
    $name = str_replace('-list', '', $name);
    $name = str_replace('-create', '', $name);
    $name = str_replace('-edit', '', $name);
    $name = str_replace('-delete', '', $name);
    $name = ucwords($name);

    return $name;
}

function label_permission_transaction($name)
{
    $name = str_replace('visit', '', $name);
    $name = str_replace('meeting.room', '', $name);
    $name = str_replace('workstation', '', $name);
    $name = str_replace('attandance', '', $name);
    $name = str_replace('send', '', $name);
    $name = str_replace('transport', '', $name);
    $name = str_replace('.', ' ', $name);
    $name = ucwords($name);

    return $name;
}

function name_permission($name)
{
    $name = str_replace('.', ' ', $name);
    $name = str_replace('-', ' ', $name);
    $name = ucwords($name);

    return $name;
}
