<?php

namespace App\Models\Other;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherStatistic extends Model
{
    use HasFactory;
    protected $table = 'o_statistic';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
