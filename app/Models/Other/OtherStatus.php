<?php

namespace App\Models\Other;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtherStatus extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'o_status';
}
