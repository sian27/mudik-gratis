<?php

namespace App\Models\Trans;

use App\Models\Master\MasterLO;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransReport extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 't_report';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function lo()
    {
        return $this->hasOne(MasterLO::class, 'id', 'lo_id');
    }
}
