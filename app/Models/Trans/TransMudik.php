<?php

namespace App\Models\Trans;

use App\Models\Master\MasterBus;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterTrip;
use App\Models\Master\MasterTruck;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransMudik extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 't_mudik';

    protected $fillable = ['*'];

    public function trip()
    {
        return $this->hasOne(MasterTrip::class, "id", "trip_id");
    }

    public function detailBus()
    {
        return $this->hasOne(MasterBus::class, "id", "bus_id");
    }

    public function detailTruck()
    {
        return $this->hasOne(MasterTruck::class, "id", "truck_id");
    }

    public function detailPassenger()
    {
        return $this->hasOne(MasterPassenger::class, "id", "passenger_id");
    }

    public function bus()
    {
        return $this->hasMany(MasterBus::class, "trip_id", "trip_id");
    }

    public function truck()
    {
        return $this->hasMany(MasterTruck::class, "trip_id", "trip_id");
    }

    public function passenger()
    {
        return $this->hasMany(MasterPassenger::class, "trip_id", "trip_id")->whereNotNull('name')->whereNotNull('email');
    }

    public function passenger_man()
    {
        return $this->hasMany(MasterPassenger::class, "trip_id", "trip_id")->where('gender', 'l');
    }

    public function passenger_woman()
    {
        return $this->hasMany(MasterPassenger::class, "trip_id", "trip_id")->where('gender', 'p');
    }

    public function vehicle()
    {
        return $this->hasMany(MasterPassengerVehicle::class, "trip_id", "trip_id")->select('*')
            // ->whereHas('trip', function ($q) {
            //     $q->whereIn('type', 'mudik-saja-motor', 'mudik-balik-motor');
            // })
            ->whereNotNull('owner_name')->whereNotNull('owner_email');
    }

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
