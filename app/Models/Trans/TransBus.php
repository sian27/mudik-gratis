<?php

namespace App\Models\Trans;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransBus extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 't_bus';
}
