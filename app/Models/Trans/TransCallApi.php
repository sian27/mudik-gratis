<?php

namespace App\Models\Trans;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransCallApi extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 't_call_api';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
