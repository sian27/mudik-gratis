<?php

namespace App\Models\Trans;

use App\Models\Master\MasterLO;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class TransAttendance extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 't_attendance';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function user()
    {
        return $this->hasOne(MasterLO::class, "id", "lo_id")->withTrashed();
    }

    public function getStartImagePathAttribute()
    {
        if ($this->attributes['start_image']) {
            $file = $this->attributes['start_image'];
            return $file;
        }
        return null;
    }

    public function getStartImageAttribute()
    {
        if ($this->attributes['start_image']) {
            $file = $this->attributes['start_image'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
        return null;
    }

    public function getEndImagePathAttribute()
    {
        if ($this->attributes['start_image']) {
            $file = $this->attributes['start_image'];
            return $file;
        }
        return null;
    }

    public function getEndImageAttribute()
    {
        if ($this->attributes['end_image']) {
            $file = $this->attributes['end_image'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
        return URL::asset('default/empty.jpg');
    }

}
