<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class MasterDriver extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'm_driver';

    public function bus()
    {
        return $this->hasOne(MasterBus::class, "id", "bus_id");
    }

    public function truck()
    {
        return $this->hasOne(MasterTruck::class, "id", "truck_id");
    }

    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            return $file;
        }
        return null;
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
    }
}
