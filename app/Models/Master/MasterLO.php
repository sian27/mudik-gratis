<?php

namespace App\Models\Master;

use App\Models\Trans\TransReport;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\URL;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

class MasterLO extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;
    protected $table = "m_lo";

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at', 
        'deleted_at',        
    ];

    public function bus()
    {
        return $this->hasOne(MasterBus::class, "id", "bus_id");
    }

    public function truck()
    {
        return $this->hasOne(MasterTruck::class, "id", "truck_id");
    }

    public function last_report()
    {
        return $this->hasOne(TransReport::class, "lo_id", "id")->orderBy('id', 'desc');
    }

    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            return $file;
        }
        return null;
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
    }
}
