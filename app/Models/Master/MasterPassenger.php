<?php

namespace App\Models\Master;

use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class MasterPassenger extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "m_passenger";

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // public static function boot()
    // {
    //     parent::boot();

    //     static::deleting(function ($data) { // before delete() method call this
    //         $data->member()->delete();
    //         $data->vehicle()->delete();
    //         $data->mudik()->delete();
    //         $data->balik()->delete();
    //         // do the rest of the cleanup...
    //     });
    // }

    public function vehicle()
    {
        return $this->hasOne(MasterPassengerVehicle::class, "passenger_id", "id")->withTrashed();
    }

    public function member()
    {
        return $this->hasMany(MasterPassenger::class, "parent_id", "id");
    }

    public function parent()
    {
        return $this->hasOne(MasterPassenger::class, "id", "parent_id");
    }

    public function busMudik()
    {
        return $this->hasOne(MasterSeatBus::class, "passenger_mudik", "id");
    }

    public function busBalik()
    {
        return $this->hasOne(MasterSeatBus::class, "passenger_balik", "id");
    }

    public function truckMudik()
    {
        return $this->hasOne(MasterSeatTruck::class, "passenger_mudik", "id");
    }

    public function truckBalik()
    {
        return $this->hasOne(MasterSeatTruck::class, "passenger_balik", "id");
    }

    public function mudik()
    {
        return $this->hasOne(TransMudik::class, "passenger_id", "id");
    }

    public function balik()
    {
        return $this->hasOne(TransBalik::class, "passenger_id", "id");
    }

    public function puskes()
    {
        return $this->hasOne(MasterPuskes::class, "id", "puskes_id");
    }

    public function trip()
    {
        return $this->hasOne(MasterTrip::class, "id", "trip_id")->withTrashed();
    }

    protected $appends = ['file_ktp_path', 'file_kk_path', 'file_booster_path'];

    public function getFileKtpPathAttribute()
    {
        if ($this->attributes['file_ktp']) {
            $file = $this->attributes['file_ktp'];
            return $file;
        }
        return null;
    }

    public function getFileKtpAttribute()
    {
        if ($this->attributes['file_ktp']) {
            $file = $this->attributes['file_ktp'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
        return null;
    }

    public function getFileKkPathAttribute()
    {
        if ($this->attributes['file_kk']) {
            $file = $this->attributes['file_kk'];
            return $file;
        }
        return null;
    }

    public function getFileKkAttribute()
    {
        if ($this->attributes['file_kk']) {
            $file = $this->attributes['file_kk'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
        return null;
    }

    public function getFileBoosterPathAttribute()
    {
        if ($this->attributes['file_booster']) {
            $file = $this->attributes['file_booster'];
            return $file;
        }
        return null;
    }

    public function getFileBoosterAttribute()
    {
        if ($this->attributes['file_booster']) {
            $file = $this->attributes['file_booster'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
        return null;
    }
}
