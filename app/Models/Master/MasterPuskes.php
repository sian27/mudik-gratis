<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterPuskes extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'm_puskes';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
