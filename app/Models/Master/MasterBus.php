<?php

namespace App\Models\Master;

use App\Models\Trans\TransMudik;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterBus extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "m_bus";

    public function driver()
    {
        return $this->hasOne(MasterDriver::class, "id", "driver_id");
    }

    public function trip()
    {
        return $this->hasOne(MasterTrip::class, "id", "trip_id");
    }

    public function mudik()
    {
        return $this->hasOne(TransMudik::class, "bus_id", "id");
    }

    public function passengers()
    {
        return $this->hasMany(MasterSeatBus::class, 'bus_id', 'id')->whereNotNull('passenger_mudik')->whereHas('passenger_mudik', function ($q) {
            $q->where('flag', '!=', 'khusus');
        });
    }

    public function lo()
    {
        return $this->hasOne(MasterLO::class, "bus_id", "id");
    }

    // protected $appends = ['category'];

    // public function getCategoryAttribute()
    // {
    //     return 'bus';
    // }
}
