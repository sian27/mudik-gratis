<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class MasterResponseApi extends Model
{
    use HasFactory, SoftDeletes, LogsActivity;

    protected $table = "m_response_api";

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('response_api')
            ->logOnly(['*'])
            ->setDescriptionForEvent(fn (string $eventName) => "has been {$eventName} Response Api")
            ->logOnlyDirty();
    }
}
