<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class MasterContent extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "m_content";

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = ['file_path'];

    public function getFilePathAttribute()
    {
        if ($this->attributes['file']) {
            $file = $this->attributes['file'];
            return $file;
        }
        return null;
    }

    public function getFileAttribute()
    {
        if ($this->attributes['file']) {
            $file = $this->attributes['file'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
    }
}
