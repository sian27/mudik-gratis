<?php

namespace App\Models\Master;

use App\Models\Trans\TransMudik;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterTruck extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "m_truck";

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function driver()
    {
        return $this->hasOne(MasterDriver::class, "id", "driver_id");

    }

    public function trip()
    {
        return $this->hasOne(MasterTrip::class, "id", "trip_id");
        
    }

    public function mudik()
    {
        return $this->hasOne(TransMudik::class, "truck_id", "id");
    }

    public function lo()
    {
        return $this->hasOne(MasterLO::class, "bus_id", "id");
    }

    // protected $appends = ['category'];

    // public function getCategoryAttribute()
    // {
    //     return 'truck';
    // }
}
