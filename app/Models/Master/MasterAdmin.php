<?php

namespace App\Models\Master;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\URL;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

class MasterAdmin extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes, LogsActivity, HasRoles;

    protected $table = "m_admin";
    protected $guard_name = 'admin';

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->setDescriptionForEvent(fn (string $eventName) => "has been {$eventName} Admin")
            ->logOnlyDirty();
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at', 'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function role()
    {
        return $this->hasOne(Role::class, "id", "role_id");
    }

    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            return $file;
        }
        return null;
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
        return URL::asset('default/empty.jpg');
    }
}
