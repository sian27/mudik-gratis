<?php

namespace App\Models\Master;

use App\Models\Trans\TransMudik;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterTrip extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "m_trip";

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function city()
    {
        return $this->hasOne(MasterCity::class, "id", "city_id");
    }

    public function bus()
    {
        return $this->hasMany(MasterBus::class, "trip_id", "id");
    }

    public function truck()
    {
        return $this->hasMany(MasterTruck::class, "trip_id", "id");
    }

    public function rute()
    {
        return $this->hasMany(MasterRute::class, "trip_id", "id");
    }

    public function mudik()
    {
        return $this->hasMany(TransMudik::class, "trip_id", "id");
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($data) { // before delete() method call this
            $data->rute()->delete();
            // do the rest of the cleanup...
        });
    }
}
