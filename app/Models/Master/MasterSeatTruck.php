<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterSeatTruck extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_seat_truck';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function passenger_mudik()
    {
        return $this->hasOne(MasterPassenger::class, "id", "passenger_mudik");
    }

    public function passenger_balik()
    {
        return $this->hasOne(MasterPassenger::class, "id", "passenger_balik");
    }

    public function truck()
    {
        return $this->hasOne(MasterTruck::class, "id", "truck_id");
    }
}
