<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterPassengerVehicle extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "m_passenger_vehicle";

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function trip()
    {
        return $this->hasOne(MasterTrip::class, 'id', 'trip_id');
    }

    public function passenger()
    {
        return $this->hasOne(MasterPassenger::class, 'id', 'passenger_id');
    }
}
