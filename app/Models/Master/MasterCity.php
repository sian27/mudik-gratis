<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class MasterCity extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "m_city";

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = ['image_path'];

    public function trip()
    {
        return $this->hasMany(MasterTrip::class, "city_id", "id");
    }

    public function getImagePathAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            return $file;
        }
        return null;
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $file = $this->attributes['image'];
            if (!str_contains($file, "https")) {
                return URL::asset($file);
            }
            return $file;
        }
        return URL::asset('default/empty.jpg');
    }
}
