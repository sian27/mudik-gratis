<?php

namespace App\Models\Master;

use App\Models\Trans\TransRute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterRute extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'm_rute';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function checkpoint()
    {
        return $this->hasOne(TransRute::class, "rute_id", "id")->orderBy('id','desc');
    }
}

