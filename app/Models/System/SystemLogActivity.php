<?php

namespace App\Models;

use App\Models\Master\MasterAdmin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    use HasFactory;
    protected $table = 's_activity_log';

    public function user()
    {
        return $this->hasOne(MasterAdmin::class, "id", "causer_id")->withTrashed();
    }

    protected $appends = ['json'];

    public function getJsonAttribute()
    {
        if ($this->attributes['properties']) {
            $file = $this->attributes['properties'];
            return json_decode($file);
        }
    }
}
