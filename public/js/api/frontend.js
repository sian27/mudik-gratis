// City
const countCity = async () => {
    await callApi('api/web/v1/city/count').then((result) => {
        const html = result.data.count
        const el = document.querySelectorAll('.count-city')
        el.forEach(item => {
            item.innerHTML = html;
        })
    })
}

// Bus
const countBus = async () => {
    await callApi('api/web/v1/bus/count').then((result) => {
        document.querySelector('#arus-mudik').querySelector('.bus').querySelector('.unit').querySelector('.html').innerHTML = result.data.count.start
        document.querySelector('#arus-balik').querySelector('.bus').querySelector('.unit').querySelector('.html').innerHTML = result.data.count.end
        // document.querySelector('#arus-mudik').querySelector('.bus').querySelector('.unit').querySelector('.html').innerHTML = 292 
        // document.querySelector('#arus-balik').querySelector('.bus').querySelector('.unit').querySelector('.html').innerHTML = 200 
    })
}

const countBusQouta = async () => {
    await callApi('api/web/v1/bus/count/quota').then((result) => {
        document.querySelector('#arus-mudik').querySelector('.bus').querySelector('.quota').querySelector('.html').innerHTML = result.data.quota.start
        document.querySelector('#arus-balik').querySelector('.bus').querySelector('.quota').querySelector('.html').innerHTML = result.data.quota.end
        // document.querySelector('#arus-mudik').querySelector('.bus').querySelector('.quota').querySelector('.html').innerHTML = 11680 
        // document.querySelector('#arus-balik').querySelector('.bus').querySelector('.quota').querySelector('.html').innerHTML = 8000 
    })
}

const countBusQoutaAvailable = async () => {
    await callApi('api/web/v1/bus/count/quota/available').then((result) => {
        // document.querySelector('#arus-mudik').querySelector('.bus').querySelector('.available').querySelector('.html').innerHTML = result.data.available.start 
        // document.querySelector('#arus-balik').querySelector('.bus').querySelector('.available').querySelector('.html').innerHTML = result.data.available.end 
        document.querySelector('#arus-mudik').querySelector('.bus').querySelector('.available').querySelector('.html').innerHTML = '---';
        document.querySelector('#arus-balik').querySelector('.bus').querySelector('.available').querySelector('.html').innerHTML = '---';
    })
}

// Truck
const countTruck = async () => {
    await callApi('api/web/v1/truck/count').then((result) => {
        document.querySelector('#arus-mudik').querySelector('.truck').querySelector('.unit').querySelector('.html').innerHTML = result.data.count.start
        document.querySelector('#arus-balik').querySelector('.truck').querySelector('.unit').querySelector('.html').innerHTML = result.data.count.end
    })
}

const countTruckQouta = async () => {
    await callApi('api/web/v1/truck/count/quota').then((result) => {
        document.querySelector('#arus-mudik').querySelector('.truck').querySelector('.quota').querySelector('.html').innerHTML = result.data.quota.start
        document.querySelector('#arus-balik').querySelector('.truck').querySelector('.quota').querySelector('.html').innerHTML = result.data.quota.end
    })
}

const countTruckQoutaAvailable = async () => {
    await callApi('api/web/v1/truck/count/quota/available').then((result) => {
        // document.querySelector('#arus-mudik').querySelector('.truck').querySelector('.available').querySelector('.html').innerHTML = result.data.available.start
        // document.querySelector('#arus-balik').querySelector('.truck').querySelector('.available').querySelector('.html').innerHTML = result.data.available.end
        document.querySelector('#arus-mudik').querySelector('.truck').querySelector('.available').querySelector('.html').innerHTML = '---'
        document.querySelector('#arus-balik').querySelector('.truck').querySelector('.available').querySelector('.html').innerHTML = '---'
    })
}

// content
const getContent = async (type) => {
    await callApi("/api/web/v1/content/" + type).then((result) => {
        document.querySelector('#' + type).querySelector('.html-' + type).innerHTML = result.data.content?.html
    })
}

// record api
const recordApi = async (type) => {
    await callApi("/api/web/v1/record-api", 'POST', { type: type }).then((result) => {
    })
}

// check valid date or not
function testDateString(string) {
    var timestamp = Date.parse(string);
    if (isNaN(timestamp) == false) {
        return true
    }
    return false
}

// external api digidata
const orcExtra = async (image) => {
    await callApi("api/web/v1/external/digidata/orc_extra", 'POST', image).then((result) => {
        result = JSON.parse(result)
        const data = result.data;
        const form = document.querySelector('#daftar').querySelector('form');
        if (data.nik) {
            form.querySelector('input[name="nik"]').value = data.nik
        }
        if (data.nama) {
            form.querySelector('input[name="name"]').value = data.nama
        }
        if (data.tempat_lahir) {
            form.querySelector('input[name="placebirth"]').value = data.tempat_lahir
        }
        // reformat date
        if (testDateString(data.tanggal_lahir)) {
            const date = data.tanggal_lahir;
            const [day, month, year] = date.split("-");
            const newDate = `${year}-${month}-${day}`
            // end reformat date
            form.querySelector('input[name="datebirth"]').value = newDate
        }
        if (data.jenis_kelamin) {
            $('#daftar form select[name="gender"]').val(data.jenis_kelamin == 'PEREMPUAN' ? 'p' : 'l').trigger('change');
        }
        $('#daftar form textarea[name="address"]').val(data.alamat)
        if (data.nik) {
            checkVaksin(data.nik, 'vaksin')
        }
        document.getElementById('close-modal-loading').click()
    })
}

const verifyIdentity = (data, target) => {
    return new Promise(function (resolve, reject) {
        // if (data.nik || data.kk) {
        //     callApi("api/web/v1/external/digidata/verify_identity", 'POST', data).then((result) => {
        //         if (!result.data) {
        //             document.getElementById('call-modal-verify-err').click();
        //             // document.getElementById('prev-step').click();  
        //             document.getElementById('target-err-verify').innerHTML = target
        //             reject()
        //         } else {
        //             pass_identify = true;
        //             resolve(true)
        //         }
        //     })
        // }
        // else {
        //     resolve()
        // }
        resolve()
    })
}

const verifyFamilyCard = (data, target) => {
    return new Promise(function (resolve, reject) {
        if (data.nik || data.kk) {
            callApi("api/web/v1/external/digidata/verify_family_card", 'POST', data).then((result) => {
                // result = JSON.parse(result)            
                if (!result.data.family_id) {
                    document.getElementById('call-modal-nik-err').click();
                    // document.getElementById('prev-step').click();
                    document.getElementById('target-err-kk').innerHTML = target
                    reject()
                } else {
                    pass_identify = true;
                    resolve(true)
                }
            })
        }
        else {
            resolve()
        }
        // resolve()
    })
}

const checkVaksin = async (nik, target) => {
    await callApi('api/web/v1/external/vaksin/check', 'POST', { nik: nik }).then((result) => {
        document.querySelector('#daftar').querySelector('form').querySelector('input[name="' + target + '"]').value = result.data.userStatus
        var label = ""
        if (result.data.userStatus == 'green') {
            label = "Dosis 2";
        }
        else if (result.data.userStatus == 'yellow') {
            label = 'Dosis 1'
        }
        else if (result.data.userStatus == 'red') {
            label = 'Belum Vaksin'
        }
        else {
            label = 'Belum Vaksin'
        }
        document.querySelector('#daftar').querySelector('form').querySelector('input[name="label_' + target + '"]').value = label
    })
}

const removeVaksin = async (target) => {
    document.querySelector('#daftar').querySelector('form').querySelector('input[name="' + target + '"]').value = null
    document.querySelector('#daftar').querySelector('form').querySelector('input[name="label_' + target + '"]').value = null
}

const checkQuota = async () => {
    let count = 4;
    const trip = document.querySelector('#kota-tujuan').value;
    await callApi('api/web/v1/quota/check', 'GET', {
        trip_id: trip,
        slot: count
    }).then((result) => {
        if (!result.success) {
            $('#daftar form').find('input').prop('disabled', true)
            $('#daftar form').find('select').prop('disabled', true)
            $('#kota-tujuan').prop('disabled', false)
            if (result.data.estimate) {
                document.getElementById('call-modal-quota-over').click()
                document.getElementById('target-quota-over').innerHTML = parseInt(result.data.estimate)
            }
            else {
                document.getElementById('call-modal-quota-limit').click()
            }
            document.getElementById('firstVal').value = 0;
        }
        else {
            $('#daftar form').find('input').prop('disabled', false)
            $('#daftar form').find('select').prop('disabled', false)
            document.getElementById('firstVal').value = 1;
        }

        if (result.data.estimate) {
            document.querySelector('#quota-trip').value = result.data.estimate;
        }
        closeModalLoading()

    })
}

const checkTypeTrip = async (data) => {
    document.getElementById('call-modal-loading-step').click()
    await callApi('api/web/v1/trip/check', 'GET', data).then((result) => {
        document.querySelector('#type-trip').value = result.data.trip.type
        if (result.data.trip.type != 'mudik-balik-motor') {
            document.querySelector('#form-label-motor').classList.add("d-none")
        }
        else {
            document.querySelector('#form-label-motor').classList.remove("d-none")
        }

        checkQuota()
    })
}

const bookingBus = async (data) => {
    // document.getElementById('call-modal-loading-step').click()
    await callApi('api/web/v1/registration/booking', 'POST', data).then((result) => {
        console.log(result);
        if (!result.success) {
            $('#daftar form').find('input').prop('disabled', true)
            $('#daftar form').find('select').prop('disabled', true)
            if (result.data.estimate) {
                document.getElementById('call-modal-quota-over').click()
                document.getElementById('target-quota-over').innerHTML = parseInt(result.data.estimate)
            }
            else {
                document.getElementById('call-modal-quota-limit').click()
            }
        }
        else {
            $('#daftar form').find('input').prop('disabled', false)
            $('#daftar form').find('select').prop('disabled', false)
            document.querySelector('#base-id').value = result.data.id;
        }
        document.querySelector('#quota-trip').value = result.data.estimate;
        closeModalLoading()

    })
}

const closeModalLoading = async () => {
    setTimeout(() => {
        document.querySelector('#close-modal-loading-step').click()
        $('#close-modal-loading-step').trigger('click');
    }, 1500);
}