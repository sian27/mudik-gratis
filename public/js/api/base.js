const baseUrl = "https://backend.mudikgratisdkijakarta.id/"

var callApi = function (url, type = 'GET', data = {}) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            method: type,
            url: baseUrl + url,
            async: true,
            data: data,
            // headers : {
            //     'Access-Control-Request-Headers' : '*',
            //     'Accept': 'application/json',
            //     'Content-Type': 'application/json'
            // },
            success: function (response) {
                resolve(response);
            },
            error: function (response) {
                // console.log(response.responseJSON.message ?? response.responseJSON.notification
                //     .message);
                reject();
            },
        });
    })
}