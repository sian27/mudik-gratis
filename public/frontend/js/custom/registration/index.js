"use strict";

var KTCreateAccount = (function () {
    var e,
        t,
        i,
        o,
        s,
        r,
        a = [];

    const scrollToContent = function (step) {
        const element = document.getElementsByClassName("content-element")[
            step
        ];
        scrolling(element);
    }
    const scrolling = (element) => {
        const offset = 140;
        const bodyRect = document.body.getBoundingClientRect().top;
        const elementRect = element.getBoundingClientRect().top;
        const elementPosition = elementRect - bodyRect;
        const offsetPosition = elementPosition - offset;
        window.scrollTo({
            top: offsetPosition,
            behavior: "smooth",
        });
    };

    const ajaxSubmit = function () {
        const form = document.querySelector('#daftar').querySelector('form')
        $.ajax({
            url: baseUrl + 'api/web/v1/registration',
            data: new FormData(form),
            type: 'POST',
            crossDomain: true,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (response) {
                console.log(response);
                Swal.fire({
                    text: response.notification.message,
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Oke",
                    customClass: {
                        confirmButton: "btn btn-cyan"
                    },
                }).then(function () {
                    o.removeAttribute("data-kt-indicator"), (o.disabled = !1)
                    window.location.reload();
                });
            },
            error: function (response) {
                Swal.fire({
                    text: response.responseJSON.message ?? response.responseJSON.notification.message,
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: "Oke, saya cek kembali",
                    customClass: {
                        confirmButton: "btn btn-cyan"
                    },
                }).then(function () {
                    o.removeAttribute("data-kt-indicator"), (o.disabled = !1)
                });
            }
        })
    }
    return {
        init: function () {
            (e = document.querySelector("#kt_modal_create_account")) && new bootstrap.Modal(e),
                (t = document.querySelector("#form-horizontal")),
                (i = t.querySelector("#registration-form")),
                (o = t.querySelector('[data-kt-stepper-action="submit"]')),
                (s = t.querySelector('[data-kt-stepper-action="next"]')),
                (r = new KTStepper(t)).on("kt.stepper.changed", function (e) {
                    2 === r.getCurrentStepIndex() && document.querySelector('#type-trip').value != "mudik-balik-motor" ?
                        (o.classList.remove("d-none"), o.classList.add("d-inline-block"), s.classList.add("d-none")) :
                        3 === r.getCurrentStepIndex() ?
                            (o.classList.remove("d-none"), o.classList.add("d-inline-block"), s.classList.add("d-none")) :
                            // 4 === r.getCurrentStepIndex() ?
                            //     (o.classList.add("d-none"), s.classList.add("d-none")) :
                            (o.classList.remove("d-inline-block"), o.classList.remove("d-none"), s.classList.remove("d-none"));
                }),
                r.on("kt.stepper.next", function (e) {
                    // console.log("stepper.next");                    

                    var t = a[e.getCurrentStepIndex() - 1];
                    t
                        ?
                        t.validate().then(async function (t) {
                            console.log(),
                                "Valid" == t ?
                                    r.getCurrentStepIndex() == 1 && !document.querySelector('#quota-trip').value ? document.getElementById('call-modal-quota-limit').click() :
                                        r.getCurrentStepIndex() == 1 ?
                                            (document.getElementById('call-modal-loading-step').click(),
                                                await verifyFamilyCard({
                                                    nik: i.querySelector('[name="nik"]').value,
                                                    kk: i.querySelector('[name="no_kk"]').value
                                                }, 'Calon Pemudik').then(async () => {
                                                    await verifyIdentity({
                                                        trx_id: "",
                                                        nik: i.querySelector('[name="nik"]').value,
                                                        name: i.querySelector('[name="name"]').value,
                                                        birthdate: i.querySelector('[name="datebirth"]').value,
                                                        birthplace: i.querySelector('[name="placebirth"]').value,
                                                        address: i.querySelector('[name="address"]').value
                                                    }, 'Calon Pemudik').then(async () => {
                                                        await bookingBus({
                                                            nik: i.querySelector('[name="nik"]').value,
                                                            trip_id: i.querySelector('[name="trip_id"]').value,
                                                            province: i.querySelector('[name="placebirth"]').value,
                                                            slot: i.querySelector('[name="quota"]').value,
                                                        }).then(() => {
                                                            closeModalLoading(), e.goNext(), scrollToContent(0)
                                                        })
                                                    })
                                                })) :
                                            r.getCurrentStepIndex() == 2 ?
                                                (document.getElementById('call-modal-loading-step').click(),
                                                    await verifyFamilyCard({
                                                        nik: i.querySelector('[name="nik_family_1"]').value,
                                                        kk: i.querySelector('[name="no_kk"]').value
                                                    }, 'Anggota Keluarga 1').then(async () => {
                                                        await verifyFamilyCard({
                                                            nik: i.querySelector('[name="nik_family_2"]').value,
                                                            kk: i.querySelector('[name="no_kk"]').value
                                                        }, 'Anggota Keluarga 2').then(async () => {
                                                            await verifyFamilyCard({
                                                                nik: i.querySelector('[name="nik_family_3"]').value,
                                                                kk: i.querySelector('[name="no_kk"]').value
                                                            }, 'Anggota Keluarga 3').then(() => {
                                                                closeModalLoading(), e.goNext(), scrollToContent(0)
                                                            })
                                                        })
                                                    }))
                                                :
                                                (console.log(t), e.goNext(), scrollToContent(0)) :
                                    Swal.fire({
                                        text: "Maaf, sepertinya input masih belum sesuai. Silahkan cek kembali",
                                        icon: "error",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn btn-cyan"
                                        },
                                    }).then(function () {
                                        scrollToContent(0);
                                    });
                        }) :
                        (e.goNext(), scrollToContent(0));
                }),
                r.on("kt.stepper.previous", function (e) {
                    //console.log("stepper.previous")
                    e.goPrevious(), scrollToContent(0);
                }),
                a.push(
                    FormValidation.formValidation(i, {
                        fields: {
                            name: {
                                validators: {
                                    notEmpty: {
                                        message: "Nama Lengkap tidak boleh kosong"
                                    }
                                }
                            },
                            ktp: {
                                validators: {
                                    callback: {
                                        message: 'Harap upload KTP',
                                        callback: function () {
                                            const input = document.getElementById('ktp').files[0];
                                            if (!input) {
                                                return false
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                            file_ktp: {
                                validators: {
                                    file: {
                                        extension: 'png,jpg,jpeg',
                                        maxSize: '5242880',
                                        message: 'Hanya menerima format PNG,JPG,JPEG dan Maksimal file 5 MB'
                                    }
                                }
                            },
                            nik: {
                                validators: {
                                    callback: {
                                        callback: function (input) {
                                            if (!input.value) {
                                                return {
                                                    valid: false,
                                                    message: 'NIK tidak boleh kosong'
                                                };
                                            }
                                            else if (input.value) {
                                                if (input.value.length != 16) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK tidak valid'
                                                    };
                                                }
                                                else {
                                                    const allNik = ['nik_family_3', 'nik_family_2', 'nik_family_1'];
                                                    let nikSame = false
                                                    allNik.forEach(item => {
                                                        const nik = i.querySelector('input[name="' + item + '"]')
                                                        if (nik.value == input.value) {
                                                            nikSame = true;
                                                        }
                                                    })
                                                    if (input.value && nikSame) {
                                                        return {
                                                            valid: false,
                                                            message: 'NIK yang sama tidak boleh di input kembali'
                                                        };
                                                    }
                                                    else {
                                                        $.get(baseUrl + i.querySelector('[name="nik"]').dataset.url, {
                                                            nik: i.querySelector('[name="nik"]').value,
                                                            base_id: i.querySelector('[name="base_id"]').value,
                                                        }, function (response) {
                                                            if (!response.valid) {
                                                                return response
                                                            }
                                                            checkVaksin(i.querySelector('[name="nik"]').value, "vaksin")
                                                        }).fail(function (response) {
                                                            console.log(response);
                                                        });
                                                    }
                                                }
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                            kk: {
                                validators: {
                                    callback: {
                                        message: 'Harap upload KK',
                                        callback: function () {
                                            const input = document.getElementById('kk').files[0];
                                            if (!input) {
                                                return false
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                            file_kk: {
                                validators: {
                                    file: {
                                        extension: 'png,jpg,jpeg',
                                        maxSize: '5242880',
                                        message: 'Hanya menerima format PNG,JPG,JPEG dan Maksimal file 5 MB'
                                    }
                                }
                            },
                            no_kk: {
                                validators: {
                                    notEmpty: {
                                        message: "Nomor KK tidak boleh kosong"
                                    },
                                    stringLength: {
                                        min: 16,
                                        max: 16,
                                        message: 'Nomor KK tidak valid'
                                    },
                                    // remote: {
                                    //     url: function () {
                                    //         return 'api/web/v1/external/digidata/verify_family_card'
                                    //     },
                                    //     method: 'POST',
                                    //     data: function () {
                                    //         return {
                                    //             nik: i.querySelector('[name="nik"]').value,
                                    //             kk: i.querySelector('[name="no_kk"]').value
                                    //         };
                                    //     },
                                    //     message: 'Nomor KK tidak sesuai dengan NIK',
                                    // }
                                }
                            },
                            trip_id: {
                                validators: {
                                    notEmpty: {
                                        message: "Harap memilih Kota Mudik"
                                    },
                                }
                            },
                            gender: {
                                validators: {
                                    notEmpty: {
                                        message: "Harap memilih Jenis Kelamin"
                                    }
                                }
                            },
                            email: {
                                validators: {
                                    notEmpty: {
                                        message: 'Email tidak boleh kosong'
                                    },
                                    emailAddress: {
                                        message: 'Email tidak valid'
                                    },
                                }
                            },
                            phone: {
                                validators: {
                                    notEmpty: {
                                        message: "No Handphone tidak boleh kosong"
                                    },
                                    stringLength: {
                                        min: 9,
                                        max: 15,
                                        message: 'No Handphone tidak valid'
                                    },
                                }
                            },
                            datebirth: {
                                validators: {
                                    notEmpty: {
                                        message: "Tanggal Lahir tidak boleh kosong"
                                    }
                                }
                            },
                            placebirth: {
                                validators: {
                                    notEmpty: {
                                        message: "Kota Sesuai KTP tidak boleh kosong"
                                    }
                                }
                            },
                            address: {
                                validators: {
                                    notEmpty: {
                                        message: "Alamat Sesuai KTP tidak boleh kosong"
                                    }
                                }
                            },
                            check_booster: {
                                validators: {
                                    notEmpty: {
                                        message: "Harap memilih salah satu"
                                    },
                                    callback: {
                                        callback: function () {
                                            const input = i.querySelector('input[name="check_booster"]:checked').value;
                                            console.log();
                                            const file_booster = document.querySelector('input[name="file_booster"]').parentElement.parentElement.parentElement.parentElement
                                            const type_vaksin = document.querySelector('select[name="type_vaksin"]').parentElement
                                            const puskes_id = document.querySelector('select[name="puskes_id"]').parentElement
                                            if (input == "sudah") {
                                                file_booster.classList.remove('d-none');
                                                type_vaksin.classList.add('d-none');
                                                puskes_id.classList.add('d-none');
                                            }
                                            else {
                                                type_vaksin.classList.remove('d-none');
                                                puskes_id.classList.remove('d-none');
                                                file_booster.classList.add('d-none');
                                            }
                                        }
                                    }
                                }
                            },
                            booster: {
                                validators: {
                                    callback: {
                                        message: 'Harap upload Booster',
                                        callback: function () {
                                            const cek = i.querySelector('input[name="check_booster"]:checked').value
                                            const input = document.getElementById('booster').files[0];
                                            if (!input && cek == 'sudah') {
                                                return false
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                            file_booster: {
                                validators: {
                                    file: {
                                        extension: 'png,jpg,jpeg',
                                        maxSize: '5242880',
                                        message: 'Hanya menerima format PNG,JPG,JPEG dan Maksimal file 5 MB'
                                    }
                                }
                            },
                            type_vaksin: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Tipe Vaksin",
                                        callback: function (input) {
                                            const cek = i.querySelector('input[name="check_booster"]:checked').value
                                            if (!input.value && cek == 'belum') {
                                                return false
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                            puskes_id: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Puskesmas",
                                        callback: function (input) {
                                            const cek = i.querySelector('input[name="check_booster"]:checked').value
                                            if (!input.value && cek == 'belum') {
                                                return false
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                            approve: {
                                validators: {
                                    callback: {
                                        message: "Harap setujui terlebih dahulu",
                                        callback: function (input) {
                                            if (input.element.checked == false) {
                                                return false
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: ".fv-row",
                                eleInvalidClass: "",
                                eleValidClass: ""
                            })
                        },
                    })
                ),
                a.push(
                    FormValidation.formValidation(i, {
                        fields: {
                            name_family_1: {
                                validators: {
                                    callback: {
                                        message: 'Nama Lengkap tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            placebirth_family_1: {
                                validators: {
                                    callback: {
                                        message: 'Tempat tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            datebirth_family_1: {
                                validators: {
                                    callback: {
                                        message: 'Tanggal tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            nik_family_1: {
                                validators: {
                                    callback: {
                                        callback: function (input) {
                                            if (!input.value) {
                                                removeVaksin("vaksin_family_1");
                                            }
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            });
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return {
                                                    valid: false,
                                                    message: 'NIK tidak boleh kosong'
                                                };
                                            }
                                            else if (input.value) {
                                                if (input.value.length != 16) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK tidak valid'
                                                    };
                                                }
                                                const allNik = ['nik', 'nik_family_3', 'nik_family_2'];
                                                let nikSame = false
                                                allNik.forEach(item => {
                                                    const nik = i.querySelector('input[name="' + item + '"]')
                                                    if (nik.value == input.value) {
                                                        nikSame = true;
                                                    }
                                                })
                                                if (input.value && nikSame) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK yang sama tidak boleh di input kembali'
                                                    };
                                                }
                                                else {
                                                    $.get(baseUrl + i.querySelector('[name="nik_family_1"]').dataset.url, {
                                                        nik: i.querySelector('[name="nik_family_1"]').value,
                                                    }, function (response) {
                                                        if (!response.valid) {
                                                            return response
                                                        }
                                                        checkVaksin(i.querySelector('[name="nik_family_1"]').value, "vaksin_family_1")
                                                    }).fail(function (response) {
                                                        return response
                                                    });
                                                }
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            gender_family_1: {
                                validators: {
                                    callback: {
                                        message: 'Jenis Kelamin tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            phone_family_1: {
                                validators: {
                                    callback: {
                                        stringLength: {
                                            min: 9,
                                            max: 15,
                                            message: 'No Handphone tidak valid'
                                        },
                                        message: 'No Handphone tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            email_family_1: {
                                validators: {
                                    emailAddress: {
                                        message: 'Email tidak valid'
                                    },
                                    callback: {
                                        message: 'Email tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            check_booster_family_1: {
                                validators: {
                                    // notEmpty: {
                                    // },
                                    callback: {
                                        message: "Harap memilih salah satu",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            console.log(input);
                                            const booster = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value;
                                            const file_booster = document.querySelector('input[name="file_booster_family_' + input.element.dataset.row + '"]').parentElement.parentElement.parentElement.parentElement
                                            const type_vaksin = document.querySelector('select[name="type_vaksin_family_' + input.element.dataset.row + '"]').parentElement
                                            const puskes_id = document.querySelector('select[name="puskes_id_family_' + input.element.dataset.row + '"]').parentElement

                                            if (booster == "sudah") {
                                                file_booster.classList.remove('d-none');
                                                type_vaksin.classList.add('d-none');
                                                puskes_id.classList.add('d-none');
                                            }
                                            else {
                                                type_vaksin.classList.remove('d-none');
                                                puskes_id.classList.remove('d-none');
                                                file_booster.classList.add('d-none');
                                            }
                                        }
                                    }
                                }
                            },
                            booster_family_1: {
                                validators: {
                                    callback: {
                                        message: 'Harap upload Booster',
                                        callback: function () {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            const booster = document.getElementById('booster').files[0];
                                            if (!booster && cek == 'sudah' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            file_booster_family_1: {
                                validators: {
                                    file: {
                                        extension: 'png,jpg,jpeg',
                                        maxSize: '5242880',
                                        message: 'Hanya menerima format PNG,JPG,JPEG dan Maksimal file 5 MB'
                                    }
                                }
                            },
                            type_vaksin_family_1: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Tipe Vaksin",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            if (!input.value && cek == 'belum' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            puskes_id_family_1: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Puskesmas",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            if (!input.value && cek == 'belum' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            name_family_2: {
                                validators: {
                                    callback: {
                                        message: 'Nama Lengkap tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            placebirth_family_2: {
                                validators: {
                                    callback: {
                                        message: 'Tempat tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            datebirth_family_2: {
                                validators: {
                                    callback: {
                                        message: 'Tanggal tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            nik_family_2: {
                                validators: {
                                    callback: {
                                        callback: function (input) {
                                            if (!input.value) {
                                                removeVaksin("vaksin_family_2");
                                            }
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            });
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return {
                                                    valid: false,
                                                    message: 'NIK tidak boleh kosong'
                                                };
                                            }
                                            else if (input.value) {
                                                if (input.value.length != 16) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK tidak valid'
                                                    };
                                                }
                                                const allNik = ['nik', 'nik_family_3', 'nik_family_1'];
                                                let nikSame = false
                                                allNik.forEach(item => {
                                                    const nik = i.querySelector('input[name="' + item + '"]')
                                                    if (nik.value == input.value) {
                                                        nikSame = true;
                                                    }
                                                })
                                                if (input.value && nikSame) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK yang sama tidak boleh di input kembali'
                                                    };
                                                }
                                                else {
                                                    $.get(baseUrl + i.querySelector('[name="nik_family_2"]').dataset.url, {
                                                        nik: i.querySelector('[name="nik_family_2"]').value,
                                                    }, function (response) {
                                                        if (!response.valid) {
                                                            return response
                                                        }
                                                        checkVaksin(i.querySelector('[name="nik_family_2"]').value, "vaksin_family_2")
                                                    }).fail(function (response) {
                                                        return response
                                                    });
                                                }
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            gender_family_2: {
                                validators: {
                                    callback: {
                                        message: 'Jenis Kelamin tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            phone_family_2: {
                                validators: {
                                    callback: {
                                        stringLength: {
                                            min: 9,
                                            max: 15,
                                            message: 'No Handphone tidak valid'
                                        },
                                        message: 'No Handphone tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);

                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            email_family_2: {
                                validators: {
                                    emailAddress: {
                                        message: 'Email tidak valid'
                                    },
                                    callback: {
                                        message: 'Email tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            check_booster_family_2: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih salah satu",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const booster = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value;
                                            const file_booster = document.querySelector('input[name="file_booster_family_' + input.element.dataset.row + '"]').parentElement.parentElement.parentElement.parentElement
                                            const type_vaksin = document.querySelector('select[name="type_vaksin_family_' + input.element.dataset.row + '"]').parentElement
                                            const puskes_id = document.querySelector('select[name="puskes_id_family_' + input.element.dataset.row + '"]').parentElement
                                            if (booster == "sudah") {
                                                file_booster.classList.remove('d-none');
                                                type_vaksin.classList.add('d-none');
                                                puskes_id.classList.add('d-none');
                                            }
                                            else {
                                                type_vaksin.classList.remove('d-none');
                                                puskes_id.classList.remove('d-none');
                                                file_booster.classList.add('d-none');
                                            }
                                        }
                                    }
                                }
                            },
                            booster_family_2: {
                                validators: {
                                    callback: {
                                        message: 'Harap upload Booster',
                                        callback: function () {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            const booster = document.getElementById('booster').files[0];
                                            if (!booster && cek == 'sudah' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            file_booster_family_2: {
                                validators: {
                                    file: {
                                        extension: 'png,jpg,jpeg',
                                        maxSize: '5242880',
                                        message: 'Hanya menerima format PNG,JPG,JPEG dan Maksimal file 5 MB'
                                    }
                                }
                            },
                            type_vaksin_family_2: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Tipe Vaksin",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            if (!input.value && cek == 'belum' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            puskes_id_family_2: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Puskesmas",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            if (!input.value && cek == 'belum' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            name_family_3: {
                                validators: {
                                    callback: {
                                        message: 'Nama Lengkap tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            placebirth_family_3: {
                                validators: {
                                    callback: {
                                        message: 'Tempat tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            datebirth_family_3: {
                                validators: {
                                    callback: {
                                        message: 'Tanggal tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            nik_family_3: {
                                validators: {
                                    callback: {
                                        callback: function (input) {
                                            if (!input.value) {
                                                removeVaksin("vaksin_family_1");
                                            }
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            });
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return {
                                                    valid: false,
                                                    message: 'NIK tidak boleh kosong'
                                                };
                                            }
                                            else if (input.value) {
                                                if (input.value.length != 16) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK tidak valid'
                                                    };
                                                }
                                                const allNik = ['nik', 'nik_family_1', 'nik_family_2'];
                                                let nikSame = false
                                                allNik.forEach(item => {
                                                    const nik = i.querySelector('input[name="' + item + '"]')
                                                    if (nik.value == input.value) {
                                                        nikSame = true;
                                                    }
                                                })
                                                if (input.value && nikSame) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK yang sama tidak boleh di input kembali'
                                                    };
                                                }
                                                else {
                                                    $.get(baseUrl + i.querySelector('[name="nik_family_3"]').dataset.url, {
                                                        nik: i.querySelector('[name="nik_family_3"]').value,
                                                    }, function (response) {
                                                        if (!response.valid) {
                                                            return response
                                                        }
                                                        checkVaksin(i.querySelector('[name="nik_family_3"]').value, "vaksin_family_3")
                                                    }).fail(function (response) {
                                                        return response
                                                    });
                                                }
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            gender_family_3: {
                                validators: {
                                    callback: {
                                        message: 'Jenis Kelamin tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            phone_family_3: {
                                validators: {
                                    callback: {
                                        stringLength: {
                                            min: 9,
                                            max: 15,
                                            message: 'No Handphone tidak valid'
                                        },
                                        message: 'No Handphone tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            email_family_3: {
                                validators: {
                                    emailAddress: {
                                        message: 'Email tidak valid'
                                    },
                                    callback: {
                                        message: 'Email tidak boleh kosong',
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            check_booster_family_3: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih salah satu",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const booster = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value;
                                            const file_booster = document.querySelector('input[name="file_booster_family_' + input.element.dataset.row + '"]').parentElement.parentElement.parentElement.parentElement
                                            const type_vaksin = document.querySelector('select[name="type_vaksin_family_' + input.element.dataset.row + '"]').parentElement
                                            const puskes_id = document.querySelector('select[name="puskes_id_family_' + input.element.dataset.row + '"]').parentElement
                                            if (booster == "sudah") {
                                                file_booster.classList.remove('d-none');
                                                type_vaksin.classList.add('d-none');
                                                puskes_id.classList.add('d-none');
                                            }
                                            else {
                                                type_vaksin.classList.remove('d-none');
                                                puskes_id.classList.remove('d-none');
                                                file_booster.classList.add('d-none');
                                            }
                                        }
                                    }
                                }
                            },
                            booster_family_3: {
                                validators: {
                                    callback: {
                                        message: 'Harap upload Booster',
                                        callback: function () {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            const booster = document.getElementById('booster').files[0];
                                            if (!booster && cek == 'sudah' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            file_booster_family_3: {
                                validators: {
                                    file: {
                                        extension: 'png,jpg,jpeg',
                                        maxSize: '5242880',
                                        message: 'Hanya menerima format PNG,JPG,JPEG dan Maksimal file 5 MB'
                                    }
                                }
                            },
                            type_vaksin_family_3: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Tipe Vaksin",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            if (!input.value && cek == 'belum' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                            puskes_id_family_3: {
                                validators: {
                                    callback: {
                                        message: "Harap memilih Puskesmas",
                                        callback: function (input) {
                                            let check = false;
                                            const allInput = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('input')
                                            allInput.forEach(items => {
                                                if (items.getAttribute('type') != 'radio') {
                                                    if (items.value) {
                                                        console.log(items, item.value);
                                                        check = true;
                                                    }
                                                }
                                            })
                                            const allSelect = document.getElementById('content-anggota-keluarga').querySelectorAll('.col-lg-4')[input.element.dataset.row - 1].querySelectorAll('select')
                                            allSelect.forEach(items => {
                                                if (items.value) {
                                                    check = true;
                                                }
                                            })
                                            if (!input.value && check) {
                                                return false
                                            }
                                            const cek = i.querySelector('input[name="check_booster_family_' + input.element.dataset.row + '"]:checked').value
                                            if (!input.value && cek == 'belum' && check) {
                                                return false
                                            }
                                            allInput.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })
                                            allSelect.forEach(items => {
                                                $('form input[name="' + items + '"]').trigger('change').trigger('keyup');
                                            })

                                            return true
                                        }
                                    }
                                }
                            },
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: ".fv-row",
                                eleInvalidClass: "",
                                eleValidClass: ""
                            })
                        },
                    })
                ),
                a.push(
                    FormValidation.formValidation(i, {
                        fields: {
                            no_police: {
                                validators: {
                                    notEmpty: {
                                        message: "Plat Motor tidak boleh kosong"
                                    }
                                }
                            },
                            merk_vehicle: {
                                validators: {
                                    notEmpty: {
                                        message: "Merk Motor tidak boleh kosong"
                                    }
                                }
                            },
                            type_vehicle: {
                                validators: {
                                    notEmpty: {
                                        message: "Tipe Motor tidak boleh kosong"
                                    }
                                }
                            },
                            color_vehicle: {
                                validators: {
                                    notEmpty: {
                                        message: "Warna Motor tidak boleh kosong"
                                    }
                                }
                            },
                            no_stnk: {
                                validators: {
                                    notEmpty: {
                                        message: "No STNK tidak boleh kosong"
                                    }
                                }
                            },
                            owner_name: {
                                validators: {
                                    notEmpty: {
                                        message: "Nama Lengkap tidak boleh kosong"
                                    }
                                }
                            },
                            owner_nik: {
                                validators: {
                                    callback: {
                                        callback: function (input) {
                                            if (!input.value) {
                                                return {
                                                    valid: false,
                                                    message: 'NIK tidak boleh kosong'
                                                };
                                            }
                                            else if (input.value) {
                                                if (input.value.length != 16) {
                                                    return {
                                                        valid: false,
                                                        message: 'NIK tidak valid'
                                                    };
                                                }
                                            }
                                            return true
                                        }
                                    }
                                }
                            },
                            owner_email: {
                                validators: {
                                    notEmpty: {
                                        message: "Email tidak boleh kosong"
                                    },
                                    emailAddress: {
                                        message: 'Email tidak valid'
                                    }
                                }
                            },
                            owner_phone: {
                                validators: {
                                    stringLength: {
                                        min: 9,
                                        max: 15,
                                        message: 'No Handphone tidak valid'
                                    },
                                    notEmpty: {
                                        message: "No Handphone tidak boleh kosong"
                                    }
                                }
                            },
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: ".fv-row",
                                eleInvalidClass: "",
                                eleValidClass: ""
                            })
                        },
                    })
                ),
                r.getCurrentStepIndex() == 2 ? o.addEventListener("click", function (e) {
                    a[1].validate().then(function (t) {
                        console.log("validated!"), console.log(t),
                            "Valid" == t ?
                                (e.preventDefault(), (o.disabled = !0),
                                    o.setAttribute("data-kt-indicator", "on"),
                                    ajaxSubmit()
                                )
                                :
                                Swal.fire({
                                    text: "Maaf, sepertinya input masih belum sesuai. Silahkan cek kembali",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-cyan"
                                    },
                                }).then(function () {
                                    scrollToContent(0);
                                });
                    });
                }) : o.addEventListener("click", function (e) {
                    a[2].validate().then(function (t) {
                        console.log("validated!"),
                            "Valid" == t ?
                                (e.preventDefault(), (o.disabled = !0),
                                    o.setAttribute("data-kt-indicator", "on"),
                                    ajaxSubmit()
                                )
                                :
                                Swal.fire({
                                    text: "Maaf, sepertinya input masih belum sesuai. Silahkan cek kembali",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-cyan"
                                    },
                                }).then(function () {
                                    scrollToContent(0);
                                });
                    });
                })
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTCreateAccount.init();
});