"use strict";

var KTCreateAccount = (function () {
    var e,
        t,
        i,
        o,
        s,
        r,
        a = [];

    const scrollToContent = function (step) {
        const element = document.getElementsByClassName("content-element")[
            step
        ];
        scrolling(element);
    }
    const scrolling = (element) => {
        const offset = 140;
        const bodyRect = document.body.getBoundingClientRect().top;
        const elementRect = element.getBoundingClientRect().top;
        const elementPosition = elementRect - bodyRect;
        const offsetPosition = elementPosition - offset;
        window.scrollTo({
            top: offsetPosition,
            behavior: "smooth",
        });
    };

    const ajaxSubmit = function () {
        const form = document.querySelector('#daftar').querySelector('form')
        $.ajax({
            url: form.getAttribute('action'),
            data: new FormData(form),
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (response) {
                console.log(response);
                Swal.fire({
                    text: response.notification.message,
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "Oke, saya cek kembali",
                    customClass: {
                        confirmButton: "btn btn-orange"
                    },
                }).then(function () {
                    o.removeAttribute("data-kt-indicator"), (o.disabled = !1)
                });
            },
            error: function (response) {
                Swal.fire({
                    text: response.responseJSON.message ?? response.responseJSON.notification.message,
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: "Oke, saya cek kembali",
                    customClass: {
                        confirmButton: "btn btn-cyan"
                    },
                }).then(function () {
                    o.removeAttribute("data-kt-indicator"), (o.disabled = !1)
                });
            }
        })
    }
    return {
        init: function () {
            (e = document.querySelector("#kt_modal_create_account")) && new bootstrap.Modal(e),
                (t = document.querySelector("#form-horizontal")),
                (i = t.querySelector("#registration-form")),
                (o = t.querySelector('[data-kt-stepper-action="submit"]')),
                (s = t.querySelector('[data-kt-stepper-action="next"]')),
                (r = new KTStepper(t)).on("kt.stepper.changed", function (e) {
                    // 4 === r.getCurrentStepIndex() ?
                    //     (o.classList.add("d-none"), s.classList.add("d-none")) :
                    (o.classList.remove("d-inline-block"), o.classList.remove("d-none"), s.classList.remove("d-none"));
                }),
                r.on("kt.stepper.next", function (e) {
                    // console.log("stepper.next");                    

                    var t = a[e.getCurrentStepIndex() - 1];
                    t
                        ?
                        t.validate().then(async function (t) {
                            console.log(),
                                "Valid" == t ?
                                    (console.log(t), e.goNext(), scrollToContent(0)) :
                                    Swal.fire({
                                        text: "Maaf, sepertinya input masih belum sesuai. Silahkan cek kembali",
                                        icon: "error",
                                        buttonsStyling: !1,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn btn-cyan"
                                        },
                                    }).then(function () {
                                        scrollToContent(0);
                                    });
                        }) :
                        (e.goNext(), scrollToContent(0));
                }),
                r.on("kt.stepper.previous", function (e) {
                    //console.log("stepper.previous")
                    e.goPrevious(), scrollToContent(0);
                }),
                a.push(
                    FormValidation.formValidation(i, {
                        fields: {
                           
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: ".fv-row",
                                eleInvalidClass: "",
                                eleValidClass: ""
                            })
                        },
                    })
                ),
                a.push(
                    FormValidation.formValidation(i, {
                        fields: {
                           
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: ".fv-row",
                                eleInvalidClass: "",
                                eleValidClass: ""
                            })
                        },
                    })
                ),
                a.push(
                    FormValidation.formValidation(i, {
                        fields: {
                           
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: ".fv-row",
                                eleInvalidClass: "",
                                eleValidClass: ""
                            })
                        },
                    })
                ),
                r.getCurrentStepIndex() == 2 ? o.addEventListener("click", function (e) {
                    a[1].validate().then(function (t) {
                        console.log("validated!"),
                            "Valid" == t ?
                                (e.preventDefault(), (o.disabled = !0),
                                    o.setAttribute("data-kt-indicator", "on"),
                                    ajaxSubmit()
                                )
                                :
                                Swal.fire({
                                    text: "Maaf, sepertinya input masih belum sesuai. Silahkan cek kembali",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-cyan"
                                    },
                                }).then(function () {
                                    scrollToContent(0);
                                });
                    });
                }) : o.addEventListener("click", function (e) {
                    a[1].validate().then(function (t) {
                        console.log("validated!"),
                            "Valid" == t ?
                                (e.preventDefault(), (o.disabled = !0),
                                    o.setAttribute("data-kt-indicator", "on"),
                                    ajaxSubmit()
                                )
                                :
                                Swal.fire({
                                    text: "Maaf, sepertinya input masih belum sesuai. Silahkan cek kembali",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-cyan"
                                    },
                                }).then(function () {
                                    scrollToContent(0);
                                });
                    });
                })
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTCreateAccount.init();
});