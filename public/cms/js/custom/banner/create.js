/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*******************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/authentication/sign-in/general.js ***!
      \*******************************************************************************************/


    // Class definition
    var KTUmrohCreate = function () {
        // Elements
        var form;
        var submitButton;
        var validator;

        // Handle form
        var handleForm = function (e) {
            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            validator = FormValidation.formValidation(
                form,
                {
                    fields: {
                        'file': {
                            validators: {
                                notEmpty: {
                                    message: 'Please upload Banner first'
                                },
                                file: {
                                    extension: 'png,jpg,jpeg,mp4,avi,mkv',
                                    maxSize: '20971520',
                                    message: 'Only accept file image as PNG, JPG, JPEG or video as MP4, AVI, MKV & Maximum File 20Mb'
                                }
                            }
                        },
                        'thumbnail': {
                            validators: {
                                file: {
                                    extension: 'png,jpg,jpeg',
                                    maxSize: '5242880',
                                    message: 'Only accept file image as PNG, JPG, JPEG & Maximum File 5Mb'
                                },
                                callback: {
                                    message: 'Can not be empty if Banner as video',
                                    callback: function () {
                                        const file = document.getElementById("image").files[0]
                                        if (file) {
                                            if (!file.type.includes('image/')) {
                                                console.log(file.type);
                                                if (!document.getElementById("thumbnail").files[0]) {
                                                    return false;
                                                }
                                            }
                                        }
                                        return true;
                                    }
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row'
                        })
                    }
                }
            );

            // Handle form submit
            submitButton.addEventListener('click', function (e) {
                // Prevent button default action
                e.preventDefault();

                // Validate form
                validator.validate().then(function (status) {
                    if (status == 'Valid') {
                        // Show loading indication
                        submitButton.setAttribute('data-kt-indicator', 'on');

                        // Disable button to avoid multiple click 
                        submitButton.disabled = true;


                        // Simulate ajax request
                        setTimeout(function () {
                            // Hide loading indication
                            submitButton.removeAttribute('data-kt-indicator');

                            // Enable button
                            submitButton.disabled = false;

                            // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                            Swal.fire({
                                text: "Please be patient!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 1000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    // form.querySelector('[name="email"]').value = "";
                                    // form.querySelector('[name="password"]').value = "";
                                }
                                form.submit(); // submit form
                            });
                        }, 500);
                    } else {
                        // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                        Swal.fire({
                            text: "Please check your input",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    }
                });
            });
        }

        // Public functions
        return {
            // Initialization
            init: function () {
                form = document.querySelector('#kt_create_form');
                submitButton = document.querySelector('#kt_create_submit');

                handleForm();
            }
        };
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTUmrohCreate.init();
    });

    /******/
})()
    ;
//# sourceMappingURL=general.js.map