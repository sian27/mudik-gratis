/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../themes/metronic/html/demo1/src/js/custom/widgets.js":
/*!********************************************************************!*\
  !*** ../../../themes/metronic/html/demo1/src/js/custom/widgets.js ***!
  \********************************************************************/
/***/ ((module) => {

                // Class definition
                var KTWidgets = function () {

                    var initVisitChart = function () {
                        var charts = document.querySelectorAll('.visit-chart');

                        [].slice.call(charts).map(function (element) {
                            var height = parseInt(KTUtil.css(element, 'height'));

                            if (!element) {
                                return;
                            }

                            var color = element.getAttribute('data-kt-chart-color');

                            var url = element.getAttribute('data-url');

                            var labelColor = KTUtil.getCssVariableValue('--bs-' + 'gray-800');
                            var strokeColor = KTUtil.getCssVariableValue('--bs-' + 'gray-300');
                            var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
                            var baseColor = KTUtil.getCssVariableValue('--bs-' + color);
                            var lightColor = KTUtil.getCssVariableValue('--bs-light-' + color);

                            var building_id = document.getElementById('filter-dashboard').value;

                            $.ajax({
                                url: url,
                                method: "GET",
                                data: { building_id: building_id, },
                                success: function (data) {
                                    const label = data.data.label
                                    const value = data.data.value
                                    document.querySelector('.label-visit-chart').innerHTML = "Period : " + data.text.first + " - " + data.text.last

                                    var options = {
                                        series: [{
                                            name: 'Visitor',
                                            data: value
                                        }],
                                        chart: {
                                            fontFamily: 'inherit',
                                            type: 'area',
                                            height: 350,
                                            toolbar: {
                                                show: false
                                            }
                                        },
                                        plotOptions: {

                                        },
                                        legend: {
                                            show: false
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function (value) {
                                                return new Intl.NumberFormat().format(value);
                                            },
                                            background: {
                                                enabled: true,
                                                foreColor: '#009EF7',
                                                padding: 4,
                                                borderRadius: 2,
                                                borderWidth: 1,
                                                borderColor: '#fff',
                                                opacity: 0.9,
                                                dropShadow: {
                                                    enabled: false,
                                                    top: 1,
                                                    left: 1,
                                                    blur: 1,
                                                    color: '#000',
                                                    opacity: 0.45
                                                }
                                            },
                                        },
                                        fill: {
                                            type: 'solid',
                                            opacity: 1
                                        },
                                        stroke: {
                                            curve: 'smooth',
                                            show: true,
                                            width: 3,
                                            colors: [baseColor]
                                        },
                                        xaxis: {
                                            categories: label,
                                            axisBorder: {
                                                show: false,
                                            },
                                            axisTicks: {
                                                show: false
                                            },
                                            type: 'datetime',
                                            tickPlacement: 'between',
                                            labels: {
                                                format: 'MMM',
                                                showDuplicates: false,
                                                rotate: -45,
                                                rotateAlways: true,
                                                // trim: true,
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            },
                                            crosshairs: {
                                                position: 'front',
                                                stroke: {
                                                    color: baseColor,
                                                    width: 1,
                                                    dashArray: 3
                                                }
                                            },
                                            tooltip: {
                                                enabled: true,
                                                formatter: undefined,
                                                offsetY: 0,
                                                style: {
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        yaxis: {
                                            // show: false,
                                            min: 0,
                                            labels: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        states: {
                                            normal: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            hover: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            active: {
                                                allowMultipleDataPointsSelection: false,
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            }
                                        },
                                        tooltip: {
                                            style: {
                                                fontSize: '12px'
                                            },
                                            y: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                            },
                                            x: {
                                                format: "MMM yyyy",
                                            },
                                        },
                                        colors: [lightColor],
                                        grid: {
                                            borderColor: borderColor,
                                            padding: {
                                                left: 22,
                                                right: 18,
                                            },
                                            strokeDashArray: 4,
                                            yaxis: {
                                                lines: {
                                                    show: true
                                                }
                                            }
                                        },
                                        markers: {
                                            strokeColor: baseColor,
                                            strokeWidth: 3
                                        }
                                    };

                                    var chart = new ApexCharts(element, options);
                                    chart.render();

                                },
                                error: function () {
                                    toastr.error("Errors");
                                }
                            })
                        });
                    }

                    var initMeetingRoomChart = function () {
                        var charts = document.querySelectorAll('.meeting-room-chart');

                        [].slice.call(charts).map(function (element) {
                            var height = parseInt(KTUtil.css(element, 'height'));

                            if (!element) {
                                return;
                            }

                            var color = element.getAttribute('data-kt-chart-color');

                            var url = element.getAttribute('data-url');

                            var labelColor = KTUtil.getCssVariableValue('--bs-' + 'gray-800');
                            var strokeColor = KTUtil.getCssVariableValue('--bs-' + 'gray-300');
                            var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
                            var baseColor = KTUtil.getCssVariableValue('--bs-' + color);
                            var lightColor = KTUtil.getCssVariableValue('--bs-light-' + color);

                            var building_id = document.getElementById('filter-dashboard').value;

                            $.ajax({
                                url: url,
                                method: "GET",
                                data: { building_id: building_id, },
                                success: function (data) {
                                    const label = data.data.label
                                    const value = data.data.value
                                    document.querySelector('.label-meeting-room-chart').innerHTML = "Period : " + data.text.first + " - " + data.text.last

                                    var options = {
                                        series: [{
                                            name: 'Visitor',
                                            data: value
                                        }],
                                        chart: {
                                            fontFamily: 'inherit',
                                            type: 'area',
                                            height: 350,
                                            toolbar: {
                                                show: false
                                            }
                                        },
                                        plotOptions: {

                                        },
                                        legend: {
                                            show: false
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function (value) {
                                                return new Intl.NumberFormat().format(value);
                                            },
                                            background: {
                                                enabled: true,
                                                foreColor: '#009EF7',
                                                padding: 4,
                                                borderRadius: 2,
                                                borderWidth: 1,
                                                borderColor: '#fff',
                                                opacity: 0.9,
                                                dropShadow: {
                                                    enabled: false,
                                                    top: 1,
                                                    left: 1,
                                                    blur: 1,
                                                    color: '#000',
                                                    opacity: 0.45
                                                }
                                            },
                                        },
                                        fill: {
                                            type: 'solid',
                                            opacity: 1
                                        },
                                        stroke: {
                                            curve: 'smooth',
                                            show: true,
                                            width: 3,
                                            colors: [baseColor]
                                        },
                                        xaxis: {
                                            categories: label,
                                            axisBorder: {
                                                show: false,
                                            },
                                            axisTicks: {
                                                show: false
                                            },
                                            type: 'datetime',
                                            tickPlacement: 'between',
                                            labels: {
                                                format: 'MMM',
                                                showDuplicates: false,
                                                rotate: -45,
                                                rotateAlways: true,
                                                // trim: true,
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            },
                                            crosshairs: {
                                                position: 'front',
                                                stroke: {
                                                    color: baseColor,
                                                    width: 1,
                                                    dashArray: 3
                                                }
                                            },
                                            tooltip: {
                                                enabled: true,
                                                formatter: undefined,
                                                offsetY: 0,
                                                style: {
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        yaxis: {
                                            // show: false,
                                            min: 0,
                                            labels: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        states: {
                                            normal: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            hover: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            active: {
                                                allowMultipleDataPointsSelection: false,
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            }
                                        },
                                        tooltip: {
                                            style: {
                                                fontSize: '12px'
                                            },
                                            y: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                            },
                                            x: {
                                                format: "MMM yyyy",
                                            },
                                        },
                                        colors: [lightColor],
                                        grid: {
                                            borderColor: borderColor,
                                            padding: {
                                                left: 22,
                                                right: 18,
                                            },
                                            strokeDashArray: 4,
                                            yaxis: {
                                                lines: {
                                                    show: true
                                                }
                                            }
                                        },
                                        markers: {
                                            strokeColor: baseColor,
                                            strokeWidth: 3
                                        }
                                    };

                                    var chart = new ApexCharts(element, options);
                                    chart.render();

                                },
                                error: function () {
                                    toastr.error("Errors");
                                }
                            })
                        });
                    }

                    var initWorkstationChart = function () {
                        var charts = document.querySelectorAll('.workstation-chart');

                        [].slice.call(charts).map(function (element) {
                            var height = parseInt(KTUtil.css(element, 'height'));

                            if (!element) {
                                return;
                            }

                            var color = element.getAttribute('data-kt-chart-color');

                            var url = element.getAttribute('data-url');

                            var labelColor = KTUtil.getCssVariableValue('--bs-' + 'gray-800');
                            var strokeColor = KTUtil.getCssVariableValue('--bs-' + 'gray-300');
                            var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
                            var baseColor = KTUtil.getCssVariableValue('--bs-' + color);
                            var lightColor = KTUtil.getCssVariableValue('--bs-light-' + color);

                            var building_id = document.getElementById('filter-dashboard').value;

                            $.ajax({
                                url: url,
                                method: "GET",
                                data: { building_id: building_id, },
                                success: function (data) {
                                    const label = data.data.label
                                    const value = data.data.value
                                    document.querySelector('.label-workstation-chart').innerHTML = "Period : " + data.text.first + " - " + data.text.last

                                    var options = {
                                        series: [{
                                            name: 'Visitor',
                                            data: value
                                        }],
                                        chart: {
                                            fontFamily: 'inherit',
                                            type: 'area',
                                            height: 350,
                                            toolbar: {
                                                show: false
                                            }
                                        },
                                        plotOptions: {

                                        },
                                        legend: {
                                            show: false
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function (value) {
                                                return new Intl.NumberFormat().format(value);
                                            },
                                            background: {
                                                enabled: true,
                                                foreColor: '#009EF7',
                                                padding: 4,
                                                borderRadius: 2,
                                                borderWidth: 1,
                                                borderColor: '#fff',
                                                opacity: 0.9,
                                                dropShadow: {
                                                    enabled: false,
                                                    top: 1,
                                                    left: 1,
                                                    blur: 1,
                                                    color: '#000',
                                                    opacity: 0.45
                                                }
                                            },
                                        },
                                        fill: {
                                            type: 'solid',
                                            opacity: 1
                                        },
                                        stroke: {
                                            curve: 'smooth',
                                            show: true,
                                            width: 3,
                                            colors: [baseColor]
                                        },
                                        xaxis: {
                                            categories: label,
                                            axisBorder: {
                                                show: false,
                                            },
                                            axisTicks: {
                                                show: false
                                            },
                                            type: 'datetime',
                                            tickPlacement: 'between',
                                            labels: {
                                                format: 'MMM',
                                                showDuplicates: false,
                                                rotate: -45,
                                                rotateAlways: true,
                                                // trim: true,
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            },
                                            crosshairs: {
                                                position: 'front',
                                                stroke: {
                                                    color: baseColor,
                                                    width: 1,
                                                    dashArray: 3
                                                }
                                            },
                                            tooltip: {
                                                enabled: true,
                                                formatter: undefined,
                                                offsetY: 0,
                                                style: {
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        yaxis: {
                                            // show: false,
                                            min: 0,
                                            labels: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        states: {
                                            normal: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            hover: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            active: {
                                                allowMultipleDataPointsSelection: false,
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            }
                                        },
                                        tooltip: {
                                            style: {
                                                fontSize: '12px'
                                            },
                                            y: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                            },
                                            x: {
                                                format: "MMM yyyy",
                                            },
                                        },
                                        colors: [lightColor],
                                        grid: {
                                            borderColor: borderColor,
                                            padding: {
                                                left: 22,
                                                right: 18,
                                            },
                                            strokeDashArray: 4,
                                            yaxis: {
                                                lines: {
                                                    show: true
                                                }
                                            }
                                        },
                                        markers: {
                                            strokeColor: baseColor,
                                            strokeWidth: 3
                                        }
                                    };

                                    var chart = new ApexCharts(element, options);
                                    chart.render();

                                },
                                error: function () {
                                    toastr.error("Errors");
                                }
                            })
                        });
                    }

                    var initAttandanceChart = function () {
                        var charts = document.querySelectorAll('.attandance-chart');

                        [].slice.call(charts).map(function (element) {
                            var height = parseInt(KTUtil.css(element, 'height'));

                            if (!element) {
                                return;
                            }

                            var color = element.getAttribute('data-kt-chart-color');

                            var url = element.getAttribute('data-url');

                            var labelColor = KTUtil.getCssVariableValue('--bs-' + 'gray-800');
                            var strokeColor = KTUtil.getCssVariableValue('--bs-' + 'gray-300');
                            var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
                            var baseColor = KTUtil.getCssVariableValue('--bs-' + color);
                            var lightColor = KTUtil.getCssVariableValue('--bs-light-' + color);

                            var building_id = document.getElementById('filter-dashboard').value;

                            $.ajax({
                                url: url,
                                method: "GET",
                                data: { building_id: building_id, },
                                success: function (data) {
                                    const label = data.data.label
                                    const value = data.data.value
                                    document.querySelector('.label-attandance-chart').innerHTML = "Period : " + data.text.first + " - " + data.text.last

                                    var options = {
                                        series: [{
                                            name: 'Visitor',
                                            data: value
                                        }],
                                        chart: {
                                            fontFamily: 'inherit',
                                            type: 'area',
                                            height: 350,
                                            toolbar: {
                                                show: false
                                            }
                                        },
                                        plotOptions: {

                                        },
                                        legend: {
                                            show: false
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function (value) {
                                                return new Intl.NumberFormat().format(value);
                                            },
                                            background: {
                                                enabled: true,
                                                foreColor: '#009EF7',
                                                padding: 4,
                                                borderRadius: 2,
                                                borderWidth: 1,
                                                borderColor: '#fff',
                                                opacity: 0.9,
                                                dropShadow: {
                                                    enabled: false,
                                                    top: 1,
                                                    left: 1,
                                                    blur: 1,
                                                    color: '#000',
                                                    opacity: 0.45
                                                }
                                            },
                                        },
                                        fill: {
                                            type: 'solid',
                                            opacity: 1
                                        },
                                        stroke: {
                                            curve: 'smooth',
                                            show: true,
                                            width: 3,
                                            colors: [baseColor]
                                        },
                                        xaxis: {
                                            categories: label,
                                            axisBorder: {
                                                show: false,
                                            },
                                            axisTicks: {
                                                show: false
                                            },
                                            type: 'datetime',
                                            tickPlacement: 'between',
                                            labels: {
                                                format: 'MMM',
                                                showDuplicates: false,
                                                rotate: -45,
                                                rotateAlways: true,
                                                // trim: true,
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            },
                                            crosshairs: {
                                                position: 'front',
                                                stroke: {
                                                    color: baseColor,
                                                    width: 1,
                                                    dashArray: 3
                                                }
                                            },
                                            tooltip: {
                                                enabled: true,
                                                formatter: undefined,
                                                offsetY: 0,
                                                style: {
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        yaxis: {
                                            // show: false,
                                            min: 0,
                                            labels: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        states: {
                                            normal: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            hover: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            active: {
                                                allowMultipleDataPointsSelection: false,
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            }
                                        },
                                        tooltip: {
                                            style: {
                                                fontSize: '12px'
                                            },
                                            y: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                            },
                                            x: {
                                                format: "MMM yyyy",
                                            },
                                        },
                                        colors: [lightColor],
                                        grid: {
                                            borderColor: borderColor,
                                            padding: {
                                                left: 22,
                                                right: 18,
                                            },
                                            strokeDashArray: 4,
                                            yaxis: {
                                                lines: {
                                                    show: true
                                                }
                                            }
                                        },
                                        markers: {
                                            strokeColor: baseColor,
                                            strokeWidth: 3
                                        }
                                    };

                                    var chart = new ApexCharts(element, options);
                                    chart.render();

                                },
                                error: function () {
                                    toastr.error("Errors");
                                }
                            })
                        });
                    }

                    var initSendChart = function () {
                        var charts = document.querySelectorAll('.send-chart');

                        [].slice.call(charts).map(function (element) {
                            var height = parseInt(KTUtil.css(element, 'height'));

                            if (!element) {
                                return;
                            }

                            var color = element.getAttribute('data-kt-chart-color');

                            var url = element.getAttribute('data-url');

                            var labelColor = KTUtil.getCssVariableValue('--bs-' + 'gray-800');
                            var strokeColor = KTUtil.getCssVariableValue('--bs-' + 'gray-300');
                            var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
                            var baseColor = KTUtil.getCssVariableValue('--bs-' + color);
                            var lightColor = KTUtil.getCssVariableValue('--bs-light-' + color);

                            var building_id = document.getElementById('filter-dashboard').value;

                            $.ajax({
                                url: url,
                                method: "GET",
                                data: { building_id: building_id, },
                                success: function (data) {
                                    const label = data.data.label
                                    const internal = data.data.internal
                                    const external = data.data.external
                                    document.querySelector('.label-send-chart').innerHTML = "Period : " + data.text.first + " - " + data.text.last

                                    var options = {
                                        series: [
                                            {
                                                name: 'Internal',
                                                data: internal
                                            },
                                            {
                                                name: 'External',
                                                data: external
                                            },
                                        ],
                                        chart: {
                                            fontFamily: 'inherit',
                                            type: 'line',
                                            height: 350,
                                            toolbar: {
                                                show: false
                                            }
                                        },
                                        plotOptions: {

                                        },
                                        legend: {
                                            show: false
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            offsetY: -3,
                                            formatter: function (value) {
                                                return new Intl.NumberFormat().format(value);
                                            },
                                            background: {
                                                enabled: false,
                                                foreColor: '#009EF7',
                                                padding: 4,
                                                borderRadius: 2,
                                                borderWidth: 1,
                                                borderColor: '#fff',
                                                opacity: 0.9,
                                                dropShadow: {
                                                    enabled: false,
                                                    top: 1,
                                                    left: 1,
                                                    blur: 1,
                                                    color: '#000',
                                                    opacity: 0.45
                                                }
                                            },
                                        },
                                        fill: {
                                            type: 'solid',
                                            opacity: 1
                                        },
                                        stroke: {
                                            curve: 'smooth',
                                            show: true,
                                            width: 3,
                                            colors: ["#63E394", "#F46865"],
                                        },
                                        xaxis: {
                                            categories: label,
                                            axisBorder: {
                                                show: false,
                                            },
                                            axisTicks: {
                                                show: false
                                            },
                                            type: 'datetime',
                                            tickPlacement: 'between',
                                            labels: {
                                                format: 'MMM',
                                                showDuplicates: false,
                                                rotate: -45,
                                                rotateAlways: true,
                                                // trim: true,
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            },
                                            crosshairs: {
                                                position: 'front',
                                                stroke: {
                                                    color: baseColor,
                                                    width: 1,
                                                    dashArray: 3
                                                }
                                            },
                                            tooltip: {
                                                enabled: true,
                                                formatter: undefined,
                                                offsetY: 0,
                                                style: {
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        yaxis: {
                                            // show: false,
                                            min: 0,
                                            labels: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        states: {
                                            normal: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            hover: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            active: {
                                                allowMultipleDataPointsSelection: false,
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            }
                                        },
                                        tooltip: {
                                            style: {
                                                fontSize: '12px'
                                            },
                                            y: {
                                                formatter: function (value) {
                                                    return value != null ? new Intl.NumberFormat().format(value) : 0;
                                                },
                                            },
                                            x: {
                                                format: "MMM yyyy",
                                            },
                                        },
                                        colors: ["#63E394", "#F46865"],
                                        grid: {
                                            borderColor: borderColor,
                                            padding: {
                                                left: 22,
                                                right: 18,
                                            },
                                            strokeDashArray: 4,
                                            yaxis: {
                                                lines: {
                                                    show: true
                                                }
                                            }
                                        },
                                        markers: {
                                            strokeColor: baseColor,
                                            strokeWidth: 3
                                        }
                                    };

                                    var chart = new ApexCharts(element, options);
                                    chart.render();

                                },
                                error: function () {
                                    toastr.error("Errors");
                                }
                            })
                        });
                    }

                    var initTransportChart = function () {
                        var charts = document.querySelectorAll('.transport-chart');

                        [].slice.call(charts).map(function (element) {
                            var height = parseInt(KTUtil.css(element, 'height'));

                            if (!element) {
                                return;
                            }

                            var color = element.getAttribute('data-kt-chart-color');

                            var url = element.getAttribute('data-url');

                            var labelColor = KTUtil.getCssVariableValue('--bs-' + 'gray-800');
                            var strokeColor = KTUtil.getCssVariableValue('--bs-' + 'gray-300');
                            var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
                            var baseColor = KTUtil.getCssVariableValue('--bs-' + color);
                            var lightColor = KTUtil.getCssVariableValue('--bs-light-' + color);

                            var building_id = document.getElementById('filter-dashboard').value;

                            $.ajax({
                                url: url,
                                method: "GET",
                                data: { building_id: building_id, },
                                success: function (data) {
                                    const label = data.data.label
                                    const internal = data.data.internal
                                    const external = data.data.external
                                    document.querySelector('.label-transport-chart').innerHTML = "Period : " + data.text.first + " - " + data.text.last

                                    var options = {
                                        series: [
                                            {
                                                name: 'Internal',
                                                data: internal
                                            },
                                            {
                                                name: 'External',
                                                data: external
                                            },
                                        ],
                                        chart: {
                                            fontFamily: 'inherit',
                                            type: 'line',
                                            height: 350,
                                            toolbar: {
                                                show: false
                                            }
                                        },
                                        plotOptions: {

                                        },
                                        legend: {
                                            show: false
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            offsetY: -3,
                                            formatter: function (value) {
                                                return new Intl.NumberFormat().format(value);
                                            },
                                            background: {
                                                enabled: false,
                                                foreColor: '#009EF7',
                                                padding: 4,
                                                borderRadius: 2,
                                                borderWidth: 1,
                                                borderColor: '#fff',
                                                opacity: 0.9,
                                                dropShadow: {
                                                    enabled: false,
                                                    top: 1,
                                                    left: 1,
                                                    blur: 1,
                                                    color: '#000',
                                                    opacity: 0.45
                                                }
                                            },
                                        },
                                        fill: {
                                            type: 'solid',
                                            opacity: 1
                                        },
                                        stroke: {
                                            curve: 'smooth',
                                            show: true,
                                            width: 3,
                                            colors: ["#63E394", "#F46865"],
                                        },
                                        xaxis: {
                                            categories: label,
                                            axisBorder: {
                                                show: false,
                                            },
                                            axisTicks: {
                                                show: false
                                            },
                                            type: 'datetime',
                                            tickPlacement: 'between',
                                            labels: {
                                                format: 'MMM',
                                                showDuplicates: false,
                                                rotate: -45,
                                                rotateAlways: true,
                                                // trim: true,
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            },
                                            crosshairs: {
                                                position: 'front',
                                                stroke: {
                                                    color: baseColor,
                                                    width: 1,
                                                    dashArray: 3
                                                }
                                            },
                                            tooltip: {
                                                enabled: true,
                                                formatter: undefined,
                                                offsetY: 0,
                                                style: {
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        yaxis: {
                                            // show: false,
                                            min: 0,
                                            labels: {
                                                formatter: function (value) {
                                                    return new Intl.NumberFormat().format(value);
                                                },
                                                style: {
                                                    colors: labelColor,
                                                    fontSize: '12px'
                                                }
                                            }
                                        },
                                        states: {
                                            normal: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            hover: {
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            },
                                            active: {
                                                allowMultipleDataPointsSelection: false,
                                                filter: {
                                                    type: 'none',
                                                    value: 0
                                                }
                                            }
                                        },
                                        tooltip: {
                                            style: {
                                                fontSize: '12px'
                                            },
                                            y: {
                                                formatter: function (value) {
                                                    return value != null ? new Intl.NumberFormat().format(value) : 0;
                                                },
                                            },
                                            x: {
                                                format: "MMM yyyy",
                                            },
                                        },
                                        colors: ["#63E394", "#F46865"],
                                        grid: {
                                            borderColor: borderColor,
                                            padding: {
                                                left: 22,
                                                right: 18,
                                            },
                                            strokeDashArray: 4,
                                            yaxis: {
                                                lines: {
                                                    show: true
                                                }
                                            }
                                        },
                                        markers: {
                                            strokeColor: baseColor,
                                            strokeWidth: 3
                                        }
                                    };

                                    var chart = new ApexCharts(element, options);
                                    chart.render();

                                },
                                error: function () {
                                    toastr.error("Errors");
                                }
                            })
                        });
                    }


                    // Public methods
                    return {
                        init: function () {

                            initVisitChart();
                            initMeetingRoomChart();
                            initWorkstationChart();
                            initAttandanceChart();
                            initSendChart();
                            initTransportChart();
                        }
                    }
                }();

                // Webpack support
                if (true) {
                    module.exports = KTWidgets;
                }

                // On document ready
                KTUtil.onDOMContentLoaded(function () {
                    KTWidgets.init();
                });


                /***/
            })

        /******/
    });
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
            /******/
        }
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
            /******/
        };
/******/
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
        /******/
    }
/******/
/************************************************************************/
/******/
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("../../../themes/metronic/html/demo1/src/js/custom/widgets.js");
    /******/
    /******/
})()
    ;
//# sourceMappingURL=widgets.js.map