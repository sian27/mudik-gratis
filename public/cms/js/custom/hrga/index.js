/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/appss/list/list.js ***!
      \*************************************************************************************/


    // Class definition
    var KTCustomersList = function () {
        // Define shared variables
        var datatable;
        var table
        var module = document.querySelector('title').innerText;

        // Private functions
        var initJS = function () {


            // Init datatable --- more info on datatables: https://datatables.net/manual/
            datatable = $(table).DataTable({
                "info": false,
                'order': [],
                'columnDefs': [
                    { orderable: false, targets: 0 },
                    { orderable: false, targets: 1 },
                    { orderable: false, targets: 5 },
                    { searchable: false, targets: 0 },
                    { searchable: false, targets: 1 },
                    { searchable: false, targets: 5 },
                ]
            });

            // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
            datatable.on('draw');
        }

        // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
        var handleSearchDatatable = () => {
            const filterSearch = document.querySelector('[data-kt-table-filter="search"]');
            filterSearch.addEventListener('keyup', function (e) {
                datatable.search(e.target.value).draw();
            });
        }

        // Public methods
        return {
            init: function () {
                table = document.querySelector('#kt_table');

                if (!table) {
                    return;
                }

                initJS();
                handleSearchDatatable();
            }
        }
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTCustomersList.init();
    });
    /******/
})()
    ;
//# sourceMappingURL=list.js.map