/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/appss/list/list.js ***!
      \*************************************************************************************/


    // Class definition
    var KTCustomersList = function () {
        // Define shared variables
        var datatable;
        var table
        var module = document.querySelector('title').innerText;

        // Private functions
        var initJS = function () {


            // Init datatable --- more info on datatables: https://datatables.net/manual/
            datatable = $(table).DataTable({
                "info": false,
                'order': [],
                'columnDefs': [
                    { orderable: false, targets: 0 },
                    { orderable: false, targets: 1 },
                    { orderable: false, targets: 4 },
                    { searchable: false, targets: 0 },
                    { searchable: false, targets: 1 },
                    { searchable: false, targets: 4 },
                ]
            });

            // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
            datatable.on('draw', function () {
                handleDeleteRows();
                handleRestoreRows();
            });
        }

        // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
        var handleSearchDatatable = () => {
            const filterSearch = document.querySelector('[data-kt-table-filter="search"]');
            filterSearch.addEventListener('keyup', function (e) {
                datatable.search(e.target.value).draw();
            });
        }

        var handleUpdateDatatable = () => {
            const update = document.querySelector('[data-kt-table-filter="update"]');
            update.addEventListener('click', function (e) {
                e.preventDefault();

                // Show loading indication
                update.setAttribute('data-kt-indicator', 'on');

                // Disable button to avoid multiple click 
                update.disabled = true;

                Swal.fire({
                    text: 'Are you sure, to run update News Crawl?',
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, sure!",
                    cancelButtonText: "Cancel",
                    customClass: {
                        confirmButton: "btn fw-bold btn-success",
                        cancelButton: "btn fw-bold btn-active-light-primary"
                    }
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: update.dataset.url,
                            method: "POST",
                            data: { _token: update.dataset.token, },
                            success: function () {
                                // Hide loading indication
                                update.removeAttribute('data-kt-indicator');

                                // Enable button
                                update.disabled = false;
                                location.reload()
                            },
                            error: function () {
                                // Hide loading indication
                                update.removeAttribute('data-kt-indicator');

                                // Enable button
                                update.disabled = false;
                                window.location.reload()
                            }
                        })
                    } else if (result.dismiss === 'cancel') {
                        Swal.fire({
                            text: 'Ok canceled.',
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn fw-bold btn-primary",
                            }
                        });
                         // Hide loading indication
                         update.removeAttribute('data-kt-indicator');

                         // Enable button
                         update.disabled = false;
                    }
                });
            })
        }

        // Delete
        var handleDeleteRows = () => {
            // Select all restore buttons
            const deleteButtons = table.querySelectorAll('[data-kt-table-filter="delete_row"]');

            deleteButtons.forEach(d => {
                // Delete button on click
                d.addEventListener('click', function (e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get name
                    const Name = parent.querySelectorAll('td')[2].innerText;

                    const data = parent.querySelectorAll('td')[4].childNodes[1].childNodes[3];
                    const id = data.dataset.id
                    const token = data.dataset.token

                    // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                    Swal.fire({
                        text: 'Are you sure, to remove ' + module + ': "' + Name + '"?',
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Remove!",
                        cancelButtonText: "Cancel",
                        customClass: {
                            confirmButton: "btn fw-bold btn-danger",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function (result) {
                        if (result.value) {
                            Swal.fire({
                                text: "Please be patient!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 1000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            }).then(function () {
                                const url = data.dataset.url

                                $.ajax({
                                    url: url,
                                    method: "DELETE",
                                    data: { _token: token, id: id, },
                                    success: function () {
                                        location.reload()
                                    },
                                    error: function () {
                                        window.location.reload()
                                    }
                                })
                                // Remove current row
                                // datatable.row($(parent)).remove().draw();
                            });
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: 'Remove ' + module + ': "' + Name + '" has canceled.',
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Restore
        var handleRestoreRows = () => {
            // Select all restore buttons
            const restoreButtons = table.querySelectorAll('[data-kt-table-filter="restore_row"]');

            restoreButtons.forEach(d => {
                // Restore button on click
                d.addEventListener('click', function (e) {
                    e.preventDefault();

                    // Select parent row
                    const parent = e.target.closest('tr');

                    // Get name
                    const Name = parent.querySelectorAll('td')[2].innerText;

                    const data = parent.querySelectorAll('td')[4].childNodes[1].childNodes[3];
                    const id = data.dataset.id
                    const token = data.dataset.token

                    // SweetAlert2 pop up --- official docs reference: https://sweetalert2.github.io/
                    Swal.fire({
                        text: 'Are you sure, to restore ' + module + ': "' + Name + '"?',
                        icon: "warning",
                        showCancelButton: true,
                        buttonsStyling: false,
                        confirmButtonText: "Restore!",
                        cancelButtonText: "Cancel",
                        customClass: {
                            confirmButton: "btn fw-bold btn-success",
                            cancelButton: "btn fw-bold btn-active-light-primary"
                        }
                    }).then(function (result) {
                        if (result.value) {
                            Swal.fire({
                                text: "Please be patient!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 1000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            }).then(function () {
                                const url = data.dataset.url

                                $.ajax({
                                    url: url,
                                    method: "PUT",
                                    data: { _token: token, id: id },
                                    success: function () {
                                        location.reload()
                                    },
                                    error: function () {
                                        window.location.reload()
                                    }
                                })
                                // Remove current row
                                // datatable.row($(parent)).remove().draw();
                            });
                        } else if (result.dismiss === 'cancel') {
                            Swal.fire({
                                text: 'Restore ' + module + ': "' + Name + '" has canceled.',
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn fw-bold btn-primary",
                                }
                            });
                        }
                    });
                })
            });
        }

        // Public methods
        return {
            init: function () {
                table = document.querySelector('#kt_table');

                if (!table) {
                    return;
                }

                initJS();
                handleSearchDatatable();
                handleUpdateDatatable();
                handleDeleteRows();
                handleRestoreRows();
            }
        }
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTCustomersList.init();
    });
    /******/
})()
    ;
//# sourceMappingURL=list.js.map