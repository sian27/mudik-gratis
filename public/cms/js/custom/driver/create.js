/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
    var __webpack_exports__ = {};
    /*!*******************************************************************************************!*\
      !*** ../../../themes/metronic/html/demo1/src/js/custom/authentication/sign-in/general.js ***!
      \*******************************************************************************************/


    // Class definition
    var KTUmrohCreate = function () {
        // Elements
        var form;
        var submitButton;
        var validator;

        // Handle form
        var handleForm = function (e) {
            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            validator = FormValidation.formValidation(
                form,
                {
                    fields: {
                        'name': {
                            validators: {
                                notEmpty: {
                                    message: 'Name cannot be empty'
                                },
                            }
                        },
                        'email': {
                            validators: {
                                notEmpty: {
                                    message: 'Email cannot be empty'
                                },
                                emailAddress: {
                                    message: 'Email not valid'
                                },
                                remote: {
                                    url: function(){
                                        return form.querySelector('[name="email"]').dataset.url
                                    },
                                    type: 'POST',
                                    // Send { username: 'its value', email: 'its value' } to the back-end
                                    data: function () {
                                        return {
                                            email: form.querySelector('[name="email"]').value,
                                            token: form.querySelector('[name="_token"]').value
                                        };
                                    },
                                    message: 'The Email is not available',
                                },
                            }
                        },
                        'password': {
                            validators: {
                                notEmpty: {
                                    message: 'Password cannot be empty'
                                },
                                stringLength: {
                                    min: 8,
                                    message: 'The password min 8 character'
                                }

                            }
                        },
                        'nik': {
                            validators: {
                                notEmpty: {
                                    message: 'NIK cannot be empty'
                                },
                            }
                        },
                        'phone': {
                            validators: {
                                notEmpty: {
                                    message: 'Phone cannot be empty'
                                }
                            }
                        },
                        'role': {
                            validators: {
                                notEmpty: {
                                    message: 'Please choose Role first'
                                }
                            }
                        },                        
                        'image': {
                            validators: {
                                notEmpty: {
                                    message: 'Please upload Image first'
                                },
                                file: {
                                    extension: 'png,jpg,jpeg',
                                    maxSize: '5242880',
                                    message: 'Only accept image as PNG, JPG, JPEG & Maximum File 5Mb'
                                }
                            }
                        },
                        'name_own': {
                            validators: {
                                notEmpty: {
                                    message: 'Name Owner cannot be empty'
                                }
                            }
                        },    
                        'address': {
                            validators: {
                                notEmpty: {
                                    message: 'Address cannot be empty'
                                }
                            }
                        },    
                        'no_police': {
                            validators: {
                                notEmpty: {
                                    message: 'No Police cannot be empty'
                                }
                            }
                        },    
                        'merk_vehicle': {
                            validators: {
                                notEmpty: {
                                    message: 'Merk Vehicle cannot be empty'
                                }
                            }
                        },    
                        'type_vehicle': {
                            validators: {
                                notEmpty: {
                                    message: 'Type Vehicle cannot be empty'
                                }
                            }
                        },   
                        'model_year': {
                            validators: {
                                notEmpty: {
                                    message: 'Model Year cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 4,
                                    message: 'Year not valid'
                                }
                            }
                        },   
                        'no_chassis': {
                            validators: {
                                notEmpty: {
                                    message: 'No Chassis cannot be empty'
                                }
                            }
                        },   
                        'no_mechine': {
                            validators: {
                                notEmpty: {
                                    message: 'No Mechine cannot be empty'
                                }
                            }
                        },       
                        'tax_period': {
                            validators: {
                                notEmpty: {
                                    message: 'Tax Period cannot be empty'
                                }
                            }
                        },    
                        'stnk_period': {
                            validators: {
                                notEmpty: {
                                    message: 'STNK Period cannot be empty'
                                }
                            }
                        },    
                        'test_emisi': {
                            validators: {
                                notEmpty: {
                                    message: 'Test Emisi cannot be empty'
                                }
                            }
                        },    
                        'vehicle_image': {
                            validators: {
                                file: {
                                    extension: 'png,jpg,jpeg',
                                    maxSize: '5242880',
                                    message: 'Only accept image as PNG, JPG, JPEG & Maximum File 5Mb'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row'
                        })
                    }
                }
            );

            // Handle form submit
            submitButton.addEventListener('click', function (e) {
                // Prevent button default action
                e.preventDefault();

                // Validate form
                validator.validate().then(function (status) {
                    if (status == 'Valid') {
                        // Show loading indication
                        submitButton.setAttribute('data-kt-indicator', 'on');

                        // Disable button to avoid multiple click 
                        submitButton.disabled = true;


                        // Simulate ajax request
                        setTimeout(function () {
                            // Hide loading indication
                            submitButton.removeAttribute('data-kt-indicator');

                            // Enable button
                            submitButton.disabled = false;

                            // Show message popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                            Swal.fire({
                                text: "Please be patient!",
                                icon: "info",
                                buttonsStyling: false,
                                timer: 1000,
                                confirmButtonText: "Ok",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            }).then(function (result) {
                                if (result.isConfirmed) {
                                    // form.querySelector('[name="email"]').value = "";
                                    // form.querySelector('[name="password"]').value = "";
                                }
                                form.submit(); // submit form
                            });
                        }, 500);
                    } else {
                        // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                        Swal.fire({
                            text: "Please check your input",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    }
                });
            });
        }

        // Public functions
        return {
            // Initialization
            init: function () {
                form = document.querySelector('#kt_create_form');
                submitButton = document.querySelector('#kt_create_submit');

                handleForm();
            }
        };
    }();

    // On document ready
    KTUtil.onDOMContentLoaded(function () {
        KTUmrohCreate.init();
    });

    /******/
})()
    ;
//# sourceMappingURL=general.js.map