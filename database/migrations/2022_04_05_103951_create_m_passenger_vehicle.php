<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_passenger_vehicle', function (Blueprint $table) {
            $table->id();
            $table->integer('passenger_id', 0, 1);
            $table->integer('trip_id', 0, 1);
            $table->string('no_police')->nullable();
            $table->string('merk_vehicle')->nullable();
            $table->string('type_vehicle')->nullable();
            $table->string('color_vehicle')->nullable();
            $table->string('no_stnk')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('owner_nik')->nullable();
            $table->string('owner_no_kk')->nullable();
            $table->string('owner_email')->nullable();
            $table->string('owner_phone')->nullable();
            $table->enum('scan_booking', [0, 1])->default(0);
            $table->enum('scan_ticket', [0, 1])->default(0);
            $table->enum('send_booking', [0, 1])->nullable();
            $table->enum('send_ticket', [0, 1])->nullable();
            $table->date('verify_date')->nullable();
            $table->time('verify_time_start')->nullable();
            $table->time('verify_time_end')->nullable();
            $table->string('verify_place')->nullable();
            $table->timestamp('verify_offline')->nullable();
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_passenger_vehicle');
    }
};
