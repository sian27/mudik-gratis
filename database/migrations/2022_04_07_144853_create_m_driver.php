<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_driver', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('type', ['bus', 'truck']);
            $table->integer('bus_id',0,1)->nullable();
            $table->integer('truck_id',0,1)->nullable();
            $table->string('image');
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_driver');
    }
};
