<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_rute', function (Blueprint $table) {
            $table->id();
            $table->integer('rute_id', 0, 1);
            $table->integer('bus_id', 0, 1)->nullable();
            $table->integer('truck_id', 0, 1)->nullable();
            $table->enum('status', ['start', 'end']);
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_rute');
    }
};
