<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_passenger', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id', 0, 1)->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('nik')->nullable();
            $table->string('no_kk')->nullable();
            $table->integer('trip_id', 0, 1);
            $table->enum('gender', ['l', 'p'])->nullable();
            $table->string('phone')->nullable();
            $table->string('placebirth')->nullable();
            $table->date('datebirth')->nullable();
            $table->text('address')->nullable();
            $table->string('file_ktp')->nullable();
            $table->string('file_kk')->nullable();
            $table->enum('vaksin', [1, 2, 3, 0])->nullable();
            $table->string('file_booster')->nullable();
            $table->enum('type_vaksin', ['booster', 'pcr', 'antigen'])->nullable();
            $table->integer('puskes_id')->nullable();
            $table->enum('status', ['accept', 'reject', 'pending'])->default('pending');
            $table->enum('scan_booking', [0, 1])->default(0);
            $table->enum('scan_ticket', [0, 1])->default(0);
            $table->enum('send_booking', [0, 1])->nullable();
            $table->enum('send_ticket', [0, 1])->nullable();
            $table->date('verify_date')->nullable();
            $table->time('verify_time_start')->nullable();
            $table->time('verify_time_end')->nullable();
            $table->string('verify_place')->nullable();
            $table->timestamp('verify_offline')->nullable();
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_passenger');
    }
};
