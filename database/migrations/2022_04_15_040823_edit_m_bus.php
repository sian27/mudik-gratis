<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_bus', function (Blueprint $table) {
            $table->integer('khusus', 0,1)->default(0);
        });

        Schema::table('m_truck', function (Blueprint $table) {
            $table->integer('khusus', 0,1)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_bus', function (Blueprint $table) {
            $table->removeColumn('khusus');
        });

        Schema::table('m_truck', function (Blueprint $table) {
            $table->removeColumn('khusus');
        });
    }
};
