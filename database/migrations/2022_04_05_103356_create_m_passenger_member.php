<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('m_passenger_member', function (Blueprint $table) {
        //     $table->id();
        //     $table->integer('user_id', 0, 1);
        //     $table->string('name');
        //     $table->string('email');
        //     $table->string('nik');
        //     $table->string('no_kk');
        //     $table->string('place_of_birth');
        //     $table->date('date_of_birth');
        //     $table->string('phone');
        //     $table->enum('start_status', ['absent', 'onboard', 'complete'])->default('absent');
        //     $table->enum('end_status', ['absent', 'onboard', 'complete'])->default('absent');
        //     $table->timestamps();
        //     $table->timestamp("deleted_at")->nullable();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('m_passenger_member');
    }
};
