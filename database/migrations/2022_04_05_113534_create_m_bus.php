<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_bus', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('driver_id', 0,1);
            $table->integer('trip_id',0,1);
            $table->string('code');
            $table->string('no_police');
            $table->integer('quota', 0, 1);     
            $table->enum('is_full', [0,1])->default(0);
            $table->enum('status', ['start','end'])->default('start');
            // $table->enum('start_status', ['idle', 'progress', 'rest', 'trouble', 'arrive'])->default('idle');
            // $table->enum('end_status', ['idle', 'progress', 'rest', 'trouble', 'arrive'])->default('idle');
            $table->date('date_at');
            $table->time('time_at');
            $table->string('place_at');
            $table->string('longitude')->default('106.827153');
            $table->string('latitude')->default('-6.175392');
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_bus');
    }
};
