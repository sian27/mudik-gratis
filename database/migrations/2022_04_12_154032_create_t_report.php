<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_report', function (Blueprint $table) {
            $table->id();
            $table->integer('lo_id', 0, 1);
            $table->string('vehicle_code');
            $table->date('date');
            $table->time('time');
            $table->text('description');
            $table->enum('type', ['bus', 'truck']);
            $table->enum('status', ['trouble', 'rest']);
            $table->enum('is_done', ['yes', 'not_yet'])->nullable();
            $table->string('longitude');
            $table->string('latitude');
            $table->string('address');
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_report');
    }
};
