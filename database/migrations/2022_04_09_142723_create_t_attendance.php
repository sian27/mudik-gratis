<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_attendance', function (Blueprint $table) {
            $table->id();
            $table->integer("lo_id", false, true);
            $table->timestamp("check_in");
            $table->timestamp("check_out")->nullable();
            $table->string('start_longitude');
            $table->string('start_latitude');
            $table->text('start_address');
            $table->string('start_image')->nullable();
            $table->string('end_longitude')->nullable();
            $table->string('end_latitude')->nullable();
            $table->text('end_address')->nullable();
            $table->string('end_image')->nullable();
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_attendance');
    }
};
