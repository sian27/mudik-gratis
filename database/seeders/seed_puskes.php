<?php

namespace Database\Seeders;

use App\Models\Master\MasterPuskes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_puskes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(public_path("import/puskes.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                MasterPuskes::insert([
                    'kabupaten' => $data[1],
                    'kecamatan' => $data[2],
                    'name' => $data[3],
                    'address' => $data[4],
                    'phone' => $data[5]
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
