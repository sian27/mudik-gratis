<?php

namespace Database\Seeders;

use App\Models\Master\MasterContent;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_content extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // dummy banner;
        MasterContent::create([
            "slug" => "banner",
            "type" => "html",
            "html" => '<div class="mb-1 text-center"> Mudik Gratis </div> <div class="mb-1 text-center"> Jakarta 2022 </div> <div class="fs-5 mb-1 text-center fw-normal">Mudik Asik, Dulu Pandemi Kini Jadi Endemik</div> <div class="fs-5 mb-1 text-center fw-normal">Perjalanan Santai, Meskipun Beramai-Ramai</div> <div class="fs-5 mb-1 text-center fw-normal">Tetap Sehat, Selamat dan Perekonomian Menguat </div>'
        ]);
        MasterContent::create([
            "slug" => "banner",
            "type" => "file",
            "file" => 'default/banner/banner.png'
        ]);

        MasterContent::create([
            "slug" => "syarat-pendaftaran",
            "type" => "html",
            "html" => '<ol> <li> Mudik Gratis <ol type="a"> <li>Warga memiliki KTP domisili DKI Jakarta </li> <li>Diutamakan bagi yang membawa motor </li> <li>Diutamakan bagi yang perjalanan pergi dan kembali ke DKI Jakarta <br> <b> Tanggal Pergi: 26 April 2022</b> <br> <b>Tanggal Kembali: 7 Mei 2022 </b> </li> <li>- Sudah vaksin 3 dosis bebas test <br> - Sudah vaksin 2 dosis wajib PCR 3x24 jam atau antigen 1x24 jam <br> - Sudah vaksin 1 dosis wajib PCR 3x24 jam <br> - Calon pemudik yang belum divaksin akibat komorbid wajib PCR 3x24 jam <br> - Anak usia < 6 tahun tidak termasuk ketentuan vaksinasi dan tidak wajib test Covid19 </li> </ol> </li> <li>Pendaftaran maksimal per KK untuk 4 orang dan 1 motor</li> <li>Pendaftaran online pada website <b>www.mudikgratisdkijakarta.id</b></li> <li> Setelah mengisi form pendaftaran, telemarketing akan menghubungi calon pemudik untuk memverifikasi data calon pemudik dan memberikan kode bookin QR Code yang akan di berikan melalui email calon pemudik setelah berhasil diverifikasi atau dihubung </li> <li> Calon pemudik harus melakukan verifikasi offline pada 6 lokasi yang telah di tentukan untuk mendapatkan E-Ticket keberangkatan. Lokasi Verifikasi offline adalah sebagai berikut: <ul> <li><u>Dinas Perhubungan Provinsi DKI Jakarta</u></li> <li><u><a target="_blank" href="https://goo.gl/maps/ad7gvM79wADsWzYQ8">Suku Dinas Perhubungan Kota Jakarta Barat </a></u></li> <li><u><a target="_blank" href="https://goo.gl/maps/Bdq5aTgu7ucv4dVs5">Suku Dinas Perhubungan Kota Jakarta Pusat </a></u></li> <li><u><a target="_blank" href="https://goo.gl/maps/5iLn8rrw3CnEx7GM9">Suku Dinas Perhubungan Kota Jakarta Timur </a></u></li> <li><u><a target="_blank" href="https://goo.gl/maps/oin1Fo8DCbfUyNT46">Suku Dinas Perhubungan Kota Jakarta Selatan </a></u></li> <li><u><a target="_blank" href="https://goo.gl/maps/YJchbWjCoH6d7gCo9">Suku Dinas Perhubungan Kota Jakarta Utara </a></u></li> </ul> </li> <li> Calon pemudik wajib hadir untuk verifikasi offline pada waktu dan lokasi yang sudah tertera pada QR Code </li> </ol>' 
        ]);

        MasterContent::create([
            "slug" => "lokasi-mudik",
            "type" => "html",
            "html" => 'Mudik Gratis DKI Jakarta Tahun 2022 menyediakan 17 lokasi tujuan yang tersebar ke wilayah Jawa Tengah, Jawa Timur, Lampung dan Palembang dengan keberangkatan Bus penumpang dari JIEXPO Kemayoran Jakarta dan keberangkatan Truk motor dari Terminal Pulogadung.'
        ]);

        MasterContent::create([
            "slug" => "lokasi-keberangkatan",
            "type" => "html",
            "html" => 'Kepada Pemudik yang akan membawa Sepeda Motor dengan ini diberitahukan bahwa Waktu dan Tempat Penyerahan Sepeda Motor adalah pada: <ul> <li>Hari/Tanggal: Senin, 25 April 2022</li> <li>Tempat: Terminal Pulogadung</li> <li>Waktu: 06.00 WIB</li> </ul>'
        ]);

        MasterContent::create([
            "slug" => "syarat-ketentuan",
            "type" => "html",
            "html" => '<ul> <li>Isi tangki BBM maksimal 1 Liter</li> <li>Spion, helm dan aksesoris motor lainnya harus dilepas dan dibawa sendiri oleh peserta. </li> <li>Pada saat penyerahan motor wajib membawa STNK dan KTP asli.</li> <li>Penyelenggara tidak bertanggung jawab atas segala bentuk kerusakan yang timbul selama perjalanan mudik maupun balik.</li> <li>Asuransi hanya berlaku bila terjadi kehilangan sepeda motor.</li> </ul>'
        ]);
    }
}
