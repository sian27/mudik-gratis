<?php

namespace Database\Seeders;

use App\Models\Master\MasterSeatTruck;
use App\Models\Master\MasterTruck;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class seed_seat_truck extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $truck = MasterTruck::get();
        DB::table('m_seat_truck')->truncate();
        foreach ($truck as $j => $item) {
            foreach (range(1, 30) as $i) {
                $data = new MasterSeatTruck;
                $data->truck_id = $item->id;
                $data->name = strlen($i) > 1 ? $i : '0' . $i;
                $data->save();
                dump($j . ' => ' . $data->truck_id . ' => ' . $data->name);
            }
        }
    }
}
