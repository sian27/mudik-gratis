<?php

namespace Database\Seeders;

use App\Models\Master\MasterBus;
use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterTrip;
use App\Models\Master\MasterTruck;
use App\Models\Trans\TransBalik;
use App\Models\Trans\TransMudik;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class seed_registration extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $passenger = MasterPassenger::with('vehicle')->with('member')->whereNull('parent_id')->get();

        foreach ($passenger as $item) {
            $trip = MasterTrip::where('id', $item->trip_id)->first();

            $bus = availableBus($trip, rand(1, 4));
            if ($trip->type_trip != 'mudik-saja') {
                $truck = availableTruck($trip, rand(1, 4));
            }

            $code = '';
            $code = strtoupper(chr(rand(ord('a'), ord('z'))));
            $code .= generateCode($trip->id, 3);
            $code .= generateCode($item->id, 5);

            $mudik = new TransMudik;
            $mudik->code = $code;
            $mudik->passenger_id = $item->id;
            $mudik->bus_id = $bus['id'];
            $mudik->bus_card = generateCode($bus['card'], 1);
            $mudik->truck_id = $trip->type == 'mudik-balik-motor' ? $truck['id'] : null;
            $mudik->truck_card = $trip->type == 'mudik-balik-motor' ? generateCode($truck['card'], 1) : null;
            $mudik->trip_id = $trip->id;
            $mudik->save();

            if ($trip->type != 'mudik-saja') {
                $balik = new TransBalik;
                $balik->code = $code;
                $balik->passenger_id = $item->id;
                $balik->bus_id = $bus['id'];
                $balik->bus_card = generateCode($bus['card'], 1);
                $balik->truck_id = $trip->type == 'mudik-balik-motor' ? $truck['id'] : null;
                $balik->truck_card = $trip->type == 'mudik-balik-motor' ? generateCode($truck['card'], 1) : null;
                $balik->trip_id = $trip->id;
                $balik->save();
            }

            $i = 1;
            if ($item->member) {
                foreach ($item->member as $sub) {

                    $code = '';
                    $code = strtoupper(chr(rand(ord('a'), ord('z'))));
                    $code .= generateCode($trip->id, 3);
                    $code .= generateCode($sub->id, 5);

                    $mudik = new TransMudik;
                    $mudik->code = $code;
                    $mudik->passenger_id = $sub->id;
                    $mudik->bus_id = $bus['id'];
                    $mudik->bus_card = generateCode($bus['card'] + $i, 1);
                    $mudik->truck_id = $trip->type == 'mudik-balik-motor' ? $truck['id'] : null;
                    $mudik->truck_card = $trip->type == 'mudik-balik-motor' ? generateCode($truck['card'] + $i, 1) : null;
                    $mudik->trip_id = $trip->id;
                    $mudik->save();

                    if ($trip->type != 'mudik-saja') {
                        $balik = new TransBalik;
                        $balik->code = $code;
                        $balik->passenger_id = $sub->id;
                        $balik->bus_id = $bus['id'];
                        $balik->bus_card = generateCode($bus['card'] + $i, 1);
                        $balik->truck_id = $trip->type == 'mudik-balik-motor' ? $truck['id'] : null;
                        $balik->truck_card = $trip->type == 'mudik-balik-motor' ? generateCode($truck['card'] + $i, 1) : null;
                        $balik->trip_id = $trip->id;
                        $balik->save();
                    }

                    $i++;
                }
            }
        }
    }
}
