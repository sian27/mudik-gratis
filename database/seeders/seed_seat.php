<?php

namespace Database\Seeders;

use App\Models\Master\MasterBus;
use App\Models\Master\MasterSeatBus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class seed_seat extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bus = MasterBus::get();
        DB::table('m_seat_bus')->truncate();
        foreach ($bus as $j => $item) {
            foreach (range(1, 60) as $i) {
                $data = new MasterSeatBus;
                $data->bus_id = $item->id;
                $data->name = strlen($i) > 1 ? $i : '0' . $i;
                $data->save();
                dump($j . ' => ' . $data->bus_id . ' => ' . $data->name);
            }
        }
    }
}
