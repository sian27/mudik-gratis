<?php

namespace Database\Seeders;

use App\Models\Master\MasterPassenger;
use App\Models\Master\MasterPassengerVehicle;
use App\Models\Master\MasterTrip;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class seed_passenger extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = Faker::create();
        $nik = 3509180328500021;
        $kk = 8787879797979797;
        $hp = 6285888993321;
        $email = 1;
        foreach (range(1, 3) as $i) {
            $length = 10;
            $random = '';
            for ($i = 0; $i < $length; $i++) {
                $random .= chr(rand(ord('a'), ord('z')));
            }
            $name = strtoupper($random);
            $date = rand(1960, 2022) . '-' . rand(1, 12) . '-' . rand(1, 28);

            $trip = MasterTrip::whereHas('bus')->whereHas('truck')->inRandomOrder()->first();

            $parent = MasterPassenger::create([
                'name' => $this->faker->name(),
                'email' => $this->faker->unique()->safeEmail(),
                'nik' => (string)$nik++,
                'no_kk' => (string)$kk,
                'trip_id' => $trip->id,
                'gender' => rand(0, 1) ? 'l' : 'p',
                'phone' => (string)$hp++,
                'vaksin' => 2,
                'placebirth' => "jakarta",
                'datebirth' => date('Y-m-d', strtotime($date)),
                'address' => "Jl. Tester " . $email++,
                'file_ktp' => "default/ktp.jpg",
                'file_kk' => "default/kk.jpg",
                'type_vaksin' => 'pcr',
                'puskes_id' => rand(1, 10),
            ]);

            $haveDetail = rand(0, 1);

            if ($haveDetail) {
                $range = rand(1, 3);
                foreach (range(1, $range) as $j) {
                    $length = 10;
                    $random = '';
                    for ($i = 0; $i < $length; $i++) {
                        $random .= chr(rand(ord('a'), ord('z')));
                    }
                    $name = strtoupper($random);
                    $date = rand(1960, 2022) . '-' . rand(1, 12) . '-' . rand(1, 28);

                    MasterPassenger::create([
                        'parent_id' => $parent->id,
                        'name' => $this->faker->name(),
                        'email' => $this->faker->unique()->safeEmail(),
                        'nik' => $nik++,
                        'no_kk' => $kk,
                        'trip_id' => $trip->id,
                        'gender' => rand(0, 1) ? 'l' : 'p',
                        'phone' => $hp++,
                        'vaksin' => 2,
                        'placebirth' => "jakarta",
                        'datebirth' => date('Y-m-d', strtotime($date)),
                        'address' => "Jl. Tester " . $email++,
                        'file_ktp' => "default/ktp.jpg",
                        'file_kk' => "default/kk.jpg",
                        'type_vaksin' => 'pcr',
                        'puskes_id' => rand(1, 10),
                    ]);
                }
            }

            if ($trip->type == 'mudik-balik-motor') {
                MasterPassengerVehicle::create([
                    'passenger_id' => $parent->id,
                    'trip_id' => $trip->id,
                    'no_police' => "B " . rand(100, 999) . " PASS",
                    'merk_vehicle' => "Bebas",
                    'type_vehicle' => "Acak",
                    'color_vehicle' => "hitam",
                    'no_stnk' => rand(10000, 99999),
                    'owner_name' => $parent->name,
                    'owner_nik' => $parent->nik,
                    'owner_no_kk' => $parent->no_kk,
                    'owner_email' => $parent->email,
                    'owner_phone' => $parent->phone,
                ]);
            }
        }
    }
}
