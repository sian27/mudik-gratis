<?php

namespace Database\Seeders;

use App\Models\Master\MasterBus;
use App\Models\Master\MasterDriver;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class seed_driver extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = Faker::create();
        foreach (range(1, 4) as $i) {
            // $bus = MasterBus::inRandomOrder()->first();
            MasterDriver::create([
                'name' => $this->faker->name(),
                "image" => "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
                'type' => 'bus',
                // 'bus_id' => $bus->id            
            ]);
        }

        foreach (range(1, 4) as $i) {
            // $truck = MasterBus::inRandomOrder()->first();
            MasterDriver::create([
                'name' => $this->faker->name(),
                "image" => "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
                'type' => 'truck',
                // 'truck_id' => $truck->id            
            ]);
        }
    }
}
