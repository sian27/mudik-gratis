<?php

namespace Database\Seeders;

use App\Models\Master\MasterBus;
use App\Models\Master\MasterDriver;
use App\Models\Master\MasterTrip;
use App\Models\Master\MasterTruck;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_bus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $driver_bus = MasterDriver::where('type', 'bus')->get()->toArray();
        $driver_truck = MasterDriver::where('type', 'truck')->get()->toArray();
        foreach (range(0, 3) as $i) {
            $trip = MasterTrip::with('city')->inRandomOrder()->first();
            $code = rand(1, 4);
            MasterBus::create([
                "name" => $trip->city->name . ' - ' . (strlen($code) > 1 ? $code : '0' . $code),
                'trip_id' => $trip->id,
                'driver_id' => $driver_bus[$i]['id'],
                'code' => strlen($code) > 1 ? $code : '0' . $code,
                'no_police' => "B " . rand(100, 999) . " BUS",
                'quota' => rand(20, 50),
                'date_at' => date('Y-m-d', strtotime('2022-4-' . rand(1, 24))),
                'time_at' => date('H:i', strtotime('06:00')),
                'place_at' => 'Di tempat..'
            ]);
            MasterTruck::create([
                "name" => $trip->city->name . ' - ' . (strlen($code) > 1 ? $code : '0' . $code),
                'trip_id' => $trip->id,
                'driver_id' => $driver_truck[$i]['id'],
                'code' => strlen($code) > 1 ? $code : '0' . $code,
                'no_police' => "B " . rand(100, 999) . " TRUCK",
                'quota' => rand(20, 50),
                'date_at' => date('Y-m-d', strtotime('2022-4-' . rand(1, 24))),
                'time_at' => date('H:i', strtotime('06:00')),
                'place_at' => 'Di tempat..'
            ]);
        }
    }
}
