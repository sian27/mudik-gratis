<?php

namespace Database\Seeders;

use App\Models\Master\MasterCity;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_city extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = ['Solo', 'Semarang', 'Malang', 'Jogjakarta'];
        $terminal = ['Tirtonadi', 'Mangkang', 'Arjosari', 'Giwangan'];
        foreach (range(0, 3) as $i) {
            $no = $i + 4;
            $image = 'default/city/Image ' . $no . '.png';
            MasterCity::create([
                'name' => $name[$i],
                'image' => $image,
                'terminal_name' => $terminal[$i]
            ]);
        }
    }
}
