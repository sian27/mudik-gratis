<?php

namespace Database\Seeders;

use App\Models\Master\MasterSupport;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_support extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSupport::create([
            'category' => 'Call Center',
            'name' => 'Support 1',
            'phone' => '081296696408'
        ]);
    }
}
