<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // must be first
            seed_permission_admin::class,
            seed_content::class,

            // middle
            seed_admin::class,
            seed_lo::class,
            seed_city::class,
            seed_trip::class,
            seed_driver::class,
            seed_bus::class,
            seed_truck::class,
            seed_passenger::class,
            seed_registration::class,

            seed_puskes::class,
            seed_dinkes::class,
            seed_support::class,

            // must be end
            clearLog::class,
        ]);
    }
}
