<?php

namespace Database\Seeders;

use App\Models\Master\MasterCity;
use App\Models\Master\MasterRute;
use App\Models\Master\MasterTrip;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_trip extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (range(1, 10) as $i) {
            $city = MasterCity::inRandomOrder()->first();
            $list = ['mudik-balik-motor', 'mudik-balik', 'mudik-saja'];
            $trip = MasterTrip::create([
                "city_id" => $city->id,
                "type" => $list[rand(0, 2)]
            ]);
            foreach (range(1, 10) as $i) {
                MasterRute::create([
                    "name" => "KM " . rand(1 * $i, 10 * $i), 'order' => $i, "trip_id" => $trip->id
                ]);
            }
        }
    }
}
