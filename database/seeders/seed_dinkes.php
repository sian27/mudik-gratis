<?php

namespace Database\Seeders;

use App\Models\Master\MasterDinkes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_dinkes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterDinkes::create([
            'name'      => 'Dinkes JakPus',
            'email'     => 'dinkes.jakpus@gmail.com',
            'password'  => bcrypt('password'),
            'territory' => 'jakarta pusat'
        ]);

        MasterDinkes::create([
            'name'      => 'Dinkes JakUt',
            'email'     => 'dinkes.jakut@gmail.com',
            'password'  => bcrypt('password'),
            'territory' => 'jakarta utara'
        ]);

        MasterDinkes::create([
            'name'      => 'Dinkes JakBar',
            'email'     => 'dinkes.jakbar@gmail.com',
            'password'  => bcrypt('password'),
            'territory' => 'jakarta barat'
        ]);

        MasterDinkes::create([
            'name'      => 'Dinkes JakTim',
            'email'     => 'dinkes.jaktim@gmail.com',
            'password'  => bcrypt('password'),
            'territory' => 'jakarta timur'
        ]);

        MasterDinkes::create([
            'name'      => 'Dinkes JakSel',
            'email'     => 'dinkes.jaksel@gmail.com',
            'password'  => bcrypt('password'),
            'territory' => 'jakarta selatan'
        ]);
    }
}
