<?php

namespace Database\Seeders;

use App\Models\Master\MasterAdmin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class seed_admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = MasterAdmin::updateOrCreate(['email'     => 'anaufal879@gmail.com'], [
            'name'      => 'sian',
            'email'     => 'anaufal879@gmail.com',
            'password'  => bcrypt('password'),
            "role_id" => 1,
            "image" => "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
        ]);

        $role = Role::updateOrCreate(['name' => 'Dev', 'guard_name' => 'admin']);

        $permissions = Permission::pluck('id', 'id')->all();

        $role->syncPermissions($permissions);

        $data->assignRole([$role->id]);
    }
}
