<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class seed_permission_admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            ["name" => "role", "fn" => ["list", "create", "edit", "delete"]],
        ];

        $tableNames = config('permission.table_names');

        if (empty($tableNames)) {
            throw new \Exception('Error: config/permission.php not loaded. Run [php artisan config:clear] and try again.');
        }

        DB::table($tableNames['permissions'])->truncate();

        foreach ($permission as $item) {
            if ($item['name'] == "transaction") {
                foreach ($item['fn'] as $sub) {
                    Permission::create(['name' => 'transaction.' . $sub, 'guard_name' => 'admin']);
                }
            } else {
                foreach ($item['fn'] as $sub) {
                    Permission::create(['name' => $item['name'] . '-' . $sub, 'guard_name' => 'admin']);
                }
            }
        }
    }
}
