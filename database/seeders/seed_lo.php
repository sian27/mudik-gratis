<?php

namespace Database\Seeders;

use App\Models\Master\MasterLO;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class seed_lo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterLO::updateOrCreate(['email'     => 'hendrijs44@gmail.com'], [
            'name'      => 'GDev',
            'email'     => 'hendrijs44@gmail.com',
            'password'  => bcrypt('password'),
            'type' => 'bus',
            'bus_id' => 1,
            'phone' => "0895397938533",
            "image" => "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
        ]);

        MasterLO::updateOrCreate(['email'     => 'ardi.bbm28@gmail.com'], [
            'name'      => 'Demo',
            'email'     => 'ardi.bbm28@gmail.com',
            'password'  => bcrypt('password'),
            'type' => 'bus',
            'bus_id' => 1,
            'phone' => "0895397938533",
            "image" => "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
        ]);
    }
}
