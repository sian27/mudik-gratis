<?php

// Admin
use App\Http\Controllers\Api\Admin\AuthController as AdminAuthController;
use App\Http\Controllers\Api\Admin\CheckpointController as AdminCheckpointController;
use App\Http\Controllers\Api\Admin\LiveMapController as AdminLiveMapController;
use App\Http\Controllers\Api\Admin\PassengerController as AdminPassengerController;
use App\Http\Controllers\Api\Admin\TripController as AdminTripController;

// Chat Bot
use App\Http\Controllers\Api\ChatBot\RegistrationController as ChatBotRegistrationController;
use App\Http\Controllers\Api\ChatBot\CallApiController as ChatBotCallApiController;
use App\Http\Controllers\Api\ChatBot\PuskesController as ChatBotPuskesController;
// CMS
use App\Http\Controllers\Api\CMS\AuthController as CMSAuthController;
use App\Http\Controllers\Api\CMS\BannerController as CMSBannerController;
use App\Http\Controllers\Api\CMS\BusController as CMSBusController;
use App\Http\Controllers\Api\CMS\CityController as CMSCityController;
use App\Http\Controllers\Api\CMS\ContentController as CMSContentController;
use App\Http\Controllers\Api\CMS\DriverController as CMSDriverController;
use App\Http\Controllers\Api\CMS\LOController as CMSLOController;
use App\Http\Controllers\Api\CMS\PuskesControlller as CMSPuskesControlller;
use App\Http\Controllers\Api\CMS\TripController as CMSTripController;
use App\Http\Controllers\Api\CMS\TruckController as CMSTruckController;

// Dinkes
use App\Http\Controllers\Api\Dinkes\AuthController as DinkesAuthController;
use App\Http\Controllers\Api\Dinkes\ManifestController as DinkesManifestController;

// Frontend
use App\Http\Controllers\Api\Frontend\BusController as FrontendBusController;
use App\Http\Controllers\Api\Frontend\CityController as FrontendCityController;
use App\Http\Controllers\Api\Frontend\ContentController as FrontendContentController;
use App\Http\Controllers\Api\Frontend\ExternalApiController as FrontendExternalApiController;
use App\Http\Controllers\Api\Frontend\PassengerController as FrontendPassengerController;
use App\Http\Controllers\Api\Frontend\RegistrationController as FrontendRegistrationController;
use App\Http\Controllers\Api\Frontend\TripController as FrontendTripController;
use App\Http\Controllers\Api\Frontend\TruckController as FrontendTruckController;
use App\Http\Controllers\Api\Khusus\MigrationController as KhususMigrationController;

// Khusus
use App\Http\Controllers\Api\Khusus\PassengerController as KhususPassengerController;
use App\Http\Controllers\Api\Khusus\RegistrationController as KhususRegistrationController;
use App\Http\Controllers\Api\Khusus\ReshuffleController as KhususReshuffleController;

// LO
use App\Http\Controllers\Api\LO\AttendanceController as LOAttendanceController;
use App\Http\Controllers\Api\LO\AuthController as LOAuthController;
use App\Http\Controllers\Api\LO\CheckpointController as LOCheckpointController;
use App\Http\Controllers\Api\LO\ManifestController as LOManifestController;
use App\Http\Controllers\Api\LO\ReportController as LOReportController;
use App\Http\Controllers\Api\LO\ScanQRController as LOScanQRController;
use App\Http\Controllers\Api\LO\SupportController as LOSupportController;
use App\Http\Controllers\Api\LO\UserController as LOUserController;
use App\Http\Controllers\Api\Other\GhostController as GhostController;
use App\Http\Controllers\StatusController as AdminStatusController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// api for frontend
Route::prefix('web/v1')->name("api.web.v1.")->group(function () {
    Route::get('nik/check', [FrontendRegistrationController::class, 'check_nik'])->name("check.nik");
    Route::get('quota/check', [FrontendRegistrationController::class, 'check_quota'])->name("check.quota");
    Route::post('registration', [FrontendRegistrationController::class, 'store'])->name('registration.store');
    Route::post('registration/booking', [FrontendRegistrationController::class, 'booking']);

    // content
    Route::prefix('content')->group(function () {
        Route::get('banner', [FrontendContentController::class, 'banner']);
        Route::get('syarat-pendaftaran', [FrontendContentController::class, 'syarat_pendaftaran']);
        Route::get('lokasi-mudik', [FrontendContentController::class, 'lokasi_mudik']);
        Route::get('lokasi-keberangkatan', [FrontendContentController::class, 'lokasi_keberangkatan']);
        Route::get('syarat-ketentuan', [FrontendContentController::class, 'syarat_ketentuan']);
    });

    // city
    Route::get('city/count', [FrontendCityController::class, 'count_city']);

    // trip
    Route::get('trip/check', [FrontendTripController::class, 'check_type']);
    Route::get('trip/quota', [FrontendTripController::class, 'quota']);
    Route::get('trip/quota/{id}', [FrontendTripController::class, 'quota_detail']);
    Route::get('trip/check/{id}', [FrontendTripController::class, 'checking']);

    // bus
    Route::get('bus/count', [FrontendBusController::class, 'count']);
    Route::get('bus/count/quota', [FrontendBusController::class, 'count_quota']);
    Route::get('bus/count/quota/available', [FrontendBusController::class, 'count_quota_available']);

    // truck
    Route::get('truck/count', [FrontendTruckController::class, 'count']);
    Route::get('truck/count/quota', [FrontendTruckController::class, 'count_quota']);
    Route::get('truck/count/quota/available', [FrontendTruckController::class, 'count_quota_available']);

    // passenger 
    Route::post('passenger/{id}', [FrontendPassengerController::class, 'update']);
    Route::get('pdf-uncrypt', [FrontendPassengerController::class, 'pdfUnencrypt']);

    // external
    Route::prefix('external')->group(function () {
        Route::post('vaksin/check', [FrontendExternalApiController::class, 'check_vaksin']);
        Route::post('digidata/orc_extra', [FrontendExternalApiController::class, 'orc_extra']);
        Route::post('digidata/verify_family_card', [FrontendExternalApiController::class, 'verify_family_card']);
        Route::post('digidata/verify_identity', [FrontendExternalApiController::class, 'verify_identity']);
    });
});

// external
Route::prefix('khusus/v1')->group(function () {
    Route::post('registration', [KhususRegistrationController::class, 'store']);

    Route::post('passenger/accept', [KhususPassengerController::class, 'accept']);

    Route::post('passenger/reshuffle', [KhususReshuffleController::class, 'index']);

    Route::post('passenger/migration', [KhususMigrationController::class, 'index']);
});

Route::prefix("admin/v1")->group(function () {
    Route::post("login", [AdminAuthController::class, 'login']);
    Route::get('trip', [AdminTripController::class, 'index']);
    Route::group(['middleware' => 'admin'], function () {
        Route::get("profile", [AdminAuthController::class, 'profile']);

        // 
        Route::prefix('passenger')->group(function () {
            Route::get('count', [AdminPassengerController::class, 'count']);
            Route::get('manifest', [AdminPassengerController::class, 'manifest']);
            Route::post('accept', [AdminPassengerController::class, 'accept']);
            Route::post('reject', [AdminPassengerController::class, 'reject']);
            Route::post('check', [AdminPassengerController::class, 'check']);
        });
        Route::prefix('trip')->group(function () {
            Route::get('count', [AdminTripController::class, 'count']);
            Route::get('summary', [AdminTripController::class, 'summary']);
            Route::get('statistic', [AdminTripController::class, 'statistic']);
        });

        Route::get('checkpoint', [AdminCheckpointController::class, 'index']);

        Route::get('livemap', [AdminLiveMapController::class, 'index']);

        Route::get('status/vehicle', [AdminStatusController::class, 'index']);
    });
});


Route::prefix("cms/v1")->group(function () {
    Route::post("login", [CMSAuthController::class, 'login']);
    Route::group(['middleware' => 'admin'], function () {
        Route::get("profile", [CMSAuthController::class, 'profile']);

        Route::resource('bus', CMSBusController::class);
        Route::resource('truck', CMSTruckController::class);
        Route::resource('city', CMSCityController::class);
        Route::resource('trip', CMSTripController::class);
        Route::resource('driver', CMSDriverController::class);
        Route::resource('lo', CMSLOController::class);
        Route::resource('banner', CMSBannerController::class);
        Route::resource('puskes', CMSPuskesControlller::class);
        Route::resource('content', CMSContentController::class)->only('index', 'update');
    });
});

Route::prefix("lo/v1")->group(function () {
    Route::post("login", [LOAuthController::class, 'login']);
    Route::group(['middleware' => 'lo'], function () {
        Route::get("profile", [LOAuthController::class, 'profile']);

        // Attendance
        Route::prefix('attendance')->group(function () {
            Route::get('', [LOAttendanceController::class, 'index'])->name('attendance');
            Route::get('check', [LOAttendanceController::class, 'check']);
            Route::post('check/in', [LOAttendanceController::class, 'check_in']);
            Route::put('check/out', [LOAttendanceController::class, 'check_out']);
            Route::get('detail/{id}', [LOAttendanceController::class, 'show']);
        });

        Route::resource('checkpoint', LOCheckpointController::class)->only(['index', 'store']);

        Route::resource('manifest', LOManifestController::class)->only(['index', 'show']);

        Route::post('user', [LOUserController::class, 'update']);

        Route::resource('report', LOReportController::class);

        Route::get('support', [LOSupportController::class, 'index']);

        // scanQR
        Route::prefix('scan/qr')->group(function () {
            Route::post('', [LOScanQRController::class, 'get']);
            // Route::post('ticket', [LOScanQRController::class , 'ticket']);
        });
    });
});

Route::prefix('chatbot/v1')->group(function () {
    Route::get('nik/check', [FrontendRegistrationController::class, 'check_nik'])->name("check.nik");
    Route::get('quota/check', [FrontendRegistrationController::class, 'check_quota'])->name("check.quota");
    Route::post('api-external/record', [ChatBotCallApiController::class, 'store']);

    Route::prefix('registration')->group(function () {
        Route::post('booking', [ChatBotRegistrationController::class, 'booking']);
        Route::post('cancel', [ChatBotRegistrationController::class, 'cancel']);
        Route::post('store', [ChatBotRegistrationController::class, 'store']);
    });

    Route::get('puskes', [ChatBotPuskesController::class, 'index']);
});

Route::prefix("dinkes/v1")->group(function () {
    Route::post("login", [DinkesAuthController::class, 'login']);
    Route::group(['middleware' => 'dinkes'], function () {
        Route::get("profile", [DinkesAuthController::class, 'profile']);

        Route::resource('manifest', DinkesManifestController::class);
    });
});


Route::prefix("other/v1")->group(function () {
    Route::get('ghost/check', [GhostController::class, 'check']);
});
