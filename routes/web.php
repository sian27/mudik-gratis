<?php

// Frontend

use App\Http\Controllers\Api\Frontend\PassengerController as FrontendPassengerController;
use App\Http\Controllers\Frontend\ScanQRController as FrontendScanQRController;
use App\Http\Controllers\Frontend\HomeController as FrontendHomeController;
use App\Http\Controllers\HomeController as AdminHomeController;
// CMS
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.home');
// });

// Route::get('/', [FrontendHomeController::class, 'index'])->name('home');
// Route::get('accept-get', [PassengerController::class, 'accept'])->middleware('guest');

Route::get('download/qr/{code}', [FrontendPassengerController::class, 'pdf'])->name('download.qr');

// Route::get('qr/scan', [FrontendScanQRController::class, 'index']);
// Route::post('qr/scan/detail', [FrontendScanQRController::class, 'detail'])->name('qr.scan.detail');

// group cms Admin
// Route::get('cms-admin', function () {
//     return redirect('/cms-admin/home');
// })->name('auto');
// Route::prefix('/cms-admin')->name('cms.')->group(function () {
//     Auth::routes();
//     Route::get('home', [AdminHomeController::class, 'index'])->name('dashboard');
//     Route::name("cms.")->middleware(['auth:web'])->group(function () {
//     });
// });
