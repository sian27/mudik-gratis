<!DOCTYPE HTML
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
</xml>
<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <title></title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/ticket.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka:wght@300;400;500;600&display=swap" rel="stylesheet">

    <style type="text/css">
        * {
            font-family: 'Fredoka', sans-serif;
        }

        @media only screen and (min-width: 520px) {
            .u-row {
                width: 500px !important;
            }

            .u-row .u-col {
                vertical-align: top;
            }

            .u-row .u-col-100 {
                width: 500px !important;
            }

        }

        @media (max-width: 520px) {
            .u-row-container {
                max-width: 100% !important;
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .u-row .u-col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .u-row {
                width: calc(100% - 40px) !important;
            }

            .u-col {
                width: 100% !important;
            }

            .u-col>div {
                margin: 0 auto;
            }
        }

        body {
            margin: 0;
            padding: 0;
        }

        table,
        tr,
        td {
            vertical-align: top;
            border-collapse: collapse;
        }

        p {
            margin: 0;
        }

        .ie-container table,
        .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors='true'] {
            color: inherit !important;
            text-decoration: none !important;
        }

        table,
        td {
            /* color: #000000; */
        }

        a {
            /* color: #0000ee; */
            text-decoration: underline;
        }

    </style>

</head>

<body class="clean-body u_body text-cyan bg-white"
    style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;color: #000000">
    <!--[if IE]><div class="ie-container"><![endif]-->
    <!--[if mso]><div class="mso-container"><![endif]-->
    <table
        style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
        cellpadding="0" cellspacing="0">
        <tbody>
            <tr style="vertical-align: top">
                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #e7e7e7;"><![endif]-->


                    <div class="u-row-container" style="padding: 0px;background-color: transparent">
                        <div class="u-row"
                            style="Margin: 0 auto;min-width: 320px;max-width: 500px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:500px;"><tr style="background-color: transparent;"><![endif]-->

                                <!--[if (mso)|(IE)]><td align="center" width="500" style="width: 500px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                                <div class="u-col u-col-100"
                                    style="max-width: 320px;min-width: 500px;display: table-cell;vertical-align: top;">
                                    <div style="width: 100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div class="container py-5"
                                            style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                                            <!--<![endif]-->

                                            <table
                                                style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #e7e7e7;width:100%"
                                                cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr style="vertical-align: top">
                                                        <td
                                                            style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                            <label id="row-ticket" style="background-color: #fff">
                                                                <span class="top-dots">
                                                                    <span class="section dots">
                                                                        <span></span>
                                                                        <span></span>
                                                                        @php
                                                                            $ii = 32;
                                                                        @endphp
                                                                        @foreach (range(3, 17) as $i)
                                                                            <span
                                                                                style="left: {{ $ii }}px"></span>
                                                                            @php
                                                                                $ii += 30;
                                                                            @endphp
                                                                        @endforeach
                                                                    </span>
                                                                </span>
                                                                <span class="duration mt-n15">
                                                                    {{-- QR CODE --}}
                                                                    <table style="" role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;"
                                                                                    align="left">

                                                                                    <table width="100%" cellpadding="0"
                                                                                        cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td style="padding-right: 0px;padding-left: 0px;"
                                                                                                align="center">

                                                                                                <img src="{{ makeQrcode($data->id, 'booking', 'vehicle') }}"
                                                                                                    style="height: 300px;border-radius: 5%;">

                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </span>
                                                                <span class="section dots">
                                                                    <span></span>
                                                                    <span></span>
                                                                    @php
                                                                        $ii = 32;
                                                                    @endphp
                                                                    @foreach (range(3, 17) as $i)
                                                                        <span
                                                                            style="left: {{ $ii }}px"></span>
                                                                        @php
                                                                            $ii += 30;
                                                                        @endphp
                                                                    @endforeach
                                                                </span>
                                                                <span class="section pt-20">
                                                                    {{-- SEPARATOR --}}
                                                                    <table style="" role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;"
                                                                                    align="left">

                                                                                    <h1 class="text-cyan"
                                                                                        style="margin: 0px; line-height: 140%; text-align: center; word-wrap: break-word; font-weight: 500; font-size: 22px;">
                                                                                        QR Booking Motor
                                                                                    </h1>

                                                                                    <p class="fs-3 fw-bold text-center"
                                                                                        style="line-height: 140%; text-align: center; word-wrap: break-word;">
                                                                                        <span
                                                                                            style="line-height: 16.8px;">No
                                                                                            :
                                                                                            #{{ $data->mudik->code }}</span>
                                                                                    </p>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                    <table role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;"
                                                                                    align="left">

                                                                                    <div align="center"
                                                                                        style="border-radius: 25px; background-color: #007474"
                                                                                        class="px-7 mx-7">
                                                                                        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="" align="center"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:45px; v-text-anchor:middle; width:120px;" arcsize="9%" stroke="f" fillcolor="#3AAEE0"><w:anchorlock/><center style="color:#FFFFFF;"><![endif]-->
                                                                                        <table
                                                                                            class="table table-borderless fs-normal text-white"
                                                                                            style="line-height: 1.2;">
                                                                                            <tr>
                                                                                                <td width="35%">Nama
                                                                                                </td>
                                                                                                <td width="2%">:
                                                                                                </td>
                                                                                                <td> {{ $data->name }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>NIK</td>
                                                                                                <td>:</td>
                                                                                                <td> {{ $data->nik }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Tujuan</td>
                                                                                                <td>:</td>
                                                                                                <td> {{ $data->trip->city->name }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Tanggal
                                                                                                    Verifikasi</td>
                                                                                                <td>:</td>
                                                                                                <td>
                                                                                                    23-24 April 2022
                                                                                                    {{-- {{ date('Y-m-d', strtotime($data->verify_date)) }} --}}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Waktu Verifikasi
                                                                                                </td>
                                                                                                <td>:</td>
                                                                                                <td>
                                                                                                    08.00 - 20.00 WIB
                                                                                                    {{-- {{ date('H:i', strtotime($data->verify_time_start)) . ' - ' . date('H:i', strtotime($data->verify_time_end)) }} --}}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Tempat
                                                                                                    Verifikasi</td>
                                                                                                <td>:</td>
                                                                                                <td>
                                                                                                    <ol>
                                                                                                        <li>Dinas
                                                                                                            Perhubungan
                                                                                                            Provinsi DKI
                                                                                                            Jakarta</li>
                                                                                                        <li>Suku Dinas
                                                                                                            Perhubungan
                                                                                                            Kota Jakarta
                                                                                                            Barat</li>
                                                                                                        <li>Suku Dinas
                                                                                                            Perhubungan
                                                                                                            Kota Jakarta
                                                                                                            Pusat</li>
                                                                                                        <li>Suku Dinas
                                                                                                            Perhubungan
                                                                                                            Kota Jakarta
                                                                                                            Timur</li>
                                                                                                        <li>Suku Dinas
                                                                                                            Perhubungan
                                                                                                            Kota Jakarta
                                                                                                            Selatan</li>
                                                                                                        <li>Suku Dinas
                                                                                                            Perhubungan
                                                                                                            Kota Jakarta
                                                                                                            Utara</li>
                                                                                                    </ol>
                                                                                                    {{-- {{ $data->verify_place }} --}}
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if mso]></center></v:roundrect></td></tr></table><![endif]-->
                                                                                    </div>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>

                                                                    <table style="" role="presentation" cellpadding="0"
                                                                        cellspacing="0" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;"
                                                                                    align="left">

                                                                                    <div style="line-height: 140%; text-align: center; word-wrap: break-word;"
                                                                                        class="text-gray-600 fs-3">
                                                                                        <p style="line-height: 140%;">
                                                                                            <span
                                                                                                style="line-height: 21.8px;">Mohon
                                                                                                datang sesuai waktu dan
                                                                                            </span>
                                                                                        </p>
                                                                                        <p style="line-height: 140%;">
                                                                                            <span
                                                                                                style="line-height: 21.8px;">tempat
                                                                                                yang tertera dengan
                                                                                                membawa dokumen KTP, KK,
                                                                                            </span>
                                                                                        </p>
                                                                                        <p style="line-height: 140%;">
                                                                                            <span
                                                                                                style="line-height: 21.8px;">Sertifikat
                                                                                                Vaksin dan STNK jika
                                                                                                membawa motor.</span>
                                                                                        </p>
                                                                                    </div>

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </span>
                                                                <span class="bottom-dots">
                                                                    <span class="section dots">
                                                                        <span></span>
                                                                        <span></span>
                                                                        @php
                                                                            $ii = 32;
                                                                        @endphp
                                                                        @foreach (range(3, 17) as $i)
                                                                            <span
                                                                                style="left: {{ $ii }}px"></span>
                                                                            @php
                                                                                $ii += 30;
                                                                            @endphp
                                                                        @endforeach
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table role="presentation" cellpadding="0" cellspacing="0" width="100%"
                                                border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:arial,helvetica,sans-serif;"
                                                            align="left">

                                                            <div align="center">
                                                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;font-family:arial,helvetica,sans-serif;"><tr><td style="font-family:arial,helvetica,sans-serif;" align="center"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:45px; v-text-anchor:middle; width:120px;" arcsize="9%" stroke="f" fillcolor="#3AAEE0"><w:anchorlock/><center style="color:#FFFFFF;font-family:arial,helvetica,sans-serif;"><![endif]-->
                                                                <a href="{{ 'https://backend.mudikgratisdkijakarta.id/download/qr/' . encrypt($data->id . ',booking,vehicle') }}"
                                                                    target="_blank" class="btn-orange btn rounded-pill"
                                                                    style="box-sizing: border-box;display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; border-radius: 4px;-webkit-border-radius: 4px; -moz-border-radius: 4px; width:auto; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none; background-color: coral">
                                                                    <span
                                                                        style="display:block;padding:10px 20px;line-height:120%;"><span
                                                                            class="fw-normal"
                                                                            style="font-size: 14px; line-height: 16.8px;">Lihat
                                                                            Tiket Booking Motor</span></span>
                                                                </a>
                                                                <!--[if mso]></center></v:roundrect></td></tr></table><![endif]-->
                                                            </div>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>


                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td><![endif]-->
                                <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>


                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
</body>

</html>
