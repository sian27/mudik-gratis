<script>
    // function changeLabe() {
    $("#{{ $var }}").on("change", function() {
        document.getElementById("{{ $var }}-label").style.display = "inline-block";
        if (!document.getElementById("{{ $var }}").files[0]) {
            document.getElementById("{{ $var }}-label").value = null;
        } else {
            document.getElementById("{{ $var }}-label").value = document.getElementById(
                "{{ $var }}").files[0].name;
        }
    })

    // function chooseImage() {
    $("#btn-{{ $var }}").on("click", function() {
        document.getElementById("{{ $var }}").click()
    })
</script>
