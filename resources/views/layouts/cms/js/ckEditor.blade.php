<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>

<script>    
    ClassicEditor
        .create(document.querySelector('{{ $selector }}'), {
            simpleUpload: {
                // The URL that the images are uploaded to.
                uploadUrl: '{{ route('cms.home.ckeditor.image') }}',

                // Enable the XMLHttpRequest.withCredentials property.
                withCredentials: true,

                // Headers sent along with the XMLHttpRequest to the upload server.
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            }
        })
        .catch(error => {
            console.error(error);
        });
</script>
