<script src="{{ asset('js/autoNumeric.min.js') }}"></script>
<script>
    new AutoNumeric('{{$exec}}', {
        decimalCharacter: ',',
        digitGroupSeparator: '.',
        maximumValue: {{ $max }},
        modifyValueOnWheel: false,
        unformatOnSubmit: true,
        decimalPlaces: {{ $decimal }}
    });
</script>
