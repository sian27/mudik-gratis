{{-- <script src="{{asset('js/tinymce.min.js')}}"></script> --}}
<script src="https://cdn.tiny.cloud/1/k04vik0qtxkiuapub8pgj3kx44falbbsdo9ckvrr1r6f6uk4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: '#mytiny',
        plugins: 'print preview paste searchreplace autolink directionality visualblocks visualchars code fullscreen link media pagebreak template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools colorpicker textpattern help',
        toolbar: 'formatselect | fontsizeselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat |',
        menubar: false,
    });
</script>
