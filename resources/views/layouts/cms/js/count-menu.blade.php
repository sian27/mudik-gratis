<script>
    var url = "{{ $apiUrl }}";
    $.get(url, {}, function(response) {
        var res = response;
        $("#{{ $idMenu }}").html(response.count > 0 ? response.count : null)
    }).fail(function() {});

    setInterval(function() {
        const url = "{{ $apiUrl }}";
        $.get(url, {}, function(response) {
            var res = response;
            $("#{{ $idMenu }}").html(response.count > 0 ? response.count : null)
        }).fail(function() {});
    }, 60000);
</script>
