@php
if (!isset($var)) {
    $var = 'image';
}
@endphp
<script>
    var url = `{{ asset('upload/empty.jpg') }}`

    document.getElementById("{{ $var }}-preview").src = url;

    // function previewImage() {
    $("#{{ $var }}").on("change", function() {
        document.getElementById("{{ $var }}-preview").style.display = "inline-block";
        if (!document.getElementById("{{ $var }}").files[0]) {
            document.getElementById("{{ $var }}-preview").src = url;
        } else {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("{{ $var }}").files[0]);
            oFReader.onload = function(oFREvent) {
                document.getElementById("{{ $var }}-preview").src = oFREvent.target.result;
            };
        }
    })

    // function chooseImage() {
    $("#{{ $var }}-preview").on("click", function() {
        document.getElementById("{{ $var }}").click()
    })
</script>
