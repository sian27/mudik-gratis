@php
$guard = 'admin';

$menu_path = public_path('default/menu/admin.json');
$menu = json_decode(file_get_contents($menu_path), true);
@endphp

<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
    id="#kt_aside_menu" data-kt-menu="true">
    @foreach ($menu as $item)
        @if (isset($item['heading']))
            @if (check_permission($pages['permission'], $guard))
                <div class="menu-item">
                    <div class="menu-content pt-8 pb-2">
                        <span class="menu-section text-muted text-uppercase fs-8 ls-1">
                            {{ $item['heading'] }}
                        </span>
                    </div>
                </div>
            @endif
        @endif
        @foreach ($item['pages'] as $pages)
            @if (isset($pages['heading']))
                @if (check_permission($pages['permission'], $guard))
                    <div class="menu-item">
                        <a class="menu-link {{ str_contains(request()->url(), $pages['target']) ? 'active' : '' }}"
                            href="{{ route($pages['route']) }}">
                            <span class="menu-icon">
                                <i class="{{$pages['icon']}} fs-3"></i>
                            </span>
                            <span class="menu-title">{{ $pages['heading'] }}</span>
                        </a>
                    </div>
                @endif
            @endif
            @if (isset($pages['sectionTitle']))
                @if (check_permission($pages['permission'], $guard))
                    <div data-kt-menu-trigger="click"
                        class="menu-item menu-accordion {{ str_contains(request()->url(), $pages['target']) ? 'show' : '' }}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <i class="{{ $pages['icon'] }} fs-3"></i>
                            </span>
                            <span class="menu-title">{{ $pages['sectionTitle'] }}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion menu-active-bg">
                            @foreach ($pages['sub'] as $sub)
                                @if (check_permission($sub['permission'], $guard))
                                    <div class="menu-item">
                                        <a class="menu-link {{ str_contains(request()->url(), $sub['target']) ? 'active' : '' }}"
                                            href=" {{ route($sub['route']) }} ">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">{{ $sub['heading'] }}</span>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    @endforeach
</div>
