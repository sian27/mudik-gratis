<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
    data-kt-menu="true">
    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <div class="menu-content d-flex align-items-center px-3">
            <!--begin::Avatar-->
            <div class="symbol symbol-50px me-5">
                <img alt="Logo" src="{{ asset($auth->image) }}" />
            </div>
            <!--end::Avatar-->
            <!--begin::Username-->
            <div class="d-flex flex-column">
                <div class="fw-bolder d-flex align-items-center fs-5">{{ ucwords($auth->name) }}
                    <span
                        class="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">{{ ucwords($auth->role->name) }}</span>
                </div>
                <a href="#" class="fw-bold text-muted cursor-default fs-7">{{ $auth->email }}</a>
            </div>
            <!--end::Username-->
        </div>
    </div>
    <!--end::Menu item-->
    <!--begin::Menu separator-->
    {{-- <div class="separator my-2"></div> --}}
    <!--end::Menu separator-->
    <!--begin::Menu item-->
    {{-- <div class="menu-item px-5">
        <a href="#" class="menu-link px-5">My Profile</a>
    </div> --}}
    <!--end::Menu item-->
    <!--begin::Menu separator-->
    <div class="separator my-2"></div>
    <!--end::Menu separator-->
    <!--begin::Menu item-->
    {{-- <div class="menu-item px-5 my-1">
        <a href="#" class="menu-link px-5">Account Settings</a>
    </div> --}}
    <!--end::Menu item-->
    <!--begin::Menu item-->
    <div class="menu-item px-5">
        <form action="{{ route('logout') }}" method="POST" id="logout-form" class="d-none">
            @csrf
        </form>
        <a href="#" id="submit-logout" class="menu-link px-5">Sign Out</a>
    </div>
    <!--end::Menu item-->
</div>
