<!--begin::Header-->
<div id="kt_header" class="header align-items-stretch" data-kt-sticky="true" data-kt-sticky-name="header"
    data-kt-sticky-offset="{default: '200px', lg: '300px'}">
    <!--begin::Container-->
    <div class="container-xxl d-flex align-items-center">
        <!--begin::Heaeder menu toggle-->
        <div class="d-flex topbar align-items-center d-lg-none ms-n2 me-3" title="Show aside menu">
            <div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px"
                id="kt_header_menu_mobile_toggle">
                <i class="fas fa-bars fs-3 text-cyan text-hover-light-orange"></i>
            </div>
        </div>
        <!--end::Heaeder menu toggle-->
        <!--begin::Header Logo-->
        <div class="header-logo me-5 me-md-10 flex-grow-1 flex-lg-grow-0">
            {{-- <a href="#">
                <img alt="Logo" src="{{ asset('images/logo.png') }}" class="logo-default h-25px" />
                <img alt="Logo" src="{{ asset('images/logo2.png') }}" class="logo-sticky h-25px" />
            </a> --}}
            <span class="text-cyan fs-3 fw-bolder">Mudik Gratis DKI Jakarta 2022</span>
        </div>
        <!--end::Header Logo-->
        <!--begin::Wrapper-->
        <div class="d-flex align-items-center justify-content-between flex-lg-grow-1">
            @include('layouts.frontend.navbar')            
            {{-- <div class="wrapper-menu flex-lg-grow-1 d-flex align-items-center justify-content-center">
                <div class="menu d-flex align-items-center justify-content-between w-75">
                    <a href="#" class="text-dark fs-5 fw-bolder">Home</a>
                    <a href="#" class="text-dark fs-5 fw-bolder">Tentang Kami</a>
                    <a href="#" class="text-dark fs-5 fw-bolder">Kontak Kami</a>
                </div>
            </div> --}}
            {{-- @include('layouts.frontend.topbar') --}}
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Container-->
</div>
<!--end::Header-->
