  <!--begin::Topbar-->
  <div class="d-flex align-items-stretch flex-shrink-0 me-15">
      <!--begin::Toolbar wrapper-->
      <div class="topbar d-flex align-items-stretch flex-shrink-0">
          <!--begin::Login-->
          <div class="d-flex align-items-center ms-1 ms-lg-3">
              <a href="#daftar" class="btn me-5 text-white text-center bg-cyan rounded-pill">
                  Daftar
                  <i class="fas fa-edit fs-3 text-white"></i>                  
              </a>
          </div>
          <!--end::Login -->
          <!--begin::Aside mobile toggle-->
          <!--end::Aside mobile toggle-->
      </div>
      <!--end::Toolbar wrapper-->
  </div>
  <!--end::Topbar-->
