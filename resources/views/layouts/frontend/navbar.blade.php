<!--begin::Navbar-->
<div class="d-flex align-items-top w-100" id="kt_header_nav">
    <!--begin::Menu wrapper-->
    <div class="header-menu align-items-stretch w-100" data-kt-drawer="true" data-kt-drawer-name="header-menu"
        data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
        data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start"
        data-kt-drawer-toggle="#kt_header_menu_mobile_toggle" data-kt-swapper="true" data-kt-swapper-mode="prepend"
        data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
        <!--begin::Menu-->
        <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 d-flex align-items-top justify-content-lg-between w-100"
            id="#kt_header_menu" data-kt-menu="true">
            {{-- <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start"
                class="menu-item menu-lg-down-accordion me-lg-1 {{ str_contains(request()->url(), '/room') ? 'here' : '' }}">
                <a class="menu-link py-3" href="#">
                    <span class="menu-title">Room</span>
                    <span class="menu-arrow d-lg-none"></span>
                </a>
                <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown menu-rounded-0 py-lg-4 w-lg-225px">
                    <div class="menu-item ">
                        <a class="menu-link py-3" href="{{ route('frontend.meeting_room.index') }}" target="_self">
                            <span class="menu-icon">
                                <i class="fas fa-door-open fs-3"></i>
                            </span>
                            <span class="menu-title">Meeting Room</span>
                        </a>
                    </div>
                </div>
            </div> --}}

            <div class="menu-item ">
                <a class="menu-link {{ url()->to('/') ? 'active' : '' }}" href="#" target="_self">
                    <span class="fs-5 fw-bolder menu-title text-dark text-hover-light-cyan fw-bolder">Home</span>
                </a>
            </div>

            <div class="menu-item ">
                <a class="menu-link {{ str_contains(request()->url(), '/tentang-kami') ? 'active' : '' }}" href="https://linktr.ee/mudikgratisdkijakarta" target="_blank"
                    target="_self">
                    <span class="fs-5 fw-bolder menu-title text-dark text-hover-light-cyan fw-bolder">Informasi</span>
                </a>
            </div>

            <div class="menu-item ">
                <a class="menu-link {{ str_contains(request()->url(), '/kontak-kami') ? 'active' : '' }}" href="https://linktr.ee/kontakmudikgratisdkijakarta"
                    target="_self">
                    <span class="fs-5 fw-bolder menu-title text-dark text-hover-light-cyan fw-bolder">Kontak Kami</span>
                </a>
            </div>

            <div class="menu-item d-block d-lg-none">
                <a class="menu-link" href="#daftar"
                    target="_self">
                    <span class="fs-5 fw-bolder menu-title text-dark text-hover-light-cyan fw-bolder">Daftar</span>
                </a>
            </div>

            <div class="menu-item d-none d-lg-block">
                <a href="#daftar" class="btn me-5 text-white text-center bg-cyan rounded-pill">
                    Daftar
                    {{-- <i class="fas fa-edit fs-3 text-white"></i> --}}
                </a>
            </div>
        </div>
        <!--end::Menu-->
    </div>
    <!--end::Menu wrapper-->
</div>
<!--end::Navbar-->
