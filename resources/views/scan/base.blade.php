<!DOCTYPE html>
<!--
Author: Sian
Product Name: Tron Moves Apps

Template: Metronic v2.0.87
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->

<head>
    <base href="">
    <title>@yield('title')</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Mudik Gratis" />
    <meta name="keywords" content="Mudik Gratis Pemprov DKI Jakarta" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    {{-- <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title"
        content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" /> --}}

    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    <!--begin::Fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka:wght@300;400;500;600&display=swap" rel="stylesheet">

    {{-- JS must first --}}
    <script src="https://kit.fontawesome.com/4a6d449c5a.js" crossorigin="anonymous"></script>

    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{ asset('frontend/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->


    <style>
        /* Chrome, Safari, Edge, Opera */
        .hide-arrow,
        .hide-arrow:hover {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        .hide-arrow,
        .hide-arrow:hover {
            -moz-appearance: textfield;
        }

        .cursor-default {
            cursor: default;
        }

        .select2-container--bootstrap5 .select2-selection--single .select2-selection__rendered {
            color: #3c3e4f;
        }

        .select2-container.select2-container-disabled .select2-choice {
            background-color: #ddd;
            border-color: #a8a8a8;
        }

        * {
            font-family: 'Fredoka', sans-serif;
        }

    </style>

    {{-- begin:Custom CSS --}}
    @yield('customCSS')
    {{-- end:Custom CSS --}}
</head>
<!--end::Head-->
<!--begin::Body-->

<!--begin::Body-->

<body>
    <div class="container">
        <div class="d-flex align-items-center justify-content-center vh-100 w-100 flex-column">
            <div class="fs-5x text-cyan fw-bold">mudikgratisjakarta</div>
            <div class="row h-50 w-100">
                @yield('content')
            </div>
        </div>
    </div>

    <!--end::Main-->
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="{{ asset('js/jquery-3.6.0.js') }}"></script>
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="{{ asset('frontend/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('frontend/js/scripts.bundle.js') }}"></script>
    <!--end::Page Custom Javascript-->
    <!--end::Javascript-->

    @include('layouts.frontend.alert')

    {{-- begin:Custom JS --}}
    @yield('customJS')
    {{-- end:Custom JS --}}
</body>
