<div id="daftar"
    class="form-pendaftaran mt-20 py-20 mb-20 px-5 border border-3 rounded content-element shadow ff-fredoka">
    <div class="fs-3tx fw-bold text-cyan text-center mb-1">{{ ucwords($param['type']) }}
        {{ ucwords($param['category']) }} #{{ $data->mudik->code }} </div>
    <div class="fs-3tx fw-bold text-cyan text-center mb-1">Mudik Gratis Jakarta 2022</div>

    <div class="stepper stepper-links d-flex flex-column pt-15" id="form-horizontal">
        <!--begin::Nav-->
        <div class="stepper-nav mb-5">
            <!--begin::Step 1-->
            <div class="stepper-item current" data-kt-stepper-element="nav">
                <h3 class="stepper-title fw-bold">Data Colon Pemudik</h3>
            </div>
            <!--end::Step 1-->
            <!--begin::Step 2-->
            <div class="stepper-item" data-kt-stepper-element="nav">
                <h3 class="stepper-title fw-bold">Data Anggota Keluarga</h3>
            </div>
            <!--end::Step 2-->
            @if ($data->vehicle)
                <!--begin::Step 3-->
                <div class="stepper-item" id="form-label-motor" data-kt-stepper-element="nav">
                    <h3 class="stepper-title fw-bold">Data Sepeda Motor</h3>
                </div>
                <!--end::Step 3-->
            @endif
        </div>
        <!--end::Nav-->
        <!--begin::Form-->
        <form class="mx-5 w-100 pt-15 pb-10" method="POST" action="{{ route('api.web.v1.registration.store') }}"
            novalidate="novalidate" id="registration-form">
            <!--begin::Step 1-->
            <div class="current me-10" id="form-page-1" data-kt-stepper-element="content">
                <!--begin::Wrapper-->
                <div class="w-100">
                    <div class="row">

                        @csrf

                        <input type="hidden" name="quota" id="quota-trip" value="4">
                        <input type="hidden" name="base_id" id="base-id">
                        <input type="hidden" name="type_trip" id="type-trip">

                        <div class="col-lg-4">

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Kota Arus Mudik dan Arus
                                    Balik</label>
                                <select data-control="select2" id="kota-tujuan" disabled data-placeholder="Pilih Kota"
                                    name="city_id" class="form-select form-select-solid">
                                    <option value=""></option>
                                    @foreach ($trip as $item)
                                        <option {{ $data->trip_id == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">{{ ucwords($item->city->name) }} (
                                            {{ ucwords(str_replace('motor', '+ motor', str_replace('balik', '- balik', str_replace('-', ' ', $item->type)))) }}
                                            )</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Foto KTP</label>
                                <div class="col-md-5 col-lg-5">
                                    <a href="{{ $data->file_ktp }}" target="_blank" class="btn btn-cyan w-100">
                                        Lihat KTP
                                    </a>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                <input autocomplete="off" type="text" name="name" class="form-control"
                                    value="{{ $data->name }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                <input autocomplete="off" type="number" name="nik" class="form-control"
                                    value="{{ $data->nik }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Foto KK</label>
                                <div class="col-md-5 col-lg-5">
                                    <a href="{{ $data->file_kk }}" target="_blank" class="btn btn-cyan w-100">
                                        Lihat KK
                                    </a>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nomor KK</label>
                                <input autocomplete="off" type="number" name="no_kk" class="form-control"
                                    value="{{ $data->no_kk }}" disabled>
                            </div>
                        </div>

                        <div class="col-lg-4">

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Kelamin</label>
                                <select data-control="select2" data-placeholder="Pilih Jenis Kelamin" name="gender"
                                    class="form-select form-select-solid select2" disabled>
                                    <option value=""></option>
                                    @php
                                        $list = ['l', 'p'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ $data->gender == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item == 'l' ? 'laki-laki' : 'perempuan') }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Tanggal Lahir</label>
                                <input autocomplete="off" type="date" name="date_of_birth" class="form-control"
                                    value="{{ $data->datebirth }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Kota Sesuai KTP</label>
                                <input autocomplete="off" type="text" name="city_of_ktp" class="form-control"
                                    value="{{ $data->placebirth }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Alamat Sesuai KTP</label>
                                <textarea autocomplete="off" rows="3" name="address" class="form-control" disabled>{{ $data->address }}</textarea>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                <input autocomplete="off" type="text" name="email" class="form-control"
                                    value="{{ $data->email }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                <input autocomplete="off" type="number" name="phone" class="form-control"
                                    value="{{ $data->phone }}" disabled>
                            </div>

                        </div>

                        <div class="col-lg-4">

                            <input type="hidden" name="vaksin">

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Status Vaksin</label>
                                <input autocomplete="off" type="text" name="label_vaksin" disabled
                                    class="form-control"
                                    value="{{ $data->vaksin ? 'Dosis ' . $data->vaksin : 'Belum Vaksin' }}" disabled>
                            </div>

                            <div class="mb-10 fv-row {{ $data->file_booster ? '' : 'd-none' }}">
                                <label class="form-label text-cyan fs-6 fw-bold">File Booster</label>
                                <a href="{{ $data->file_booster }}" target="_blank" class="btn btn-cyan w-100">
                                    Lihat File Booster
                                </a>
                            </div>

                            <div class="mb-10 fv-row {{ $data->type_vaksin ? '' : 'd-none' }}">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Vaksin</label>
                                <select data-control="select2" data-placeholder="Pilih Jenis Vaksin" name="type_vaksin"
                                    class="form-select form-select-solid" disabled>
                                    <option value=""></option>
                                    @php
                                        $list = ['booster', 'antigen', 'pcr'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ $data->type_vaksin == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row {{ $data->type_vaksin ? '' : 'd-none' }}">
                                <label class="form-label fs-6 fw-bold text-cyan">Puskesmas</label>
                                <select data-control="select2" data-placeholder="Pilih Puskesmas" name="puskes_id"
                                    class="form-select form-select-solid" disabled>
                                    <option value=""></option>
                                    @foreach ($puskes as $item)
                                        <option {{ $data->puskes_id == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">
                                            {{ 'Puskesmas - ' . $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--begin::Step 2-->
            <div class="me-10" data-kt-stepper-element="content">
                <!--begin::Wrapper-->
                <div class="w-100">
                    <div id="content-anggota-keluarga" class="row">

                        @foreach ($data->member as $i => $item)
                            <div class="col-lg-4">

                                <!--begin::Title-->
                                <div class="pb-10 pb-lg-15">
                                    <h4 class="fw-bold d-flex align-items-center text-orange">Anggota Keluarga
                                        {{ $i + 1 }}</h4>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                    <input autocomplete="off" type="text" name="name_family_1" class="form-control"
                                        data-row="1" value="{{ $item->name }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <div class="row">
                                        <div class="col-5">
                                            <label class="form-label text-cyan fs-6 fw-bold">Tempat</label>
                                            <input autocomplete="off" type="text" name="city_family_1"
                                                class="form-control" data-row="1" value="{{ $item->placebirth }}"
                                                disabled>
                                        </div>
                                        <div class="col-7">
                                            <label class="form-label text-cyan fs-6 fw-bold">Tanggal</label>
                                            <input autocomplete="off" type="date" name="date_family_1"
                                                class="form-control" data-row="1" value="{{ $item->datebirth }}"
                                                disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                    <input autocomplete="off" type="number" name="nik_family_1" class="form-control"
                                        value="{{ $item->nik }}" data-row="1" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label fs-6 fw-bold text-cyan">Jenis Kelamin</label>
                                    <select data-control="select2" data-placeholder="Pilih Jenis Kelamin"
                                        name="gender_family_1" class="form-select form-select-solid" disabled>
                                        <option value=""></option>
                                        @php
                                            $list = ['l', 'p'];
                                        @endphp
                                        @foreach ($list as $items)
                                            <option {{ $item->gender == $items ? 'selected' : '' }}
                                                value="{{ $items }}">
                                                {{ ucwords($items == 'l' ? 'laki-laki' : 'perempuan') }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                    <input autocomplete="off" type="number" name="phone_family_1" class="form-control"
                                        data-row="1" value="{{ $item->phone }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                    <input autocomplete="off" type="email" name="email_family_1" class="form-control"
                                        data-row="1" value="{{ $item->email }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Status Vaksin</label>
                                    <input autocomplete="off" type="text" name="label_vaksin_family_1" disabled
                                        class="form-control"
                                        value="{{ $item->vaksin ? 'Dosis ' . $item->vaksin : 'Belum Vaksin' }}"
                                        disabled>
                                </div>

                                <div class="mb-10 fv-row {{ $item->file_booster ? '' : 'd-none' }}">
                                    <label class="form-label text-cyan fs-6 fw-bold">Upload Booster</label>
                                    <a href="{{ $item->file_booster }}" target="_blank" class="btn btn-cyan w-100">
                                        Lihat File Booster
                                    </a>
                                </div>

                                <div class="mb-10 fv-row {{ $item->type_vaksin ? '' : 'd-none' }}">
                                    <label class="form-label fs-6 fw-bold text-cyan">Jenis Vaksin</label>
                                    <select data-control="select2" data-placeholder="Pilih Jenis Vaksin"
                                        name="type_vaksin_family_1" data-row="1" class="form-select form-select-solid"
                                        disabled>
                                        <option value=""></option>
                                        @php
                                            $list = ['booster', 'antigen', 'pcr'];
                                        @endphp
                                        @foreach ($list as $items)
                                            <option {{ $item->type_vaksin == $items ? 'selected' : '' }}
                                                value="{{ $items }}">
                                                {{ ucwords($items) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="mb-10 fv-row {{ $item->type_vaksin ? '' : 'd-none' }}">
                                    <label class="form-label fs-6 fw-bold text-cyan">Puskesmas</label>
                                    <select data-control="select2" data-placeholder="Pilih Puskesmas" data-row="1"
                                        name="puskes_id_family_1" class="form-select form-select-solid" disabled>
                                        <option value=""></option>
                                        @foreach ($puskes as $items)
                                            <option {{ $item->puskes_id == $items->id ? 'selected' : '' }}
                                                value="{{ $items->id }}">
                                                {{ 'Puskesmas - ' . $items->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        @endforeach

                    </div>
                </div>
            </div>

            @if ($data->vehicle)
                <!--begin::Step 3-->
                <div class="me-10" data-kt-stepper-element="content">
                    <!--begin::Wrapper-->
                    <div class="w-100">
                        <div class="row">
                            <div class="col-lg-4">

                                <!--begin::Title-->
                                <div class="pb-10 pb-lg-15">
                                    <h4 class="fw-bold d-flex align-items-center text-orange">Data Motor</h4>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Plat Motor</label>
                                    <input autocomplete="off" type="text" name="no_police" class="form-control"
                                        value="{{ $data->vehicle->no_police }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Merk Motor</label>
                                    <input autocomplete="off" type="text" name="merk_vehicle" class="form-control"
                                        value="{{ $data->vehicle->merk_vehicle }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Tipe Motor</label>
                                    <input autocomplete="off" type="text" name="type_vehicle" class="form-control"
                                        value="{{ $data->vehicle->type_vehicle }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Warna Motor</label>
                                    <input autocomplete="off" type="text" name="color_vehicle" class="form-control"
                                        value="{{ $data->vehicle->color_vehicle }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">No STNK</label>
                                    <input autocomplete="off" type="number" name="no_stnk" class="form-control"
                                        value="{{ $data->vehicle->no_stnk }}" disabled>
                                </div>

                            </div>
                            <div class="col-lg-4">

                                <!--begin::Title-->
                                <div class="pb-10 pb-lg-15">
                                    <h4 class="fw-bold d-flex align-items-center text-orange">Data Pemilik</h4>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                    <input autocomplete="off" type="text" name="owner_name" class="form-control"
                                        value="{{ $data->vehicle->owner_name }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                    <input autocomplete="off" type="number" name="owner_nik" class="form-control"
                                        value="{{ $data->vehicle->owner_nik }}"
                                        data-url="{{ route('api.web.v1.check.nik') }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Nomor KK</label>
                                    <input autocomplete="off" type="number" name="owner_no_kk" class="form-control"
                                        value="{{ $data->vehicle->owner_no_kk }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                    <input autocomplete="off" type="text" name="owner_email" class="form-control"
                                        value="{{ $data->vehicle->owner_email }}" disabled>
                                </div>

                                <div class="mb-10 fv-row">
                                    <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                    <input autocomplete="off" type="text" name="owner_phone" class="form-control"
                                        value="{{ $data->vehicle->owner_phone }}" disabled>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <!--begin::Actions-->
            <div class="d-flex flex-stack pt-15 mx-10">
                <!--begin::Wrapper-->
                <div class="mr-2">
                    <button type="button" id="prev-step" class="btn btn-lg btn-light-orange me-3"
                        data-kt-stepper-action="previous">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                        <i class="fas fa-angle-double-left fs-4 text-inverse"></i>
                        <!--end::Svg Icon-->Sebelumnya
                    </button>
                </div>
                <!--end::Wrapper-->
                <!--begin::Wrapper-->
                <div>
                    <button type="button" class="btn btn-lg btn-orange me-3" data-kt-stepper-action="submit">
                        <span class="indicator-label">Daftar
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                            <i class="fas fa-save fs-4 text-white"></i>
                            <!--end::Svg Icon-->
                        </span>
                        <span class="indicator-progress">Mohon Tunggu...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                    <button type="button" id="next-step" class="btn btn-lg btn-orange"
                        data-kt-stepper-action="next">Selanjutnya
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                        <i class="fas fa-angle-double-right fs-4 text-white"></i>
                        <!--end::Svg Icon-->
                    </button>
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
</div>


@section('customJS')
    <script src="{{ asset('frontend/js/custom/scan/passenger.js') }}"></script>
@endsection
