@extends('scan.base')

@section('title')
    Hasil Scan QR
@endsection

@section('content')
    <div class="card w-100 bg-transparent">
        <div class="card-body">
            @if ($param['category'] == 'passenger')
                @include('scan.passenger')
            @else
                @include('scan.vehicle')
            @endif
        </div>
    </div>
@endsection
