<div id="daftar"
    class="form-pendaftaran mt-20 py-20 mb-20 px-5 border border-3 rounded content-element shadow ff-fredoka">
    <div class="fs-3tx fw-bold text-cyan text-center mb-1">{{ ucwords($param['type']) }}
        {{ ucwords($param['category']) }} #{{ $data->passenger->mudik->code }} </div>
    <div class="fs-3tx fw-bold text-cyan text-center mb-1">Mudik Gratis Jakarta 2022</div>

    <div class="stepper stepper-links d-flex flex-column pt-15" id="form-horizontal">
        <!--begin::Nav-->
        <div class="stepper-nav mb-5">
            <!--begin::Step 3-->
            <div class="stepper-item" id="form-label-motor" data-kt-stepper-element="nav">
                <h3 class="stepper-title fw-bold">Data Sepeda Motor</h3>
            </div>
            <!--end::Step 3-->
        </div>
        <!--end::Nav-->
        <!--begin::Form-->
        <form class="mx-5 w-100 pt-15 pb-10" method="POST" action="{{ route('api.web.v1.registration.store') }}"
            novalidate="novalidate" id="registration-form">

            <!--begin::Step 3-->
            <div class="me-10 current" data-kt-stepper-element="content">
                <!--begin::Wrapper-->
                <div class="w-100">
                    <div class="row">
                        <div class="col-lg-4">

                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Data Motor</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Plat Motor</label>
                                <input autocomplete="off" type="text" name="no_police" class="form-control"
                                    value="{{ $data->no_police }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Merk Motor</label>
                                <input autocomplete="off" type="text" name="merk_vehicle" class="form-control"
                                    value="{{ $data->merk_vehicle }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Tipe Motor</label>
                                <input autocomplete="off" type="text" name="type_vehicle" class="form-control"
                                    value="{{ $data->type_vehicle }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Warna Motor</label>
                                <input autocomplete="off" type="text" name="color_vehicle" class="form-control"
                                    value="{{ $data->color_vehicle }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No STNK</label>
                                <input autocomplete="off" type="number" name="no_stnk" class="form-control"
                                    value="{{ $data->no_stnk }}" disabled>
                            </div>

                        </div>
                        <div class="col-lg-4">

                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Data Pemilik</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                <input autocomplete="off" type="text" name="owner_name" class="form-control"
                                    value="{{ $data->owner_name }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                <input autocomplete="off" type="number" name="owner_nik" class="form-control"
                                    value="{{ $data->owner_nik }}" data-url="{{ route('api.web.v1.check.nik') }}"
                                    disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nomor KK</label>
                                <input autocomplete="off" type="number" name="owner_no_kk" class="form-control"
                                    value="{{ $data->owner_no_kk }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                <input autocomplete="off" type="text" name="owner_email" class="form-control"
                                    value="{{ $data->owner_email }}" disabled>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                <input autocomplete="off" type="text" name="owner_phone" class="form-control"
                                    value="{{ $data->owner_phone }}" disabled>
                            </div>

                        </div>
                        <div class="col-lg-4">
                              <!--begin::Title-->
                              <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Lainnya</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Kota Arus Mudik dan Arus
                                    Balik</label>
                                <select data-control="select2" id="kota-tujuan" disabled data-placeholder="Pilih Kota"
                                    name="city_id" class="form-select form-select-solid">
                                    <option value=""></option>
                                    @foreach ($trip as $item)
                                        <option {{ $data->trip_id == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">{{ ucwords($item->city->name) }} (
                                            {{ ucwords(str_replace('motor', '+ motor', str_replace('balik', '- balik', str_replace('-', ' ', $item->type)))) }}
                                            )</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--begin::Actions-->
            <div class="d-flex flex-stack pt-15 mx-10">
                <!--begin::Wrapper-->
                <div class="mr-2">
                    <button type="button" id="prev-step" class="btn btn-lg btn-light-orange me-3"
                        data-kt-stepper-action="previous">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                        <i class="fas fa-angle-double-left fs-4 text-inverse"></i>
                        <!--end::Svg Icon-->Sebelumnya
                    </button>
                </div>
                <!--end::Wrapper-->
                <!--begin::Wrapper-->
                <div>
                    <button type="button" class="btn btn-lg btn-orange me-3" data-kt-stepper-action="submit">
                        <span class="indicator-label">Daftar
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                            <i class="fas fa-save fs-4 text-white"></i>
                            <!--end::Svg Icon-->
                        </span>
                        <span class="indicator-progress">Mohon Tunggu...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                    <button type="button" id="next-step" class="btn btn-lg btn-orange d-none"
                        data-kt-stepper-action="next">Selanjutnya
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                        <i class="fas fa-angle-double-right fs-4 text-white"></i>
                        <!--end::Svg Icon-->
                    </button>
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
</div>


@section('customJS')
    <script src="{{ asset('frontend/js/custom/scan/vehicle.js') }}"></script>
@endsection
