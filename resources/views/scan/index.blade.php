@extends('scan.base')

@section('title')
    Scan QR
@endsection

@section('content')
    <div class="card w-100 bg-transparent">
        <div class="card-header d-flex align-items-center justify-content-center">
            <div class="fs-2x text-cyan fw-bold text-center">Scan QR </div>
        </div>
        <div class="card-body">
            <form action="{{route('qr.scan.detail')}}" method="POST" class="form" id="scanQR">
                @csrf
                <div class="row">
                    <div class="col-12 mb-10">
                        <input type="text" name="key" autocomplete="off" class="form-control" placeholder="input the key">
                    </div>
                    <div class="col-12 text-center">
                        <button class="btn btn-cyan px-10 fw-normal" type="submit" form="scanQR">Check QR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
