  {{-- Syarat Pendaftaran --}}
  <div id="syarat-pendaftaran" class="mt-20">
    <div class="row">
        <div class="bg-cyan-50 border border-2 border-cyan p-11 rounded-25 text-gray-800">
            <div class="column-2">
                <div class="fs-3x fw-bold">
                    Syarat dan <br> Cara Pendaftaran
                </div>
                <div class="detail fs-4 mt-5">
                    <span class="html-syarat-pendaftaran fw-normal">
                        {!! $syarat_pendaftaran->html !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
