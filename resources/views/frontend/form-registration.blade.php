{{-- Form pendaftaran --}}
<div id="daftar"
    class="form-pendaftaran mt-20 py-20 mb-20 px-5 border border-3 rounded content-element shadow ff-fredoka">
    <div class="fs-3tx fw-bold text-cyan text-center mb-1">Form Pendaftaran</div>
    <div class="fs-3tx fw-bold text-cyan text-center mb-1">Mudik Gratis Jakarta 2022</div>

    <div class="stepper stepper-links d-flex flex-column pt-15" id="form-horizontal">
        <!--begin::Nav-->
        <div class="stepper-nav mb-5">
            <!--begin::Step 1-->
            <div class="stepper-item current" data-kt-stepper-element="nav">
                <h3 class="stepper-title fw-bold">Data Colon Pemudik</h3>
            </div>
            <!--end::Step 1-->
            <!--begin::Step 2-->
            <div class="stepper-item" data-kt-stepper-element="nav">
                <h3 class="stepper-title fw-bold">Data Anggota Keluarga</h3>
            </div>
            <!--end::Step 2-->
            <!--begin::Step 3-->
            <div class="stepper-item d-none" id="form-label-motor" data-kt-stepper-element="nav">
                <h3 class="stepper-title fw-bold">Data Sepeda Motor</h3>
            </div>
            <!--end::Step 3-->
        </div>
        <!--end::Nav-->
        <!--begin::Form-->
        <form class="mx-5 w-100 pt-15 pb-10" method="POST" action="api/web/v1/registration" novalidate="novalidate"
            id="registration-form">
            <!--begin::Step 1-->
            <div class="current me-10" id="form-page-1" data-kt-stepper-element="content">
                <!--begin::Wrapper-->
                <div class="w-100">
                    <div class="row">

                        @csrf

                        <input type="hidden" name="first" value="0" id="firstVal">
                        <input type="hidden" name="quota" id="quota-trip" value="4">
                        <input type="hidden" name="base_id" id="base-id">
                        <input type="hidden" name="type_trip" id="type-trip">

                        <div class="col-lg-4">

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Kota Arus Mudik dan Arus
                                    Balik</label>
                                <select data-control="select2" data-allow-clear="true" id="kota-tujuan"
                                    data-placeholder="Pilih Kota" name="trip_id" class="form-select">
                                    <option value=""></option>
                                    @foreach ($trip as $item)
                                        <option {{ old('trip_id') == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">{{ ucwords($item->city->name) }} (
                                            {{ ucwords(str_replace('motor', '+ motor', str_replace('balik', '- balik', str_replace('-', ' ', $item->type)))) }}
                                            )</option>
                                    @endforeach
                                </select>
                                <div class="text-gray-600">*tidak semua tujuan kota mengakomodir <br> pengangkutan
                                    sepeda
                                    motor <br>
                                    <u><a href="#" target="_blank" class="text-gray-600 text-hover-light-orange">Cek
                                            lokasi
                                            truk arus dan balik mudik</a></u>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Foto KTP</label>
                                <div class="row gy-0">
                                    <div class="col-md-7 col-lg-7">
                                        <input autocomplete="off" type="text" id="ktp-label" name="ktp"
                                            class="form-control" readonly>
                                    </div>
                                    <div class="col-md-5 col-lg-5">
                                        <div class="m-0 btn fw-normal btn-primary py-3 px-2 bg-cyan" id="btn-ktp">
                                            Upload KTP
                                            <input autocomplete="off" type="file" accept=".jpg,.png,.jpeg" id="ktp"
                                                name="file_ktp" class="d-none" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                <input autocomplete="off" type="text" name="name" class="form-control"
                                    value="{{ old('name') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                <input autocomplete="off" type="number" name="nik" class="form-control"
                                    value="{{ old('nik') }}" data-url="api/web/v1/nik/check">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Foto KK</label>
                                <div class="row">
                                    <div class="col-md-7 col-lg-7">
                                        <input autocomplete="off" type="text" id="kk-label" name="kk"
                                            class="form-control" readonly>
                                    </div>
                                    <div class="col-md-5 col-lg-5">
                                        <div class="m-0 btn fw-normal btn-primary py-3 px-2 bg-cyan" id="btn-kk">
                                            Upload KK
                                            <input autocomplete="off" type="file" accept=".jpg,.png,.jpeg" id="kk"
                                                name="file_kk" class="d-none" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nomor KK</label>
                                <input autocomplete="off" type="number" name="no_kk" class="form-control"
                                    value="{{ old('no_kk') }}">
                            </div>
                        </div>

                        <div class="col-lg-4">

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Kelamin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Kelamin" name="gender" class="form-select">
                                    <option value=""></option>
                                    @php
                                        $list = ['l', 'p'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('gender') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item == 'l' ? 'laki-laki' : 'perempuan') }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Tanggal Lahir</label>
                                <input autocomplete="off" type="date" name="datebirth" class="form-control"
                                    value="{{ old('datebirth') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Kota Sesuai KTP</label>
                                <input autocomplete="off" type="text" name="placebirth" class="form-control"
                                    value="{{ old('placebirth') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Alamat Sesuai KTP</label>
                                <textarea autocomplete="off" rows="3" name="address" class="form-control">{{ old('address') }}</textarea>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                <input autocomplete="off" type="text" name="email" class="form-control"
                                    value="{{ old('email') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                <input autocomplete="off" type="number" name="phone" class="form-control"
                                    value="{{ old('phone') }}">
                            </div>

                        </div>

                        <div class="col-lg-4">

                            <input type="hidden" name="vaksin">

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Status Vaksin</label>
                                <input autocomplete="off" type="text" name="label_vaksin" readonly
                                    class="form-control" value="{{ old('label_vaksin') }}">
                            </div>

                            <div class="mb-10 fv-row" data-kt-buttons="true">
                                <label class="form-label text-cyan fs-6 fw-bold">Apakah Anda Sudah Booster?</label>
                                <div class="row">
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster"
                                                value="sudah" />
                                            <span class="fw-bold">Sudah</span>
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster"
                                                value="belum" />
                                            <span class="fw-bold">Belum</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label text-cyan fs-6 fw-bold">Upload Booster</label>
                                <div class="row">
                                    <div class="col-md-7 col-lg-7">
                                        <input autocomplete="off" type="text" id="booster-label" name="booster"
                                            class="form-control" readonly>
                                    </div>
                                    <div class="col-md-5 col-lg-5">
                                        <div class="m-0 btn fw-normal btn-primary py-3 px-2 bg-cyan" id="btn-booster">
                                            Upload File
                                            <input autocomplete="off" type="file" accept=".jpg,.png,.jpeg" id="booster"
                                                name="file_booster" class="d-none" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Vaksin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Vaksin" name="type_vaksin" class="form-select">
                                    <option value=""></option>
                                    @php
                                        $list = ['booster', 'antigen', 'pcr'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('type_vaksin') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Puskesmas</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Puskesmas" name="puskes_id" class="form-select">
                                    <option value=""></option>
                                    @foreach ($puskes as $item)
                                        <option {{ old('puskes_id') == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">
                                            {{ 'Puskesmas - ' . $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" name="approve" id="flexCheckDefault">
                                    <label class="form-check-label fs-6 text-cyan" for="flexCheckDefault">
                                        Saya sudah membaca dan menyetujui persyaratan Mudik Gratis DKI Jakarta tahun
                                        2022
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--begin::Step 2-->
            <div class="me-10" data-kt-stepper-element="content">
                <!--begin::Wrapper-->
                <div class="w-100">
                    <div id="content-anggota-keluarga" class="row">

                        <div class="col-lg-4">

                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Anggota Keluarga 1</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                <input autocomplete="off" type="text" name="name_family_1" class="form-control"
                                    data-row="1" value="{{ old('name_family_1') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <div class="row">
                                    <div class="col-5">
                                        <label class="form-label text-cyan fs-6 fw-bold">Tempat</label>
                                        <input autocomplete="off" type="text" name="placebirth_family_1"
                                            class="form-control" data-row="1"
                                            value="{{ old('placebirth_family_1') }}">
                                    </div>
                                    <div class="col-7">
                                        <label class="form-label text-cyan fs-6 fw-bold">Tanggal</label>
                                        <input autocomplete="off" type="date" name="datebirth_family_1"
                                            class="form-control" data-row="1"
                                            value="{{ old('datebirth_family_1') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                <input autocomplete="off" type="number" name="nik_family_1" class="form-control"
                                    value="{{ old('nik_family_1') }}" data-row="1" data-url="api/web/v1/nik/check">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Kelamin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Kelamin" name="gender_family_1"
                                    class="form-select">
                                    <option value=""></option>
                                    @php
                                        $list = ['l', 'p'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('gender_family_1') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item == 'l' ? 'laki-laki' : 'perempuan') }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                <input autocomplete="off" type="number" name="phone_family_1" class="form-control"
                                    data-row="1" value="{{ old('phone_family_1') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                <input autocomplete="off" type="email" name="email_family_1" class="form-control"
                                    data-row="1" value="{{ old('email_family_1') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Status Vaksin</label>
                                <input autocomplete="off" type="text" name="label_vaksin_family_1" readonly
                                    class="form-control" value="{{ old('label_vaksin_family_1') }}">
                            </div>

                            <input type="hidden" name="vaksin_family_1">

                            <div class="mb-10 fv-row" data-kt-buttons="true">
                                <label class="form-label text-cyan fs-6 fw-bold">Sudah Booster?</label>
                                <div class="row">
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster_family_1"
                                                value="sudah" data-row="1" />
                                            <span class="fw-bold">Sudah</span>
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster_family_1"
                                                value="belum" data-row="1" />
                                            <span class="fw-bold">Belum</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label text-cyan fs-6 fw-bold">Upload Booster</label>
                                <div class="row">
                                    <div class="col-md-7 col-lg-7">
                                        <input autocomplete="off" type="text" id="booster_family_1-label"
                                            name="booster_family_1" class="form-control" data-row="1" readonly>
                                    </div>
                                    <div class="col-md-5 col-lg-5">
                                        <div class="m-0 btn fw-normal btn-primary py-3 px-2 bg-cyan"
                                            id="btn-booster_family_1">
                                            Upload File
                                            <input autocomplete="off" type="file" accept=".jpg,.png,.jpeg"
                                                id="booster_family_1" name="file_booster_family_1"
                                                class="d-none" data-row="1" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Vaksin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Vaksin" name="type_vaksin_family_1" data-row="1"
                                    class="form-select">
                                    <option value=""></option>
                                    @php
                                        $list = ['booster', 'antigen', 'pcr'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('type_vaksin') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Puskesmas</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Puskesmas" data-row="1" name="puskes_id_family_1"
                                    class="form-select">
                                    <option value=""></option>
                                    @foreach ($puskes as $item)
                                        <option {{ old('puskes_id') == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">
                                            {{ 'Puskesmas - ' . $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="col-lg-4">

                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Anggota Keluarga 2</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                <input autocomplete="off" type="text" name="name_family_2" class="form-control"
                                    data-row="2" value="{{ old('name_family_2') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <div class="row">
                                    <div class="col-5">
                                        <label class="form-label text-cyan fs-6 fw-bold">Tempat</label>
                                        <input autocomplete="off" type="text" name="placebirth_family_2"
                                            class="form-control" data-row="2"
                                            value="{{ old('placebirth_family_2') }}">
                                    </div>
                                    <div class="col-7">
                                        <label class="form-label text-cyan fs-6 fw-bold">Tanggal</label>
                                        <input autocomplete="off" type="date" name="datebirth_family_2"
                                            class="form-control" data-row="2"
                                            value="{{ old('datebirth_family_2') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                <input autocomplete="off" type="number" name="nik_family_2" class="form-control"
                                    value="{{ old('nik_family_2') }}" data-row="2" data-url="api/web/v1/nik/check">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Kelamin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Kelamin" name="gender_family_2"
                                    class="form-select">
                                    <option value=""></option>
                                    @php
                                        $list = ['l', 'p'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('gender_family_1') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item == 'l' ? 'laki-laki' : 'perempuan') }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                <input autocomplete="off" type="number" name="phone_family_2" class="form-control"
                                    data-row="2" value="{{ old('phone_family_2') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                <input autocomplete="off" type="email" name="email_family_2" class="form-control"
                                    data-row="2" value="{{ old('email_family_2') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Status Vaksin</label>
                                <input autocomplete="off" type="text" name="label_vaksin_family_2" readonly
                                    class="form-control" value="{{ old('label_vaksin_family_2') }}">
                            </div>

                            <input type="hidden" name="vaksin_family_2">

                            <div class="mb-10 fv-row" data-kt-buttons="true">
                                <label class="form-label text-cyan fs-6 fw-bold">Sudah Booster?</label>
                                <div class="row">
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster_family_2"
                                                value="sudah" data-row="2" />
                                            <span class="fw-bold">Sudah</span>
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster_family_2"
                                                value="belum" data-row="2" />
                                            <span class="fw-bold">Belum</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label text-cyan fs-6 fw-bold">Upload Booster</label>
                                <div class="row">
                                    <div class="col-md-7 col-lg-7">
                                        <input autocomplete="off" type="text" id="booster_family_2-label"
                                            name="booster_family_2" class="form-control" data-row="2" readonly>
                                    </div>
                                    <div class="col-md-5 col-lg-5">
                                        <div class="m-0 btn fw-normal btn-primary py-3 px-2 bg-cyan"
                                            id="btn-booster_family_2">
                                            Upload File
                                            <input autocomplete="off" type="file" accept=".jpg,.png,.jpeg"
                                                id="booster_family_2" name="file_booster_family_2" data-row="2"
                                                class="d-none" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Vaksin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Vaksin" name="type_vaksin_family_2"
                                    class="form-select" data-row="2">
                                    <option value=""></option>
                                    @php
                                        $list = ['booster', 'antigen', 'pcr'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('type_vaksin') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Puskesmas</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Puskesmas" name="puskes_id_family_2" class="form-select"
                                    data-row="2">
                                    <option value=""></option>
                                    @foreach ($puskes as $item)
                                        <option {{ old('puskes_id') == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">
                                            {{ 'Puskesmas - ' . $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="col-lg-4">

                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Anggota Keluarga 3</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                <input autocomplete="off" type="text" name="name_family_3" class="form-control"
                                    data-row="3" value="{{ old('name_family_3') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <div class="row">
                                    <div class="col-5">
                                        <label class="form-label text-cyan fs-6 fw-bold">Tempat</label>
                                        <input autocomplete="off" type="text" name="placebirth_family_3"
                                            class="form-control" data-row="3"
                                            value="{{ old('placebirth_family_3') }}">
                                    </div>
                                    <div class="col-7">
                                        <label class="form-label text-cyan fs-6 fw-bold">Tanggal</label>
                                        <input autocomplete="off" type="date" name="datebirth_family_3"
                                            class="form-control" data-row="3"
                                            value="{{ old('datebirth_family_3') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                <input autocomplete="off" type="number" name="nik_family_3" class="form-control"
                                    value="{{ old('nik_family_3') }}" data-row="3" data-url="api/web/v1/nik/check">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Kelamin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Kelamin" name="gender_family_3"
                                    class="form-select">
                                    <option value=""></option>
                                    @php
                                        $list = ['l', 'p'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('gender_family_1') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item == 'l' ? 'laki-laki' : 'perempuan') }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                <input autocomplete="off" type="number" name="phone_family_3" class="form-control"
                                    data-row="3" value="{{ old('phone_family_3') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                <input autocomplete="off" type="email" name="email_family_3" class="form-control"
                                    data-row="3" value="{{ old('email_family_3') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Status Vaksin</label>
                                <input autocomplete="off" type="text" name="label_vaksin_family_3" readonly
                                    class="form-control" value="{{ old('label_vaksin_family_3') }}">
                            </div>

                            <input type="hidden" name="vaksin_family_3">

                            <div class="mb-10 fv-row" data-kt-buttons="true">
                                <label class="form-label text-cyan fs-6 fw-bold">Sudah Booster?</label>
                                <div class="row">
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster_family_3"
                                                value="sudah" data-row="3" />
                                            <span class="fw-bold">Sudah</span>
                                        </label>
                                    </div>
                                    <div class="col-6">
                                        <label class="btn btn-outline btn-outline-dashed btn-outline-cyan w-100 p-3">
                                            <input type="radio" class="btn-check" name="check_booster_family_3"
                                                value="belum" data-row="3" />
                                            <span class="fw-bold">Belum</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label text-cyan fs-6 fw-bold">Upload Booster</label>
                                <div class="row">
                                    <div class="col-md-7 col-lg-7">
                                        <input autocomplete="off" type="text" id="booster_family_3-label"
                                            name="booster_family_3" class="form-control" readonly data-row="3">
                                    </div>
                                    <div class="col-md-5 col-lg-5">
                                        <div class="m-0 btn fw-normal btn-primary py-3 px-2 bg-cyan"
                                            id="btn-booster_family_3">
                                            Upload File
                                            <input autocomplete="off" type="file" accept=".jpg,.png,.jpeg"
                                                id="booster_family_3" name="file_booster_family_3" data-row="3"
                                                class="d-none" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Jenis Vaksin</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Jenis Vaksin" name="type_vaksin_family_3"
                                    class="form-select" data-row="3">
                                    <option value=""></option>
                                    @php
                                        $list = ['booster', 'antigen', 'pcr'];
                                    @endphp
                                    @foreach ($list as $item)
                                        <option {{ old('type_vaksin') == $item ? 'selected' : '' }}
                                            value="{{ $item }}">
                                            {{ ucwords($item) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-10 fv-row d-none">
                                <label class="form-label fs-6 fw-bold text-cyan">Puskesmas</label>
                                <select data-control="select2" data-allow-clear="true"
                                    data-placeholder="Pilih Puskesmas" data-row="3" name="puskes_id_family_3"
                                    class="form-select">
                                    <option value=""></option>
                                    @foreach ($puskes as $item)
                                        <option {{ old('puskes_id') == $item->id ? 'selected' : '' }}
                                            value="{{ $item->id }}">
                                            {{ 'Puskesmas - ' . $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!--begin::Step 3-->
            <div class="me-10" data-kt-stepper-element="content">
                <!--begin::Wrapper-->
                <div class="w-100">
                    <div class="row">
                        <div class="col-lg-4">

                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Data Motor</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Plat Motor</label>
                                <input autocomplete="off" type="text" name="no_police" class="form-control"
                                    value="{{ old('no_police') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Merk Motor</label>
                                <input autocomplete="off" type="text" name="merk_vehicle" class="form-control"
                                    value="{{ old('merk_vehicle') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Tipe Motor</label>
                                <input autocomplete="off" type="text" name="type_vehicle" class="form-control"
                                    value="{{ old('type_vehicle') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Warna Motor</label>
                                <input autocomplete="off" type="text" name="color_vehicle" class="form-control"
                                    value="{{ old('color_vehicle') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No STNK</label>
                                <input autocomplete="off" type="number" name="no_stnk" class="form-control"
                                    value="{{ old('no_stnk') }}">
                            </div>

                        </div>
                        <div class="col-lg-4">

                            <!--begin::Title-->
                            <div class="pb-10 pb-lg-15">
                                <h4 class="fw-bold d-flex align-items-center text-orange">Data Pemilik</h4>
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nama Lengkap</label>
                                <input autocomplete="off" type="text" name="owner_name" class="form-control"
                                    value="{{ old('owner_name') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">NIK</label>
                                <input autocomplete="off" type="number" name="owner_nik" class="form-control"
                                    value="{{ old('owner_nik') }}" data-url="api/web/v1/nik/check">
                            </div>
{{-- 
                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Nomor KK</label>
                                <input autocomplete="off" type="number" name="owner_no_kk" class="form-control"
                                    value="{{ old('owner_no_kk') }}">
                            </div> --}}

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">Email Aktif</label>
                                <input autocomplete="off" type="text" name="owner_email" class="form-control"
                                    value="{{ old('owner_email') }}">
                            </div>

                            <div class="mb-10 fv-row">
                                <label class="form-label text-cyan fs-6 fw-bold">No Handphone Aktif</label>
                                <input autocomplete="off" type="text" name="owner_phone" class="form-control"
                                    value="{{ old('owner_phone') }}">
                            </div>

                            <input type="hidden" name="owner_vaksin">

                        </div>
                    </div>
                </div>
            </div>

            <!--begin::Actions-->
            <div class="d-flex flex-stack pt-15 mx-10">
                <!--begin::Wrapper-->
                <div class="mr-2">
                    <button type="button" id="prev-step" class="btn btn-lg btn-light-orange me-3 d-none"
                        data-kt-stepper-action="previous">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                        <i class="fas fa-angle-double-left fs-4 text-inverse"></i>
                        <!--end::Svg Icon-->Sebelumnya
                    </button>
                </div>
                <!--end::Wrapper-->
                <!--begin::Wrapper-->
                <div>
                    <button type="button" class="btn btn-lg btn-orange me-3" data-kt-stepper-action="submit">
                        <span class="indicator-label">Daftar
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                            <i class="fas fa-save fs-4 text-white"></i>
                            <!--end::Svg Icon-->
                        </span>
                        <span class="indicator-progress">Mohon Tunggu...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                    <button type="button" id="next-step" class="btn btn-lg btn-orange"
                        data-kt-stepper-action="next">Selanjutnya
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                        <i class="fas fa-angle-double-right fs-4 text-white"></i>
                        <!--end::Svg Icon-->
                    </button>
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
</div>
