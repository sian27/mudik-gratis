<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-ktp" data-bs-toggle="modal"
    data-bs-target="#modalKtp">
    Call modal
</button>

<!-- Modal -->
<div class="modal fade" id="modalKtp" tabindex="-1" aria-labelledby="modalKtpLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title fs-3" id="modalKtpLabel">Harap pastikan foto tidak miring</span>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <img src="{{ asset('default/rule ktp.jpg') }}" alt="Rules KTP" class="img-fluid"
                    style="max-height: 300px">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batalkan</button>
                <button type="button" class="btn btn-cyan" id="choose-ktp">Oke, saya paham</button>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-loading-step" data-bs-toggle="modal"
    data-bs-target="#modalLoadingStep">
    Call modal loading step
</button>

<!-- Modal Loading -->
<div class="modal fade" id="modalLoadingStep" data-bs-backdrop="static" tabindex="-1"
    aria-labelledby="modalLoadingStepLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title fs-3">Mohon tunggu sedang memproses data</span>
                <button type="button" id="close-modal-loading-step" class="btn-close" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <input type="hidden" id="close-modalll" value="1">
                <img src="{{ asset('default/loading.gif') }}" alt="loading" class="img-fluid"
                    style="max-height: 300px">
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-loading" data-bs-toggle="modal"
    data-bs-target="#modalLoading">
    Call modal loading
</button>

<!-- Modal Loading -->
<div class="modal fade" id="modalLoading" data-bs-backdrop="static" tabindex="-1"
    aria-labelledby="modalLoadingLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title fs-3">Mohon tunggu sedang memproses KTP</span>
                <button type="button" id="close-modal-loading" class="btn-close d-none" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <img src="{{ asset('default/loading.gif') }}" alt="loading" class="img-fluid"
                    style="max-height: 300px">
            </div>
        </div>
    </div>
</div>


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-nik-err" data-bs-toggle="modal"
    data-bs-target="#modalKkError">
    Call modal KK ERROR
</button>

<!-- Modal KK OR NIK ERROR -->
<div class="modal fade" id="modalKkError" data-bs-backdrop="static" tabindex="-1"
    aria-labelledby="modalKkErrorLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title fs-3">Terjadi Kesalahan</span>
                <button type="button" class="btn-close d-none" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <div class="fs-3">
                    Hallo, kami mendeteksi NIK yang kamu input tidak ada di dalam KK ataupun sebaliknya pada Data <span id="target-err-kk" class="text-cyan fw-bold"></span> , mohon di cek
                    kembali data NIK dan KK kamu ya..
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cyan" data-bs-dismiss="modal">Oke, saya paham</button>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-verify-err" data-bs-toggle="modal"
    data-bs-target="#modalVerifyErr">
    Call modal Verify Indetify error
</button>

<!-- Modal Verify Identify ERROR -->
<div class="modal fade" id="modalVerifyErr" data-bs-backdrop="static" tabindex="-1"
    aria-labelledby="modalVerifyErrLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title fs-3">Terjadi Kesalahan</span>
                <button type="button" class="btn-close d-none" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body text-start">
                <div class="fs-3">
                    Hallo, kami mendeteksi pada Data <span id="target-err-verify" class="text-cyan fw-bold"></span> berikut ini ada yang tidak sesuai dengan KTP
                    <ol>
                        <li>Nama Lengkap</li>
                        <li>Tanggal Lahir</li>
                        <li>Kota sesuai KTP</li>
                        <li>Alamat</li>
                    </ol>
                    Mohon untuk mengecek kembali
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cyan" data-bs-dismiss="modal">Oke, saya paham</button>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-quota-limit" data-bs-toggle="modal"
    data-bs-target="#modalQuotaLimit">
    Call modal quota limit
</button>

<!-- Modal qouta limit -->
<div class="modal fade" id="modalQuotaLimit" data-bs-backdrop="static" tabindex="-1"
    aria-labelledby="modalQuotaLimitLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <span class="modal-title fs-3">Terjadi Kesalahan</span> --}}
                <button type="button" class="btn-close d-none" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <div class="fs-2 text-cyan">
                    Maaf Tujuan Mudik/Balik <br>
                    Yang Dipilih Tidak Tersedia
                </div>
                <div class="fs-4 text-gray-600">
                    Silahkan cek kembali lokasi arus mudik dan lokasi arus balik <a href="#" target="_blank" class="text-hover-orange text-decoration-underline">di sini</a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="closeModalLoading()" class="btn btn-cyan" data-bs-dismiss="modal">Oke, saya paham</button>
            </div>
        </div>
    </div>
</div>


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-quota-over" data-bs-toggle="modal"
    data-bs-target="#modalQuotaOver">
    Call modal quota over
</button>

<!-- Modal qouta over -->
<div class="modal fade" id="modalQuotaOver" data-bs-backdrop="static" tabindex="-1"
    aria-labelledby="modalQuotaOverLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <span class="modal-title fs-3">Terjadi Kesalahan</span> --}}
                <button type="button" class="btn-close d-none" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <div class="fs-2 text-cyan">
                    Kuota yang tersedia <br>
                    <span id="target-quota-over"></span> Orang Penumpang
                </div>
                <div class="fs-4 text-gray-600">
                    Apakah kamu ingin melanjutkan pendaftaran mudik gratis 2022?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="closeModalLoading()" class="btn btn-cyan" data-bs-dismiss="modal">Oke, tidak masalah</button>
            </div>
        </div>
    </div>
</div>


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary d-none" id="call-modal-first" data-bs-toggle="modal"
    data-bs-target="#modalFirst">
    Call modal Verify Indetify error
</button>

<!-- Modal First ERROR -->
<div class="modal fade" id="modalFirst" data-bs-backdrop="static" tabindex="-1"
    aria-labelledby="modalFirstLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <span class="modal-title fs-3">Terjadi Kesalahan</span>
                <button type="button" class="btn-close d-none" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body text-start">
                <div class="fs-3">
                    Mohon untuk pilih kota terlebih dahulu atau kouta saat ini kota pilihan kamu sudah habis
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-cyan" onclick="closeModalLoading()" data-bs-dismiss="modal">Oke, saya paham</button>
            </div>
        </div>
    </div>
</div>