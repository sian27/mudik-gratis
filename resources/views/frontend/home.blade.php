@php
$baseUrl = 'https://backend.mudikgratisdkijakarta.id/';
@endphp

@extends('layouts.frontend.app')

@section('title')
    {{ $title }}
@endsection

@section('customCSS')
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka:wght@300;400;500;600&display=swap" rel="stylesheet">

    <style>
        .ff-fredoka {
            font-family: 'Fredoka', sans-serif;
        }

        body {
            background-color: white !important;
        }

        .border-cyan {
            border-color: #007474 !important;
        }

        .border-muted rounded-15 {
            border-color: #A1A9C6 !important;
        }

        .carousel-control-next,
        .carousel-control-prev {
            position: absolute;
            top: 0;
            bottom: 0;
            z-index: 1;
            width: 4%;
            padding: 0;
            background: 0 0;
            border: 0;
            opacity: .5;
            transition: opacity .15s ease;
        }

        .bg-cyan-50 {
            background-image: linear-gradient(to right, #007474 0%, #007474 50%, white 50%, white 100%);
        }

        .column-2 {
            column-count: 2;
        }

        .rounded-25 {
            border-radius: 25px;
        }

        .rounded-15 {
            border-radius: 15px;
        }

        #lokasi-mudik .card {
            background: none;
        }

        #lokasi-mudik .carousel-item img {
            border-radius: 25px;
        }

        #lokasi-mudik .carousel-inner .carousel-item>div {
            position: relative;
            text-align: center;
        }

        #lokasi-mudik .tombol-detail {
            position: absolute;
            top: 80%;
            left: 23%;
            color: white;
        }

        #lokasi-mudik .btn {
            border-radius: 25px;
        }

        #lokasi-mudik .card {
            border: 0;
        }

        @media (max-width: 767px) {
            #lokasi-mudik .carousel-inner .carousel-item>div {
                display: none;
            }

            #lokasi-mudik .carousel-inner .carousel-item>div:first-child {
                display: block;
            }
        }

        #lokasi-mudik .carousel-inner .carousel-item.active,
        #lokasi-mudik .carousel-inner .carousel-item-next,
        #lokasi-mudik .carousel-inner .carousel-item-prev {
            display: flex;
        }

        /* medium and up screens */
        @media (min-width: 768px) {

            #lokasi-mudik .carousel-inner .carousel-item-end.active,
            #lokasi-mudik .carousel-inner .carousel-item-next {
                transform: translateX(25%);
            }

            #lokasi-mudik .carousel-inner .carousel-item-start.active,
            #lokasi-mudik .carousel-inner .carousel-item-prev {
                transform: translateX(-25%);
            }

            #carouselKotaMD .carousel-inner .carousel-item-end.active,
            #carouselKotaMD .carousel-inner .carousel-item-next {
                transform: translateX(50%);
            }

            #carouselKotaMD .carousel-inner .carousel-item-start.active,
            #carouselKotaMD .carousel-inner .carousel-item-prev {
                transform: translateX(-50%);
            }
        }

        #lokasi-mudik .carousel-inner .carousel-item-end,
        #lokasi-mudik .carousel-inner .carousel-item-start {
            transform: translateX(0);
        }

        @media (max-width: 992px) {
            .bg-cyan-50 {
                background-image: linear-gradient(to bottom, #007474 0%, #007474 50%, white 50%, white 100%);
            }

            .column-2 {
                column-count: 1;
            }
        }

        #daftar input,
        #daftar select {
            box-shadow: 0 0.1rem 1rem 0.25rem rgba(0, 0, 0, 0.02) !important;
        }

        .stepper.stepper-links .stepper-nav .stepper-item.current .stepper-title {
            color: #007474 !important;
        }

        .stepper.stepper-links .stepper-nav .stepper-item.current:after {
            background-color: #007474 !important;
            margin-top: -0.50rem !important;
        }

        /* .html-syarat-ketentuan-2 a {
                                                                                                                                                                                                                                                    color: #3f4254 !important;
                                                                                                                                                                                                                                                } */

        .html-syarat-ketentuan-2 a:hover {
            color: #FE852A !important;
        }

        .menu-item .menu-link.active .menu-title {
            color: #019292 !important;
        }

        select[readonly].ini-select2+.select2-container {
            pointer-events: none;
            touch-action: none;

            .select2-selection {
                background: #eee;
                box-shadow: none;
            }

            .select2-selection__arrow,
            .select2-selection__clear {
                display: none;
            }
        }

    </style>
@endsection

@section('content')
    <div class="container ff-fredoka">

        {{-- Banner --}}
        <div id="banner" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                {{-- @foreach ($banner as $i => $item)
                    <div class="carousel-item {{ !$i ? 'active' : '' }}">
                        @if ($item->type == 'html')
                            <div
                                class="d-flex justify-content-center align-items-center mt-10 bg-cyan text-white rounded-25 mx-15 py-20">
                                <div class="fs-1 fw-bolder py-10 text-center">
                                    <span class="html-banner"> {!! $item->html !!} </span>
                                    <div class="mt-10 d-flex justify-content-center">
                                        <a href="#daftar" class="btn btn-warning rounded-pill px-15">Daftar</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="d-flex justify-content-center align-items-center mt-10 mx-15 rounded-25">
                                @if (str_contains($item->file_path, '.png') || str_contains($item->file_path, '.jpg') || str_contains($item->file_path, '.jpeg'))
                                    <img src="{{ $baseUrl . $item->file_path }}" alt="{{ $baseUrl . $item->file_path }}"
                                        class="img-fluid" style="max-height: 400px; border-radius: 25px;">
                                @else
                                    <div class="ratio ratio-16x9" style="max-height: 400px">
                                        <iframe src="{{ $baseUrl . $item->file_path }}" allowfullscreen></iframe>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                @endforeach --}}

                @php
                    $list = [asset('default/banner/Banner 1.png'), asset('default/banner/vid1.mp4'), asset('default/banner/Banner 3.png'), asset('default/banner/Banner 4.png')];
                    $i = 0;
                @endphp

                @foreach ($list as $item)
                    <div class="carousel-item {{ !$i++ ? 'active' : '' }}">
                        <div class="d-flex justify-content-center align-items-center mt-10 mx-15 rounded-25">
                            @if (str_contains($item, '.png') || str_contains($item, '.jpg') || str_contains($item, '.jpeg'))
                                <img src="{{ $item . '?ver=' . date('Y-m-d H:i:s') }}" alt="{{ $item }}"
                                    class="img-fluid" style="max-height: 400px; border-radius: 25px;">
                            @else
                                <div class="ratio ratio-16x9" style="max-height: 400px">
                                    <iframe src="{{ $item . '?ver=' . date('Y-m-d H:i:s') }}" allowfullscreen></iframe>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            <button class="carousel-control-prev d-flex align-items-center justify-content-start" type="button"
                data-bs-target="#banner" data-bs-slide="prev">
                <span
                    class="rounded-circle border border-1 border-cyan d-flex justify-content-center align-items-center min-w-40px min-h-40px">
                    <i class="fas fa-chevron-left fs-3 text-dark"></i>
                    <span class="visually-hidden">Previous</span>
                </span>
            </button>
            <button class="carousel-control-next d-flex align-items-center justify-content-end" type="button"
                data-bs-target="#banner" data-bs-slide="next">
                <span
                    class="rounded-circle border border-1 border-cyan d-flex justify-content-center align-items-center min-w-40px min-h-40px">
                    <i class="fas fa-chevron-right fs-3 text-dark"></i>
                    <span class="visually-hidden">Next</span>
                </span>
            </button>
        </div>

        {{-- Arus Mudik --}}
        <div id="arus-mudik" class="mt-20">
            <div class="row">
                <div class="col-lg-2 d-flex align-self-end">
                    <div class="label text-orange fs-1 fw-bold mb-2">Jumlah Arus Mudik</div>
                </div>
                <div class="col-lg-2">
                    <div class="kuota d-none d-lg-block">
                        <div
                            class="label fs-3 fw-bold text-gray-600 text-center pb-3 border-bottom border-1 border-gray-400">
                            Kuota/Kota</div>
                        <div class="count-city fs-3tx text-orange fw-bold text-center">-</div>
                    </div>
                    <div class="kuota d-block d-lg-none border border-2 border-gray-500 rounded-15 p-2">
                        <div class="label fs-3 fw-bold text-gray-600 text-center mb-n4">Kuota/Kota</div>
                        <hr>
                        <div class="count-city fs-1 text-orange fw-bold text-center">-</div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="bus border border-2 border-gray-500 rounded-15 p-2">
                        <div
                            class="label fs-3 fw-bold text-gray-600 pb-0 border-bottom border-1 border-gray-400 text-center">
                            Bus Penumpang</div>
                        <div class="d-flex content-justify-between align-items-center">
                            <div class="unit mx-2 w-100">
                                <div class="text-gray-600 text-center">Unit</div>
                                <div class="fs-2 fw-bold text-orange text-center html">-</div>
                            </div>
                            <div class="quota mx-2 w-100">
                                <div class="text-gray-600 text-center">Kursi</div>
                                <div class="fs-2 fw-bold text-orange text-center html">-</div>
                            </div>
                            <div class="available mx-2 w-100">
                                <div class="text-gray-600 text-center">Sisa</div>
                                <div class="fs-2 fw-bold text-orange text-center html">-</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="truck border border-2 border-gray-600 rounded-15 p-2">
                        <div
                            class="label fs-3 fw-bold text-gray-600 pb-0 border-bottom border-1 border-gray-400 text-center text-center">
                            Truck Motor</div>
                        <div class="d-flex content-justify-between align-items-center">
                            <div class="unit mx-2 w-100">
                                <div class="text-gray-600 text-center">Unit</div>
                                <div class="fs-2 fw-bold text-orange text-center html">-</div>
                            </div>
                            <div class="quota mx-2 w-100">
                                <div class="text-gray-600 text-center">Kuota</div>
                                <div class="fs-2 fw-bold text-orange text-center html">-</div>
                            </div>
                            <div class="available mx-2 w-100">
                                <div class="text-gray-600 text-center">Sisa</div>
                                <div class="fs-2 fw-bold text-orange text-center html">-</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Arus Balik --}}
        <div id="arus-balik" class="mt-20">
            <div class="row">
                <div class="col-lg-2 d-flex align-self-end">
                    <div class="label text-cyan fs-1 fw-bold mb-3">Jumlah Arus Balik</div>
                </div>
                <div class="col-lg-2">
                    <div class="kuota d-none d-lg-block">
                        <div
                            class="label fs-3 fw-bold text-gray-600 text-center pb-3 border-bottom border-1 border-gray-400 text-center">
                            Kuota/Kota</div>
                        <div class="count-city fs-3tx text-cyan fw-bold text-center">-</div>
                    </div>
                    <div class="kuota d-block d-lg-none border border-2 border-gray-600 rounded-15 p-2">
                        <div class="label fs-3 fw-bold text-gray-600 text-center mb-n4">Kuota/Kota</div>
                        <hr>
                        <div class="count-city fs-1 text-cyan fw-bold text-center">-</div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="bus border border-2 border-gray-600 rounded-15 p-2">
                        <div
                            class="label fs-3 fw-bold text-gray-600 pb-0 border-bottom border-1 border-gray-400 text-center text-center">
                            Bus Penumpang</div>
                        <div class="d-flex content-justify-between align-items-center">
                            <div class="unit mx-2 w-100">
                                <div class="text-gray-600 text-center">Unit</div>
                                <div class="fs-2 fw-bold text-cyan text-center html">-</div>
                            </div>
                            <div class="quota mx-2 w-100">
                                <div class="text-gray-600 text-center">Kursi</div>
                                <div class="fs-2 fw-bold text-cyan text-center html">-</div>
                            </div>
                            <div class="available mx-2 w-100">
                                <div class="text-gray-600 text-center">Sisa</div>
                                <div class="fs-2 fw-bold text-cyan text-center html">-</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="truck border border-2 border-gray-600 rounded-15 p-2">
                        <div
                            class="label fs-3 fw-bold text-gray-600 pb-0 border-bottom border-1 border-gray-400 text-center text-center">
                            Truck Motor</div>
                        <div class="d-flex content-justify-between align-items-center">
                            <div class="unit mx-2 w-100">
                                <div class="text-gray-600 text-center">Unit</div>
                                <div class="fs-2 fw-bold text-cyan text-center html">-</div>
                            </div>
                            <div class="quota mx-2 w-100">
                                <div class="text-gray-600 text-center">Kuota</div>
                                <div class="fs-2 fw-bold text-cyan text-center html">-</div>
                            </div>
                            <div class="available mx-2 w-100">
                                <div class="text-gray-600 text-center">Sisa</div>
                                <div class="fs-2 fw-bold text-cyan text-center html">-</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Syarat Ketentuan --}}
        <div id="syarat-ketentuan" class="mt-20">
            <div class="row">
                <div class="bg-cyan-50 border border-2 border-cyan px-8 rounded-25 text-dark">
                    <div class="row">
                        <div class="col-lg-6 my-20 text-white">
                            <div class="fs-3x fw-bold">
                                Syarat dan <br> Cara Pendaftaran
                            </div>
                            <div class="detail fs-4 mt-5">
                                <span class="html-syarat-ketentuan-1">
                                    <ol>
                                        <li> Mudik Gratis DKI Jakarta tahun 2022 diutamakan bagi: <ol type="a">
                                                <li>Warga yang memiliki KTP domisili
                                                    DKI Jakarta </li>
                                                <li>Warga yang sudah vaksin dosis 3 (booster) </li>
                                                <li>Warga yang membawa/ memiliki sepeda motor dan wajib melampirkan STNK
                                                </li>
                                                <li>Warga yang perjalanan pergi dan kembali ke
                                                    DKI Jakarta
                                                    <br>
                                                    <b>Jadwal Arus Mudik</b>
                                                    <ul>
                                                        <li>Truk; Selasa, 26 April 2022
                                                            di Terminal Pulogadung</li>
                                                        <li>Bus; Rabu, 27 April 2022
                                                            di Terminal Terpadu Pulo Gebang
                                                        </li>
                                                    </ul>

                                                    <b>Jadwal Arus Balik</b>
                                                    <ul>
                                                        <li>Truk; Sabtu, 7 Mei 2022 dari lokasi
                                                            masing-masing, tujuan Terminal Pulogadung</li>
                                                        <li>Bus; Minggu, 8 Mei 2022 dari lokasi
                                                            masing-masing, tujuan Terminal Terpadu Pulo Gebang</li>
                                                    </ul>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-6 ps-8 my-20 text-gray-600">
                            <div class="detail fs-4 mt-5">
                                <span class="html-syarat-ketentuan-2">
                                    <ol start="2">
                                        <li>Pendaftaran maksimal per KK untuk 4 orang dan 1 sepeda motor.</li>
                                        <li>Pendaftaran online pada website <b>www.mudikgratisdkijakarta.id</b> atau melalui
                                            Whatsapp <b>08-123-188-5758</b> </li>
                                        <li> Setelah mengisi form pendaftaran, petugas
                                            panitia akan menghubungi Anda untuk jadwal
                                            verifikasi offline. Pastikan nomor Anda aktif
                                            untuk dapat dihubungi.</li>
                                        <li> Calon pemudik datang ke lokasi verifikasi
                                            offline sesuai dengan tempat dan waktu yang
                                            tertera pada tiket kode booking (QR code)
                                            untuk mendapatkan E-Ticket keberangkatan.
                                            Lokasi verifikasi offline adalah sebagai
                                            berikut: <ul>
                                                <li><u><a class="text-gray-600" target="_blank"
                                                            href="https://goo.gl/maps/Hb9KeMbYECCGFLEu9">Dinas Perhubungan
                                                            Provinsi DKI Jakarta </a>
                                                    </u></li>
                                                <li><u><a class="text-gray-600" target="_blank"
                                                            href="https://goo.gl/maps/ad7gvM79wADsWzYQ8">Suku
                                                            Dinas Perhubungan Kota Jakarta
                                                            Barat </a></u></li>
                                                <li><u><a class="text-gray-600" target="_blank"
                                                            href="https://goo.gl/maps/Bdq5aTgu7ucv4dVs5">Suku
                                                            Dinas Perhubungan Kota Jakarta
                                                            Pusat </a></u></li>
                                                <li><u><a class="text-gray-600" target="_blank"
                                                            href="https://goo.gl/maps/5iLn8rrw3CnEx7GM9">Suku
                                                            Dinas Perhubungan Kota Jakarta
                                                            Timur </a></u></li>
                                                <li><u><a class="text-gray-600" target="_blank"
                                                            href="https://goo.gl/maps/oin1Fo8DCbfUyNT46">Suku
                                                            Dinas Perhubungan Kota Jakarta
                                                            Selatan </a></u></li>
                                                <li><u><a class="text-gray-600" target="_blank"
                                                            href="https://goo.gl/maps/YJchbWjCoH6d7gCo9">Suku
                                                            Dinas Perhubungan Kota Jakarta
                                                            Utara </a></u></li>
                                            </ul>
                                        </li>
                                        {{-- <li> Calon pemudik wajib hadir untuk verifikasi offline pada waktu dan lokasi yang
                                            sudah tertera pada QR Code </li> --}}
                                    </ol>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Lokasi Mudik --}}
        <div id="lokasi-mudik" class="mt-20">
            <div class="row">
                <div class="col-lg-3 text-cyan fw-bolder fs-2qx">
                    <span class="count-city"></span> Lokasi Mudik Gratis Jakarta 2022
                </div>
                <div class="col-lg-9 fs-2 text-gray-700 fw-light">
                    <span class="html-lokasi-mudik">
                        {!! $lokasi_mudik->html !!}
                    </span>
                </div>
                <div class="col-12">
                    <div id="carouselKotaLG" class="carousel d-none d-lg-block carousel-kota slide bg-transparent"
                        data-bs-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($city as $item)
                                <div class="carousel-item item-carousel-kota {{ !$i++ ? 'active' : '' }}">
                                    <div class="card bg-transparent p-1 text-center w-100">
                                        <div class="card-body">
                                            <img src="{{ $baseUrl . $item->image_path }}"
                                                alt="{{ $baseUrl . $item->image_path }}" height="200px" width="200px"
                                                style="object-fit: cover;object-position: center; position: relative">
                                            @foreach ($item->trip as $sub)
                                                @if ($sub->type == 'mudik-balik-motor')
                                                    <div class="overlay"
                                                        style="position: absolute;top: 28px; right: 34px;">
                                                        <img src="{{ asset('default/motorcyle.png') }}"
                                                            class="img-fluid rounded" style="max-height: 50px"
                                                            alt="overlay">
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="detail mx-5 rounded">
                                                <div class="fs-2 text-cyan"> {{ $item->name }} </div>
                                                <div class="fs-2 text-cyan">
                                                    {{ str_replace(' ', '-', ucwords(str_replace('-', ' ', str_replace('-motor', '', $item->type)))) }}
                                                </div>
                                                <div class="fs-4 text-muted"> Terminal {{ $item->terminal_name }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="carousel-control-prev d-flex align-items-center justify-content-start" type="button"
                            data-bs-target="#carouselKotaLG" data-bs-slide="prev">
                            <span
                                class="rounded-circle border border-1 border-cyan d-flex justify-content-center align-items-center min-w-25px min-h-25px">
                                <i class="fas fa-chevron-left fs-1 text-dark"></i>
                                <span class="visually-hidden">Previous</span>
                            </span>
                        </button>
                        <button class="carousel-control-next d-flex align-items-center justify-content-end" type="button"
                            data-bs-target="#carouselKotaLG" data-bs-slide="next">
                            <span
                                class="rounded-circle border border-1 border-cyan d-flex justify-content-center align-items-center min-w-25px min-h-25px">
                                <i class="fas fa-chevron-right fs-1 text-dark"></i>
                                <span class="visually-hidden">Next</span>
                            </span>
                        </button>
                    </div>
                    <div id="carouselKotaMD" class="carousel d-block d-lg-none carousel-kota-md slide bg-transparent"
                        data-bs-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($city as $item)
                                <div class="carousel-item item-carousel-kota-md {{ !$i++ ? 'active' : '' }}">
                                    <div class="card bg-transparent p-1 text-center w-100">
                                        <div class="card-body">
                                            <img src="{{ $baseUrl . $item->image_path }}"
                                                alt="{{ $baseUrl . $item->image_path }}" height="200px" width="200px"
                                                style="object-fit: cover;object-position: center; position: relative;">
                                            @foreach ($item->trip as $sub)
                                                @if ($sub->type == 'mudik-balik-motor')
                                                    <div class="overlay"
                                                        style="position: absolute;top: 28px; right: 84px;">
                                                        <img src="{{ asset('default/motorcyle.png') }}"
                                                            class="img-fluid rounded" style="max-height: 50px"
                                                            alt="overlay">
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="detail mx-5 rounded">
                                                <div class="fs-2 text-cyan"> {{ $item->name }} </div>
                                                <div class="fs-2 text-cyan"> (Mudik-Balik) </div>
                                                <div class="fs-4 text-muted"> Terminal {{ $item->terminal_name }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="carousel-control-prev d-flex align-items-center justify-content-start" type="button"
                            data-bs-target="#carouselKotaMD" data-bs-slide="prev">
                            <span
                                class="rounded-circle border border-1 border-cyan d-flex justify-content-center align-items-center min-w-25px min-h-25px">
                                <i class="fas fa-chevron-left fs-1 text-dark"></i>
                                <span class="visually-hidden">Previous</span>
                            </span>
                        </button>
                        <button class="carousel-control-next d-flex align-items-center justify-content-end" type="button"
                            data-bs-target="#carouselKotaMD" data-bs-slide="next">
                            <span
                                class="rounded-circle border border-1 border-cyan d-flex justify-content-center align-items-center min-w-25px min-h-25px">
                                <i class="fas fa-chevron-right fs-1 text-dark"></i>
                                <span class="visually-hidden">Next</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        {{-- Lokasi dan Ketentuan --}}
        <div id="lokasi-ketentuan" class="mt-20">
            <div class="row">
                <div class="bg-cyan-50 border border-2 border-cyan px-8 rounded-25 text-dark">
                    <div class="row">
                        <div class="col-lg-6 my-20 text-white">
                            <div class="fs-3x fw-bold">
                                Lokasi keberangkatan <br> dan Kedatangan Motor
                            </div>
                            <div class="detail fs-4 mt-5">
                                <span class="html-lokasi-keberangkatan">
                                    {!! $lokasi_keberangkatan->html !!}
                                </span>
                                <u><a href="#" class="text-white text-hover-warning mt-10 fw-bold">Cek Lokasi Arus Dan
                                        Balik Mudik</a></u>
                            </div>
                        </div>
                        <div class="col-lg-6 ps-8 my-20 text-gray-600">
                            <div class="fs-3x fw-bold text-cyan">
                                Syarat & Ketentuan Pengangkutan Sepeda Motor
                            </div>
                            <div class="detail fs-4 mt-5">
                                <span class="syarat-ketentuan">
                                    {!! $syarat_ketentuan->html !!}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('frontend.form-registration')

        @include('frontend.modal')

    </div>
@endsection

@section('customJS')
    <script>
        var items = document.querySelectorAll('.carousel-kota .item-carousel-kota')

        items.forEach((el) => {
            var minPerSlide = 4
            let next = el.nextElementSibling
            for (var i = 1; i < minPerSlide; i++) {
                if (!next) {
                    // wrap carousel by using first child
                    next = items[0]
                }
                let cloneChild = next.cloneNode(true)
                el.appendChild(cloneChild.children[0])
                next = next.nextElementSibling
            }
        })
    </script>

    <script>
        var items = document.querySelectorAll('.carousel-kota-md .item-carousel-kota-md')

        items.forEach((el) => {
            var minPerSlide = 2
            let next = el.nextElementSibling
            for (var i = 1; i < minPerSlide; i++) {
                if (!next) {
                    // wrap carousel by using first child
                    next = items[0]
                }
                let cloneChild = next.cloneNode(true)
                el.appendChild(cloneChild.children[0])
                next = next.nextElementSibling
            }
        })
    </script>
    <script src="{{ asset('js/api/base.js') }}"></script>
    <script src="{{ asset('js/api/frontend.js') }}"></script>
    <script src="{{ asset('frontend/js/custom/registration/index.js') }}"></script>
    <script>
        // declare api
        const callAllApi = () => {
            countCity();
            countBus();
            countBusQouta();
            countBusQoutaAvailable();
            countTruck();
            countTruckQouta();
            countTruckQoutaAvailable();
        }

        // first
        callAllApi();

        // looping interval 5s
        // setInterval(() => {
        //     callAllApi();
        // }, 10000);
    </script>

    {{-- upload foto and call api ocr extra --}}
    <script>
        $("#btn-ktp").on("click", function(e) {
            if (document.getElementById('ktp-label').value == '') {
                if (document.getElementById('firstVal').value === 0) {
                    document.getElementById('call-modal-first').click();
                } else if (document.querySelector('#quota-trip').value > 0) {
                    document.getElementById('call-modal-ktp').click();
                } else {
                    document.getElementById('call-modal-quota-limit').click()
                }
            }
        })
        $("#choose-ktp").on("click", function() {
            document.getElementById("ktp").click();
        })

        $("#ktp").on("change", function() {
            document.getElementById("ktp-label").style.display = "inline-block";
            if (!document.getElementById("ktp").files[0]) {
                document.getElementById("ktp-label").value = null;
            } else {
                document.getElementById("ktp-label").value = document.getElementById("ktp").files[0].name;

                var reader = new FileReader();
                reader.readAsDataURL(document.getElementById("ktp").files[0]);

                reader.onload = function() {
                    const data = {
                        ktp_image: reader.result.replace('data:image/jpeg;base64,', '')
                    }
                    orcExtra(data);
                    document.getElementById('call-modal-loading').click()
                };
                reader.onerror = function(error) {
                    console.log('Error: ', error);
                };

            }
        })
    </script>

    <script>
        $('#kota-tujuan').on('change', function() {
            if ($(this).val()) {
                checkTypeTrip({
                    id: $(this).val()
                })
            }
        })
    </script>
    {{-- @include('layouts.cms.js.up-file', ['var' => 'ktp']) --}}
    @include('layouts.frontend.up-file', ['var' => 'kk'])
    @include('layouts.frontend.up-file', ['var' => 'booster'])
    @include('layouts.frontend.up-file', ['var' => 'booster_family_1'])
    @include('layouts.frontend.up-file', ['var' => 'booster_family_2'])
    @include('layouts.frontend.up-file', ['var' => 'booster_family_3'])
@endsection
